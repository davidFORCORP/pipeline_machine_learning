

# ML Model Functions

1. Load data: use the load_data function to load your data from a CSV file or a dictionary into a NumPy array of shape (num_samples, num_features) and a NumPy array of shape (num_samples,) containing the labels.
2. Split data: use the split_data function to split your data into training and testing sets.
3. Select features: use the select_features function to select the best k features based on the F-test (ANOVA) between the features and the labels.
4. Make datasets: use the make_datasets function to create TensorFlow datasets from your training and testing sets, and normalize your features.
5. Build models: use the build_models function to create your neural network model with a specified number of hidden layers, input shape, and dropout rate.
6. Train model: use the train_model function to train your model on the training dataset, and validate on a validation dataset if available.
7. Test model: use the test_model function to test your trained model on the testing dataset.
8. Run experiment: use the run_experiment function to run a complete experiment with a set of hyperparameters, and return the training history, test loss, and test accuracy.
9. Run multiple experiments: use the run_multiple_experiments function to run multiple experiments with different hyperparameters and save the results to a CSV file.

## load_data
This function loads the data from either a CSV file or a dictionary of NumPy arrays. If a CSV file is provided, the function uses the pandas library to load the data into a pandas DataFrame. The function then separates the features and labels and returns them as separate NumPy arrays. If a dictionary is provided, the function assumes that the dictionary contains the feature and label data as separate NumPy arrays.

* data: A pandas DataFrame or dictionary containing the input data.
* label_col: A string representing the label column in the input data.
* batch_size: An integer representing the batch size.
* num_epochs: An integer representing the number of epochs to repeat the dataset.

### Returns:
* tensor flow dataset object

## split_data
* Splits the data into training and testing sets.

### Arguments:
* X (pandas.DataFrame): A DataFrame containing the features.
y (pandas.DataFrame): A DataFrame containing the target variable.
* test_size (float): The proportion of the data to use for testing.

### Returns:
* X_train (pandas.DataFrame): A DataFrame containing the training features.
* X_test (pandas.DataFrame): A DataFrame containing the testing features.
* y_train (pandas.DataFrame): A DataFrame containing the training target variable.
* y_test (pandas.DataFrame): A DataFrame containing the testing target variable.


## select_features
* Selects the best subset of features to use for training the model.

### Arguments:
* X_train (pandas.DataFrame): A DataFrame containing the training features.
* y_train (pandas.DataFrame): A DataFrame containing the training target variable.
* k (int): The number of features to select.

### Returns:
best_features (list): A list containing the names of the best features.


## build_model
* Builds a neural network model.

### Arguments:
* input_shape (tuple): The shape of the input layer.
* hidden_layers (list): A list of integers representing the number of neurons in each hidden layer.
* output_shape (int): The number of neurons in the output layer.
* activation (string): The activation function to use in each layer.

### Returns:
* model (keras.Model): A compiled Keras model.


## make_datasets
* Creates TensorFlow Dataset objects from the training and testing data.

### Arguments:
* X_train (pandas.DataFrame): A DataFrame containing the training features.
* y_train (pandas.DataFrame): A DataFrame containing the training target variable.
* X_test (pandas.DataFrame): A DataFrame containing the testing features.
* y_test (pandas.DataFrame): A DataFrame containing the testing target variable.
* batch_size (int): The size of the batches to use.

* ### Returns:
* train_dataset (tf.data.Dataset): A Dataset object containing the training data.
* test_dataset (tf.data.Dataset): A Dataset object containing the testing data.


## train_model
* This function takes a model and a training dataset and trains the model for a specified number of epochs using the specified optimizer and loss function.

### Arguments:

* model: The Keras model to train.
* train_dataset: The training dataset.
* epochs: The number of epochs to train the model for.
* optimizer: The optimizer to use during training.
* loss: The loss function to use during training.

### Returns:
* None


## run_experiment
* This function performs a single experiment by building a model with the specified parameters, training the model on the training dataset, and evaluating the model on the validation dataset.

### Arguments:

* train_dataset: The training dataset.
* val_dataset: The validation dataset.
* feature_subset: The subset of features to use in the model.
* hidden_units: The number of hidden units to use in each dense layer of the model.
* learning_rate: The learning rate to use during training.
* batch_size: The batch size to use during training.
* epochs: The number of epochs to train the model for.
* optimizer: The optimizer to use during training.
* loss: The loss function to use during training.

### Returns:
* A dictionary containing the following keys:
    * feature_subset': The subset of features used in the model.
  * hidden_units': The number of hidden units used in each dense layer of the model. 
  * learning_rate': The learning rate used during training.
  * 'batch_size': The batch size used during training.
  * 'epochs': The number of epochs trained for.
  * 'optimizer': The optimizer used during training.
  * 'loss': The loss function used during training.
  * 'train_loss': The training loss.
  * 'val_loss': The validation loss.
  * 'train_accuracy': The training accuracy.
  * 'val_accuracy': The validation accuracy.


## run_multiple_experiments
* This function performs multiple experiments by iterating over the specified hyperparameters and calling the run_experiment function for each combination of hyperparameters.

### Arguments:

* train_ds: The training dataset obtained using make_datasets() function.
* val_ds: The validation dataset obtained using make_datasets() function.
* test_ds: The testing dataset obtained using make_datasets() function.
* feature_subset: A list of indices of features to be used for training the model.
* optimizer: The optimizer to be used for training the model.
* lr: The learning rate to be used with the optimizer.
* batch_size: The size of the mini-batch to be used for training the model.
* epochs: The number of epochs to train the model.
* early_stopping: Whether to use early stopping during training.
*  patience: The patience to use for early stopping.
* verbose: Whether to print training progress during training.

### Returns:
* history: The training history of the model.