"""Contains the AbstractTopicLoader class"""
from core.queue import Queue
from loader.abstract_topic_loader import AbstractTopicLoader


class AttributeLoader(AbstractTopicLoader):
    """Loads messages for the MachineLearningProcessor"""

    def __init__(self):
        AbstractTopicLoader.__init__(self)

    def supports(self, topic):
        """If topic loader supports topic, return true. Otherwise, return false."""
        return topic == Queue().QUEUE_ATTRIBUTE

    def create_message(self, tile_name):
        """Creates a message for the topic."""
        return {
            'tile_name': tile_name,
        }
