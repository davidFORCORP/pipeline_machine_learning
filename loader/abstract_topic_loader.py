"""Contains the AbstractTopicLoader class"""
from abc import ABCMeta, abstractmethod


class AbstractTopicLoader(metaclass=ABCMeta):
    """Parent class of all topic loaders"""

    def __init__(self, las_directory="output"):
        self._las_directory = las_directory

    @abstractmethod
    def supports(self, topic):
        """If topic loader supports topic, return true. Otherwise, return false."""
        raise NotImplementedError

    @abstractmethod
    def create_message(self, tile_name):
        """Creates a message for the topic."""
        raise NotImplementedError

    @property
    def las_directory(self):
        return self._las_directory
