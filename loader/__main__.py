from loader.tile_loader import TileLoader


def load_images():
    loader = TileLoader()
    loader.begin()


if __name__ == "__main__":
    load_images()
