"""
Contains the Loader class.
"""
import json

from core.config import Config
from core.db_connection import DbConnection
from core.progress_service import ProgressService
from core.rabbitmq_service import RabbitMqService
from core.queue import Queue
from loader.attribute_loader import AttributeLoader
from loader.clip_loader import ClipLoader
from loader.projection_loader import ProjectionLoader


class TileLoader:
    """
    Loads images into the first processor of the pipeline.

    This can be ran on any machine, developer or VM.
    """

    def __init__(self):
        self.rabbitmq_service = RabbitMqService()
        self.queue_connection = self.rabbitmq_service.connection()
        self.queue_channel = self.rabbitmq_service.channel(self.queue_connection)
        db_connection = DbConnection()
        self.progress_service = ProgressService(db_connection)
        self.config = Config()

        self.tls_dir = self.config.get('loader', 'terrestial_lidar_directory')
        self.als_dir = self.config.get('loader', 'airborne_lidar_directory')

    def begin(self):

        # Choose topic queue to load messages
        topic = Queue().QUEUE_PROJECTION

        # Choose one of these paths, comment out the other
        las_directory = self.als_dir
        # las_directory = self.tls_dir

        # Choose files based on above directory
        las_files = [
            'NE0404306w5m.las','NE0404915w5m.las','NE0405010w5m.las','NE0504406w5m.las','NE0504915w5m.las',
            'NE0604406w5m.las','NE0604915w5m.las','NE0904306w5m.las','NE0904712w5m.las','NE1004210w5m.las',
            'NE1604915w5m.las','NE1804306w5m.las','NE1804915w5m.las','NE1904306w5m.las','NE1904810w5m.las',
            'NE2004508w5m.las','NE2205009w5m.las','NE2504709w5m.las','NE2704412w5m.las','NE2805110w5m.las',
            'NE3104306w5m.las','NE3305110w5m.las','NE3305114w5m.las','NE3504109w5m.las','NE3604307w5m.las',
            'NW0404915w5m.las','NW0405010w5m.las','NW0504406w5m.las','NW0604406w5m.las','NW0804915w5m.las',
            'NW0904306w5m.las','NW1004210w5m.las','NW1104210w5m.las','NW1304210w5m.las','NW1304307w5m.las',
            'NW1604915w5m.las','NW1704915w5m.las','NW1804915w5m.las','NW1904810w5m.las','NW2004508w5m.las',
            'NW2205009w5m.las','NW2504709w5m.las','NW2704412w5m.las','NW2805110w5m.las','NW3104306w5m.las',
            'NW3204306w5m.las','NW3604307w5m.las','SE0304210w5m.las','SE0404306w5m.las','SE0504406w5m.las',
            'SE0604915w5m.las','SE0804915w5m.las','SE0904306w5m.las','SE0904712w5m.las','SE1104210w5m.las',
            'SE1304307w5m.las','SE1604915w5m.las','SE1804306w5m.las','SE1904810w5m.las','SE2504709w5m.las',
            'SE3004306w5m.las','SE3005110w5m.las','SE3304306w5m.las','SE3305110w5m.las','SE3305114w5m.las',
            'SE3504109w5m.las','SW0304210w5m.las','SW0404915w5m.las','SW0405010w5m.las','SW0504915w5m.las',
            'SW0604406w5m.las','SW0604915w5m.las','SW0804915w5m.las','SW0904712w5m.las','SW1004210w5m.las',
            'SW1104210w5m.las','SW1304307w5m.las','SW1804306w5m.las','SW1804915w5m.las','SW1904306w5m.las',
            'SW2004508w5m.las','SW2205009w5m.las','SW2704412w5m.las','SW2805110w5m.las','SW3004306w5m.las',
            'SW3005110w5m.las','SW3204306w5m.las','SW3305110w5m.las','SW3504109w5m.las','SW3604307w5m.las',
        ]

        # All possible loaders where we can start sending messages
        # Must be of AbstractTopicLoader subclass
        loaders = [ProjectionLoader(las_directory=las_directory), ClipLoader(), AttributeLoader()]
        loader = self.pick_loader(topic, loaders)

        for las_file in las_files:
            tile_name = las_file.split('.las')[0]
            message = loader.create_message(tile_name)
            self.log(tile_name, 'Loaded')
            self.queue_channel.basic_publish('',
                                             topic,
                                             json.dumps(message).encode('utf-8'),
                                             self.rabbitmq_service.message_properties())
            self.progress_service.update_progress(tile_name,
                                                  f'Loaded by {loader.__class__.__name__}',
                                                  reset_start_time=True)

        self.queue_connection.close()

    def pick_loader(self, topic, loaders):
        """Given topic and set of loaders, choose the appropriate loader for the topic."""
        for loader in loaders:
            if loader.supports(topic):
                return loader

        raise NotImplementedError(f'Loader does not exist for topic {topic}')

    @staticmethod
    def log(las_name, message):
        print('Loader [{0}]: {1}'.format(las_name, message))
