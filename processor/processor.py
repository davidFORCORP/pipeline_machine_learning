"""Contains the Processor class."""
import abc
import json
import os
import signal
import tempfile
import time
import traceback
import uuid

from core.config import Config
from core.db_connection import DbConnection
from core.logger import Logger
from core.progress_service import ProgressService
from core.utils import file_utils


class Processor(metaclass=abc.ABCMeta):
    """
    Parent class for all processors. It provides the initial consumer/producer
    and sets up the common workflow where it consumes messages and then produces
    results onto the message queue pipeline.
    """

    def __init__(self, name, input_topic, output_topic, rabbitmq_service, config=None, terminate_on_idle=False):
        self.config = config
        if self.config is None:
            self.config = Config()

        # RabbitMQ
        self.rabbitmq_service = rabbitmq_service
        self.input_topic = input_topic
        self.output_topic = output_topic
        self.group_id = "ssim.pipeline.processor." + name
        self.queue_connection = None  # will be lazy-connected when we begin
        self.queue_channel = None  # will be lazy-connected when we begin
        self.heartbeat_thread = None

        # Misc utilities
        self.db_connection = DbConnection()
        self.logger = Logger(self.__class__.__name__)
        self.progress_service = ProgressService(self.db_connection)

        # The global working directory
        self.working_directory = self.config.get('global', 'working_directory')

        # The subdirectory used for persistent non-image-specific working files
        self.subdirectory = os.path.join(self.working_directory, name)

        # Output directory for image-specific output files
        self.output_directory = self.create_output_directory()

        # A temp working directory for temporary files
        self.temp_working_directory = self.create_temp_working_directory(name)

        # Terminate on idle option will kill this processor if no messages received
        self.idle_timeout = None
        if terminate_on_idle:
            self.idle_timeout = int(self.config.get('processor', 'idle_timeout'))

        # Every processor requires class variable "processor_name"
        if self.processor_name == None:
            raise NotImplementedError("Missing class variable processor_name in Processor subclass.")

    def begin(self):
        """
        Loops through every incoming message in the pipeline, processes each individual image,
        the emits a downstream message for other processors.
        """
        self.connect_to_rabbitmq()

        # Use a flag and an associated signal handler to try and catch when the user wants to stop the
        # infinite consumption loop. This allows us to do some cleanup during graceful exits.
        stop = False

        def handle_signal(signal, frame):
            self.log(None, "Signal has been caught and infinite consumption loop will soon end")
            nonlocal stop
            stop = True
            self.queue_channel.cancel()

        signal.signal(signal.SIGINT, handle_signal)

        for method, properties, body in \
                self.queue_channel.consume(self.input_topic, inactivity_timeout=self.idle_timeout):
            # CASE: Inactivity timeout
            # If this occurs, stop consuming messages and kill process
            if method is None and properties is None and body is None:
                break

            message = json.loads(body.decode('utf-8'))
            tile_name = message['tile_name']

            try:
                self.progress_service.update_progress(tile_name,
                                                      'Processing by [{group_id}]'.format(
                                                          group_id=self.group_id))

                downstream_message = self.process_incoming_message(message)
            except Exception as e:
                self.log(tile_name, e)
                traceback.print_exc()

                self.db_connection.reconnect()  # Make sure database connected before updating progress
                self.progress_service.update_progress(tile_name,
                                                      'Errored out during processing by [{group_id}]. '
                                                      'Re-queued to try again later.'.format(
                                                          group_id=self.group_id))

                self.queue_channel.basic_recover()
                time.sleep(10)

            else:
                if "skip_las" in downstream_message \
                        and downstream_message['skip_las'] is True:
                    self.progress_service.update_progress(tile_name,
                                                          'Successfully processed by [{group_id}]. '
                                                          'Halting image here due to \'skip_image\' flag.'
                                                          .format(group_id=self.group_id))
                    self.log(tile_name, "Processing successful; halting image due to 'skip_image' flag")
                elif self.output_topic:
                    self.progress_service.update_progress(tile_name,
                                                          'Successfully processed by [{group_id}]. '
                                                          'Sent downstream to next processor.'.format(
                                                              group_id=self.group_id))
                    self.log(tile_name, "Processing successful; sending downstream message")

                    # Send to default exchange, where message will be delivered to "output_topic" since it matches
                    # the routing key.
                    self.queue_channel.basic_publish("",
                                                     self.output_topic,
                                                     json.dumps(downstream_message).encode('utf-8'),
                                                     self.rabbitmq_service.message_properties())
                else:
                    self.progress_service.update_progress(tile_name, 'Completed')

                # We acknowledge after publishing to guarantee "at least once" delivery
                self.queue_channel.basic_ack(method.delivery_tag)
            finally:
                self.reset_temp_working_directory()

                if stop:
                    self.queue_channel.cancel()

        self.cleanup()

    def create_output_directory(self):
        """
        The final output directory.
        """
        output_directory = os.path.join(self.subdirectory, 'output')
        if not os.path.isdir(output_directory):
            os.makedirs(output_directory)
        return output_directory

    def create_temp_working_directory(self, subdirectory_name):
        """
        The temp directory is a workspace where we can throw intermediary files that will be deleted.
        """
        temp_working_directory = self.config.get('global', 'temp_working_directory')

        if temp_working_directory == '':
            temp_root = tempfile.gettempdir()  # OS-specific temp directory
            temp_working_directory = os.path.join(temp_root, 'ssim')

        """
        Use a unique identifier for this process so that it can safely delete it's 
        own temp directory without affecting other processes running on the same
        machine.
        """
        unique_processor_id = str(uuid.uuid4())

        temp_output_directory = os.path.join(temp_working_directory, subdirectory_name, unique_processor_id)

        self.log(None, 'Using [{}] as temporary working directory'.format(temp_output_directory))

        if not os.path.isdir(temp_output_directory):
            os.makedirs(temp_output_directory)

        return temp_output_directory

    def connect_to_rabbitmq(self):
        self.queue_connection = self.rabbitmq_service.connection()
        self.queue_channel = self.rabbitmq_service.channel(self.queue_connection)
        self.heartbeat_thread = self.rabbitmq_service.heartbeat_thread(self.queue_connection)
        self.logger.set_channel(self.queue_connection)
        self.log(None, "Connecting to RabbitMQ...")

        self.rabbitmq_service.ensure_queue_exists(self.queue_channel, self.input_topic)
        if self.output_topic:
            self.rabbitmq_service.ensure_queue_exists(self.queue_channel, self.output_topic)

        self.log(None, "Ready to receive images to process from RabbitMQ queue [{}]".format(self.input_topic))

    def reset_temp_working_directory(self):
        self.log(None, 'Deleting all files from temp working directory [{}]'.format(self.temp_working_directory))
        file_utils.delete_all_files_from_directory(self.temp_working_directory)

    def cleanup(self):
        """
        Clean up all the temporary files we've created and close all connections to external systems (db, rabbitmq, etc)
        """
        self.log(None, "Cleaning up all resources")
        temp_directory = self.temp_working_directory
        file_utils.delete_all_files_from_directory(temp_directory)
        os.rmdir(temp_directory)
        self.log(None, "Deleted temporary directory [{}]".format(temp_directory))
        self.db_connection.close()
        if self.queue_connection:
            self.queue_connection.close()


    @abc.abstractmethod
    def process_incoming_message(self, message):
        """
        Processes a message that was received from the previous processor in the pipeline.
        It should also return a message that will be sent downstream.
        """
        return

    @staticmethod
    def get_filename_without_extension(tile_name):
        return file_utils.get_filename_without_extension(tile_name)

    def copy_file_to_output_directory(self, source_file_absolute_path):
        """
        Copies a file to the output directory and returns a relative path that can be used
        in the messages from one processor to another.
        """
        filename = os.path.basename(source_file_absolute_path)

        file_utils.copy_file(source_file_absolute_path, self.output_directory)

        # Returns a relative path
        output_path = os.path.join(self.output_directory, filename)
        relative_path = os.path.relpath(output_path, start=self.working_directory)

        # Normalize slashes to all be forward-slashes since these paths are used
        # on both linux and windows
        return os.path.normpath(relative_path).replace('\\', '/')

    def log(self, tile_name, message):
        self.logger.log(tile_name, message)

    def get_temp_working_directory(self):
        return self.temp_working_directory
