"""Module that contains the Clip Processor."""
import os
import numpy as np

from core.laspy_service import LaspyService
from core.pdal_service import PdalService
from core.timing import timed_method
from core.queue import Queue
from processor.vision.vision_learning import VisionMachineLearning
from processor.processor import Processor

class VisionProcessor(Processor):
    """

    """

    processor_name = "vision"

    def __init__(self, rabbitmq_service, terminate_on_idle=False):
        Processor.__init__(self, self.processor_name, Queue().QUEUE_CLIP,
                           Queue().QUEUE_BARE_EARTH, rabbitmq_service, terminate_on_idle=terminate_on_idle)

    def process_incoming_message(self, message):
        relative_las_path = message["las_path"]
        tile_name = message["tile_name"]
        self.log(tile_name,"Began ML")

        # Downstream message for the next processor
        return {
            "tile_name": tile_name,
            "las_paths": clipped_las_file_paths,
            "clipped_extents": clipped_extents,
            "skip_las": not valid_las,
        }
    def perform(self, tile_name):

        """The values for params will depend on the specific problem you are trying to solve, as well as the size and complexity of your dataset. Here are some general guidelines for setting hyperparameters:

        hidden_layers: This should be a list of integers representing the number of neurons in each hidden layer of the neural network. The number of hidden layers and the number of neurons in each layer will depend on the complexity of your dataset and the amount of data you have. Generally, larger and more complex datasets will require more layers and neurons.

        dropout_rate: This is a hyperparameter that controls the amount of regularization in the neural network. A higher dropout rate will result in more regularization, which can help prevent overfitting. A good starting value for this parameter is usually around 0.2 or 0.3.

        optimizer: This is the optimization algorithm used to train the neural network. There are many different optimization algorithms to choose from, each with its own strengths and weaknesses. The most commonly used optimizer is Adam, which is a good default choice for most problems.

        loss_fn: This is the loss function used to evaluate the performance of the neural network during training. The choice of loss function will depend on the problem you are trying to solve. For binary classification problems, binary crossentropy is a common choice.

        epochs: This is the number of epochs (iterations over the entire dataset) to train the neural network for. The optimal number of epochs will depend on the complexity of your dataset and the amount of data you have. A good starting value is usually around 10-20.

        You may also want to experiment with other hyperparameters, such as the learning rate of the optimizer, the batch size used during training, and the number of features to select using the select_features function. It's generally a good idea to start with a small number of epochs and increase it gradually while monitoring the validation loss to prevent overfitting. You can also use techniques like early stopping to stop training early if the validation loss stops improving."""

        # spawn worker
        # # worker responsible for the creation of 1 ML model for 1 defined label
        # select a division for execution
        # execute ML
        # # pull data for ML
        # # # grab 1 Vision dataset from postgres and load to array
        # # create trained model
        # # # save model
        # # # save stats on current label
        # #
        # # test model on trained area for different locations
        # # track competitions
        # next tile


        # Define hyperparameters
        params = {
            'hidden_layers': [64, 64],
            'dropout_rate': 0.2,
            'optimizer': 'adam',
            'loss_fn': 'binary_crossentropy',
            'epochs': 10
        }

        # Create an instance of the YourClass class
        my_model = VisionMachineLearning(params)

        # Load the data
        data_path = 'path/to/my/data.csv'
        label_col = 'my_label'
        preds = my_model.predict(data_path, label_col)

        # Print the predictions
        print(preds)


        # Load the data and split into training and testing sets
        features, labels = my_model.load_data('my_data.csv', 'my_label')
        train_features, test_features, train_labels, test_labels = my_model.split_data(features, labels)

        # Select the best features
        k_best_features = my_model.select_features(train_features, train_labels, k=10)
        train_features = train_features[:, k_best_features]
        test_features = test_features[:, k_best_features]

        # Create the training and testing datasets
        train_dataset = my_model.make_datasets(train_features, train_labels, batch_size=32, num_epochs=10)
        test_dataset = my_model.make_datasets(test_features, test_labels, batch_size=32, num_epochs=1)

        # Train the model
        history = my_model.train_model(model, train_dataset, test_dataset, optimizer='adam', loss_fn='binary_crossentropy', epochs=10)

    def list_data_tiles(self, las_paths):

        all_tiles =



def plan02_coredata_accepted_columns(self):

    columns = ['abmi_site', 'accuracy', 'acq_date', 'acqtech', 'acres10tm', 'act_status', 'act_status1',
               'act_status2', 'act_status3', 'act_status4', 'actioncode1', 'actioncode2', 'actioncode3',
               'actioncode4', 'actiondescription1', 'actiondescription2', 'actiondescription3',
               'actiondescription4', 'activity_class', 'adc', 'addrcd', 'address1', 'address2', 'address3',
               'address4', 'address5', 'addrstatus', 'admin_area', 'adminarea', 'adminareac', 'adrangenid',
               'aenv_major', 'aenv_w_reg', 'afl_val', 'afr_val', 'ag', 'ag_srcfile', 'age', 'agent_code', 'aggroup',
               'agreearea', 'agreementn', 'agreementt', 'alias', 'allspecies', 'alt_name',
               'amendedapprovaldatetime', 'amldate', 'amndate', 'app_holder', 'app_no', 'appdate',
               'application_rate', 'applicationtype', 'applsubmissiondatetime', 'approval', 'as_dep_code', 'as_num',
               'asrdma_code', 'asrdma_name', 'assessment_datetime', 'assessment_hectares', 'atl_val', 'atr_val',
               'ats', 'att_stage', 'attacqtecc', 'attacqtech', 'attcredate', 'attention', 'attprovidc',
               'attprovide', 'attrevdate', 'AVG_SPEED', 'awcs', 'awr_code', 'awr_name', 'ba_code', 'basin',
               'beetle_yr', 'bf_id', 'bf1', 'bf2', 'bf3', 'bf4', 'bf5', 'bf6', 'bf7', 'bf8', 'bf9', 'bfnatpk_co',
               'bfnatpk_na', 'bh_fs_date', 'bh_hectares', 'bidire_ind', 'bndry_sour', 'bndry_source', 'bt_',
               'buff_dist', 'burn_class', 'burncode', 'c_parametr', 'calc_ha', 'calendar_year', 'can_date',
               'cancellationdatetime', 'candate', 'capture_da', 'category', 'categorydescription',
               'categorytypecode', 'cause', 'ccount', 'cd_', 'centroid_x', 'centroid_y', 'cfs_note1', 'cfs_note2',
               'cfs_ref_id', 'cgndb_id', 'ch_order', 'change_dat', 'city', 'city_code', 'city_name', 'class',
               'class_loc', 'clientcd', 'cmp', 'cmp_id', 'coalcatego', 'col', 'collec_dat', 'collec_src',
               'collection', 'comment', 'comments', 'commname_e', 'commname_f', 'common_nam', 'comp_name',
               'compaction', 'company', 'competition', 'compname', 'concise', 'cont_basin', 'contacts', 'contdate',
               'contour', 'control1', 'control2', 'control3', 'control4', 'cosewic_du', 'cosewic_du_code',
               'cosewic_id', 'country', 'cr_', 'create_dat', 'create_tim', 'create_timestamp', 'create_use',
               'create_userid', 'creationdate', 'creator', 'credate', 'csdname_l', 'csdname_r', 'csdtype_l',
               'csdtype_r', 'csduid_l', 'csduid_r', 'culpt_code', 'culpt_name', 'curexpiry', 'current_size',
               'cz_name', 'data_owner', 'dataowner', 'datasetnam', 'date_', 'date_approved', 'date_est',
               'date_loaded', 'date_retired', 'date_type', 'day', 'db1', 'db2', 'db3', 'decade', 'decision',
               'dep_enviro', 'deposit', 'descriptio', 'descriptor', 'desrep', 'desrepid', 'det_agent',
               'det_agent_type', 'dguid', 'dguidp', 'dimension', 'dir', 'directions', 'disc_flag',
               'discovered_date', 'disp_num', 'disp_type', 'disposit_1', 'dispositio', 'disposition_nbr', 'dl1',
               'dl2', 'dl3', 'dmg_code', 'dmg_desc', 'doc', 'drainage', 'drought', 'du1', 'du2', 'du3', 'east_10tm',
               'eco_grid', 'eco_group', 'eco_id', 'ecode', 'ecores_cod', 'ecores_nam', 'ecosite', 'ed_', 'edc',
               'edct', 'edit_date', 'editdate', 'edited', 'editor', 'editor_use', 'edits', 'edn', 'edtc', 'effdate',
               'effective', 'effectivedatetime', 'electric', 'electricc', 'elevation', 'eo_id', 'ep_code',
               'ep_name', 'erosion', 'es_code', 'es_name', 'esc_code', 'esc_name', 'esluz_code', 'esluz_name',
               'ewc_grid', 'ex_fs_date', 'ex_hectares', 'excess_moist', 'exitnbr', 'expdate', 'ext_coat',
               'f_area_ha', 'f_code', 'f5', 'f6', 'feat_len', 'feature__1', 'feature__2', 'feature_co',
               'feature_id', 'feature_ty', 'federal_rs', 'fid', 'field_2', 'field_23', 'field_24', 'field_code',
               'file_name', 'file_no__1', 'fin_drl_date', 'fire_class', 'fire_fighting_start_date',
               'fire_fighting_start_size', 'fire_id', 'fire_location_latitude', 'fire_location_longitude',
               'fire_name', 'fire_numbe', 'fire_number', 'fire_origin', 'fire_position_on_slope', 'fire_start_date',
               'fire_type', 'fire_year', 'firename', 'firenumber', 'fishmgmt_c', 'fishmgmt_n', 'fld_ctr_nm',
               'fluid_descrp', 'fluid_short_descrp', 'fma_code', 'fma_name', 'fma_num', 'fmu_code', 'fmu_name',
               'fname', 'followup', 'forest_are', 'form_no', 'fpa_hectar', 'from_fac', 'from_loc', 'ft_',
               'fuel_type', 'fwdist_cod', 'fwdist_nam', 'g_group2', 'g_group3', 'g1', 'g2', 'g3', 'gauge', 'gaugec',
               'gb_popunit', 'gbwu', 'gcontent', 'gencmt', 'general_cause_desc', 'generic', 'genetic_gr',
               'geo_date', 'geo_field', 'geo_pool', 'geo_source', 'geo_type', 'geoaccura', 'geoacqtecc',
               'geoacqtech', 'geocredate', 'geoname', 'geoprovidc', 'geoprovide', 'georevdate', 'gis_type',
               'globalid', 'graphic_ty', 'grid10kmid', 'groundelev', 'gvolume', 'gwa_code', 'gwa_name', 'h2s_cont',
               'h2s_r_levl', 'h2s_r_volm', 'ha10tm', 'habitat', 'hdd_ind', 'herbicide', 'herbicide_comments',
               'herd', 'herd_code', 'herd_name', 'herd_no', 'herd_plan', 'hfi_id', 'histreservationnumber',
               'hrange_cod', 'hrange_nam', 'hrv', 'huc_10', 'huc_2', 'huc_4', 'huc_6', 'huc_8', 'humidity', 'id',
               'id_code', 'id_name', 'identif', 'identifier', 'in_range', 'industry_identifier_desc', 'infest',
               'infest_id', 'infest1', 'infest2', 'infest3', 'infest4', 'initial_action_by', 'int_coat', 'intent',
               'ires_code', 'ires_name', 'irplo_code', 'irplo_name', 'is_neb', 'iucn', 'kbe', 'kind', 'l_adddirfg',
               'l_hnumf', 'l_hnuml', 'l_placenam', 'l_stname_c', 'label', 'land_use', 'land_use_t', 'landarea',
               'language', 'last_obs_d', 'last_occyr', 'last_updat', 'lastupdate', 'lat', 'lat_d', 'lat_md',
               'latitude', 'layer', 'legaldesig', 'lf1', 'lf10', 'lf11', 'lf12', 'lf13', 'lf14', 'lf15', 'lf2',
               'lf3', 'lf4', 'lf5', 'lf6', 'lf7', 'lf8', 'lf9', 'lha', 'lic_li_no', 'licappdate', 'liccode',
               'licdate', 'licence', 'licence_no', 'licensee_code', 'licno', 'licstatdat', 'licstatdat_dt',
               'licstatus', 'line_no', 'liner_grd', 'liner_type', 'lithology', 'lname', 'lno', 'loadate',
               'locat_ind', 'location', 'locsf', 'long', 'long_d', 'long_md', 'longitude', 'longtitude', 'ls',
               'lsd', 'luf_code', 'luf_name', 'm', 'maj_subwat', 'management', 'map_code', 'map_label',
               'map_method', 'map_source', 'maparea', 'material', 'md', 'md_code', 'md_name', 'mdep1', 'mdep2',
               'mdep3', 'mer', 'metacover', 'metis_code', 'metis_name', 'mf1', 'mf2', 'mf3', 'mf4', 'mf5', 'mf6',
               'mf7', 'mf8', 'mf9', 'mill_name', 'mill_type', 'mine_meth', 'mine_name', 'mine_no', 'mine_owner',
               'mine_stat', 'mine_type', 'mineraltyp', 'mintype', 'mix_amount', 'mode_descrp', 'mode_short_descrp',
               'modifier', 'modifier_2017to2018', 'modifier_ft', 'modifier_hfi_2017_year', 'modifier_year',
               'moist_es', 'month', 'more_info', 'mpb_s_site', 'mpb_s_site_id', 'mt_', 'name', 'name_eng',
               'natarea_co', 'natarea_na', 'nbrlanes', 'nearwater', 'new_dmg_co', 'new_dmg_code', 'ngd_uid', 'nid',
               'nom', 'nom_fra', 'north_10tm', 'notes', 'nr_', 'nrc', 'nrn', 'nrname', 'ns_', 'nsn', 'nsr',
               'nsr2005', 'nsrcode', 'nsrname', 'nts', 'nts_snrc', 'num_trees', 'numtracks', 'nutr_es', 'observer',
               'oc_date', 'oc_name', 'oc_no', 'ogc_fid', 'op_code', 'operatoena', 'operator_code', 'operatormk',
               'opsubend', 'opsubstart', 'opsubudis', 'opsubudisc', 'opsurvprov', 'orc_primry', 'orc_scndry',
               'order2', 'order3', 'org_issued', 'orgarea', 'orig_licno', 'origlin_no', 'origpsppid',
               'os_area_code', 'os_dep_code', 'osa', 'other_fuel_type', 'out_date', 'out_diamet', 'owner_land',
               'ownerena', 'paired', 'par_as_num', 'parent_id', 'parentcomp', 'park_class', 'park_code',
               'park_name', 'participan', 'parts', 'pasites_id', 'path', 'pavstatus', 'pavsurf', 'pcclass',
               'pcname', 'pcpuid', 'pctype', 'pcuid', 'percent', 'perim10tm', 'perimeter', 'permit_detail_desc',
               'permit_no', 'permt_appr', 'permt_expi', 'phase', 'pip_materl', 'pipe_envir', 'pipe_grade',
               'pipe_maop', 'pipe_type', 'piptechstd', 'pl_spec_id', 'plan', 'play_area', 'pllicsegid',
               'plrec_code', 'plrec_name', 'plrt_code', 'plrt_name', 'pluz_code', 'pluz_name', 'pm1', 'pm2', 'pm3',
               'pmchem1', 'pmchem2', 'pmchem3', 'pmtex1', 'pmtex2', 'pmtex3', 'poly_date', 'poly_id', 'poly_perc',
               'pool_code', 'population', 'postalcode', 'pra_code', 'pra_name', 'pre_app_na', 'preabbr', 'prename',
               'prfabbr', 'prfname', 'prname', 'prname_l', 'prname_r', 'pro_date', 'prod_field', 'prod_pool',
               'profile', 'project', 'project_na', 'project_name', 'prot_code', 'prot_desg', 'prot_name',
               'prov_name', 'prov_terr', 'provider', 'province', 'pruid', 'pruid_l', 'pruid_r', 'ps_flow',
               'psp_group', 'purpcd', 'purposetypecode', 'purposetypedescription', 'purptxt', 'qs', 'qt',
               'r_adddirfg', 'r_hnumf', 'r_hnuml', 'r_placenam', 'r_stname_c', 'ra', 'range', 'rank', 'rast',
               'ratinglev', 'reasoncode', 'reasondescription', 'receivedda', 'ref_src', 'ref1', 'ref2', 'ref3',
               'ref4', 'ref5', 'region', 'regulator', 'regulatorc', 'reidate', 'rel', 'rel_scale', 'remarktext',
               'remarktype', 'rendate', 'rep_date', 'reported_date', 'res_cat', 'res_type', 'reservationnumber',
               'reserve', 'resp_level', 'responsible_group_desc', 'restcd', 'restex1', 'restex2', 'restex3',
               'restex4', 'restr_type', 'restrict_1', 'restrict_2', 'restrict_3', 'restrictio', 'resurvey',
               'revdate', 'rfma_code', 'rfma_name', 'rgb', 'rge', 'rid', 'rip', 'rng', 'roadclass', 'roadsegid',
               'rockymtn_c', 'rockymtn_n', 'rootrestri', 'row', 'rsplvl_id', 'rtename1en', 'rtename1fr',
               'rtename2en', 'rtename2fr', 'rtename3en', 'rtename3fr', 'rtename4en', 'rtename4fr', 'rtnumber1',
               'rtnumber2', 'rtnumber3', 'rtnumber4', 'rtnumber5', 'ruleid', 'rutting', 's_group2', 's_group3',
               's_rank', 's1', 's2', 's3', 'sara_statu', 'sbflag', 'sch_descri', 'sch_status', 'sch_sub_ty',
               'sche_name', 'scheme_no', 'scheme_sub_type', 'scheme_typ', 'scheme_type', 'scientific', 'sciname',
               'scomname', 'scontent', 'se1', 'se2', 'se3', 'sec', 'sect_name', 'sector_hfie', 'sectorcode1',
               'sectorcode2', 'sectorcode3', 'sectorcode4', 'sectorname1', 'sectorname2', 'sectorname3',
               'sectorname4', 'securclasc', 'securclass', 'seedzone', 'seg_length', 'seg_status', 'settle_cod',
               'settle_nam', 'sev', 'sevdesc', 'severity', 'site_cnt', 'site_comments', 'site_name', 'size',
               'size_class', 'sl1', 'sl2', 'sl3', 'slope', 'slu1', 'slu2', 'slu3', 'sname', 'soil_code', 'soil_id',
               'soil_temp', 'soilname', 'source', 'source_id', 'source_key', 'sourceid', 'sp1', 'sp1_control',
               'sp1_control_other', 'sp1_density', 'sp1_distribution', 'sp1_other', 'sp1_percent', 'sp2',
               'sp2_control', 'sp2_control_other', 'sp2_density', 'sp2_distribution', 'sp2_other', 'sp2_percent',
               'sp3', 'sp3_control', 'sp3_control_other', 'sp3_density', 'sp3_distribution', 'sp3_other',
               'sp3_percent', 'species', 'species_co', 'species_li', 'species1', 'species2', 'species3', 'species4',
               'specvers', 'speedfreit', 'speedpasse', 'spmun_code', 'spmun_name', 'src_agency', 'src_agy2',
               'srd_priori', 'srid', 'srv_gen_pl', 'ssg1', 'ssg2', 'start_for_fire_date', 'stat_flag', 'statcd',
               'status', 'statusc', 'statuscode', 'stone', 'strat_int', 'stresslevl', 'struc_descrp',
               'struc_short_descrp', 'structid', 'structtype', 'strunameen', 'strunamefr', 'studylevel', 'stype1',
               'stype2', 'sub_contnl', 'sub1unitdc', 'sub1unitdi', 'sub2unitdc', 'sub2unitdi', 'subd1end',
               'subd1start', 'subd2end', 'subd2start', 'subdi1name', 'subdi1nid', 'subdi2name', 'subdi2nid',
               'substance1', 'substance2', 'substance3', 'subtype', 'sum_area', 'surfaceacc', 'surfloc',
               'surv_year', 'survey_date', 'surveyyear', 'survyear', 'svolume', 'syllabic', 'symbol', 'symid',
               'symptom', 't1', 't2', 't3', 'target_fid', 'targetsubs', 'taxon', 'temperature', 'tempsurfpl',
               'term_date', 'termdate', 'texture', 'tfc_sp1', 'tfc_sp2', 'tfc_sp3', 'tfc_sp4', 'thickness',
               'tile_name', 'time', 'time_applied', 'to_fac', 'to_fs_date', 'to_hectares', 'to_loc', 'totalvolum',
               'town_code', 'town_name', 'trackclasc', 'trackclass', 'trackname', 'tracksegid', 'tract',
               'transptypc', 'transptype', 'treaty_cod', 'treaty_nam', 'trees', 'trkusr1ena', 'trkusr1rmk',
               'trkusr2ena', 'trkusr2rmk', 'trkusr3ena', 'trkusr3rmk', 'trkusr4ena', 'trkusr4rmk', 'trm',
               'true_cause', 'twn', 'twp', 'type', 'type_descrp', 'type_short_descrp', 'typedescri', 'typename',
               'uc_fs_date', 'uc_hectares', 'unburn_rem', 'uniq_num', 'unique_no', 'unit_desc', 'unitofspec',
               'unitofspee', 'unpavsurf', 'update_dat', 'update_flag', 'update_tim', 'update_use', 'upland',
               'usa_code', 'usa_name', 'usetype', 'usetypec', 'utm', 'uwi_display', 'uwi_id', 'v1', 'v2', 'v3',
               'ver_date', 'version', 'versionenddate', 'vill_code', 'vill_name', 'vintage', 'wall_thick',
               'wam_date', 'water_rem', 'waterbody', 'watersheds', 'watertbl', 'wb', 'wd',
               'weather_conditions_over_fire', 'web_link', 'well_depth', 'well_id', 'well_name', 'well_stat',
               'well_stat_code', 'well_stat_date', 'wetlandclass', 'wf', 'width', 'wildarea_c', 'wildarea_n',
               'wildpark_c', 'wildpark_n', 'wind_direction', 'wind_speed', 'windthrow', 'wkb_geometry', 'wld',
               'wlpark_cod', 'wlpark_nam', 'wm', 'wma_name', 'wmf', 'wmunit_cod', 'wmunit_code', 'wmunit_nam',
               'wmunit_name', 'wo', 'ws', 'wsc_code', 'wsc_region', 'x', 'y', 'year', 'year_source', 'zone',
               'zone_id', 'zone_prior', 'zonedesc']

    return columns