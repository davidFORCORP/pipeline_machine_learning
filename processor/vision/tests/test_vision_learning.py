"""
Contains class and methods for testing VisionMachineLearning.
"""
import os
import unittest
from distutils.dir_util import copy_tree

from core.config import Config
from processor.vision.vision_processor import VisionProcessor
from core.tests.test_rabbitmq_service import TestRabbitMqService
from core.utils import file_utils


class TestVisionMachineLearning(unittest.TestCase):
    """
    Class with methods that test ClipProcessor.

    So far only contains integration tests for ClipProcessor.
    """

    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName)
        config = Config()
        working_directory = config.get('global', 'working_directory')
        self.fixtures_directory = os.path.join(working_directory, 'clip/tests/fixtures')

        self.tile_name = 'test_fixture_NE0106011w5m'
        self.output_directory = os.path.join(working_directory, 'clip/tests/output')
        file_utils.delete_all_files_from_directory(self.output_directory)


    def setUp(self) -> None:
        rabbitmq_service = TestRabbitMqService()
        self.vision_processor = VisionProcessor(rabbitmq_service)

    def tearDown(self) -> None:
        # Copy all generated files into the output directory and reset the temp directory
        copy_tree(self.vision_processor.get_temp_working_directory(), self.output_directory)
        self.vision_processor.cleanup()

    def testClipMajorUnit(self):
        # tile_extent = self.laspy_service.get_2d_extent(self.laspy_file)
        self.vision_processor.perform(self.tile_name)
