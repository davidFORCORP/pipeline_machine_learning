from random import random
from tensorflow.keras.layers import Dropout
from sklearn.feature_selection import SelectKBest, f_classif
from sklearn.model_selection import train_test_split
from tensorflow.keras.layers import Dense
from tensorflow.keras.models import Sequential
from tensorflow.keras.utils import normalize
from tensorflow.data import Dataset
from tensorflow.keras.backend import clear_session
from tensorflow.keras.callbacks import EarlyStopping
import numpy as np
import pandas as pd
from itertools import combinations
import os, time
from num2words import num2words
from transformers import pipeline, Trainer, TrainingArguments
from core.db_connection import DbConnection
####https://wizardforcel.gitbooks.io/deep-learning-keras-tensorflow/content/Requirements.html








class VisionMachineLearning():

    def __init__(self, params):
        self.params = params
        self.model = None
        self.current_dependent

    def perform(self, data_dict):
        data, labels = self.load_data(data_dict, label=self.current_dependent, data_type='dictionary')
        training_features, testing_features, training_labels, testing_labels = self.split_data(data, labels)
        self.run_multiple_experiments(self.params, training_features, val_dataset, test_dataset, results_csv

    def load_data(self, data, label, data_type="csv", **kwargs):
        """
        Loads data from either a CSV file or a dictionary.

        Args:
            data (str or dict): If `data_type` is "csv", this is the path to the CSV file. If `data_type` is "dict", this is
                the dictionary containing the data.
            label (str): The name of the column containing the labels.
            data_type (str, optional): Either "csv" or "dict". Defaults to "csv".
            **kwargs: Additional arguments to be passed to the CSV loading function (if `data_type` is "csv").

        Returns:
            tuple: A tuple containing the data and labels, where the data is a NumPy array of shape `(num_samples, num_features)`
                and the labels are a NumPy array of shape `(num_samples,)`.
        """

        if data_type == "csv":
            data = pd.read_csv(data, **kwargs)

        data_cols = list(data.columns)
        data_cols.remove(label)

        return data[data_cols].values, data[label].values

    def normalize_data(self, data):
        return normalize(data, axis=1)

    def split_data(self, features, labels, test_size=0.2):
        """Splits data into training and testing sets.

        Args:
            features (ndarray): The features numpy array.
            labels (ndarray): The labels numpy array.
            test_size (float): The proportion of data to use for testing.

        Returns:
            Four numpy arrays: the training features, testing features, training labels, and testing labels.
        """
        return train_test_split(features, labels, test_size=test_size, random_state=42)

    def select_features(self, features, labels, k):
        """Selects the k best features according to the f_classif function.

        Args:
            features (ndarray): The features numpy array.
            labels (ndarray): The labels numpy array.
            k (int): The number of features to select.

        Returns:
            The indices of the k best features.
        """
        selector = SelectKBest(f_classif, k=k)
        selector.fit(features, labels)
        return selector.get_support(indices=True)

    def build_model(self, data_shape, num_classes, hidden_layers, dropout_rate):
        model = Sequential()
        model.add(Dense(hidden_layers[0], activation='relu', input_shape=data_shape))
        model.add(Dropout(dropout_rate))

        for i in range(1, len(hidden_layers)):
            model.add(Dense(hidden_layers[i], activation='relu'))
            model.add(Dropout(dropout_rate))

        model.add(Dense(num_classes, activation='softmax'))

        return model

    def predict(self, data, label):
        """
        Make predictions on new data using a trained model.

        Args:
            data (str or dict): If `data_type` is "csv", this is the path to the CSV file. If `data_type` is "dict", this is
                the dictionary containing the data.
            label (str): The name of the column containing the labels.

        Returns:
            An ndarray of predicted labels.
        """
        # Load the data
        features, labels = self.load_data(data, label)

        # Normalize the data
        features = self.normalize_data(features)

        # Select the best features
        if 'k' in self.params:
            features = features[:, self.select_features(features, labels, self.params['k'])]

        # Split the data into training and testing sets
        X_train, X_test, y_train, y_test = self.split_data(features, labels)

        # Build the model
        if not self.model:
            self.model = self.build_model(X_train.shape[1:], len(np.unique(labels)), self.params['hidden_layers'], self.params['dropout_rate'])

        # Train the model
        self.train_model(self.model, X_train, X_test, self.params['optimizer'], self.params['loss_fn'], self.params['epochs'], self.params.get('early_stopping'))

        # Make predictions on the test set
        return self.model.predict(X_test)

    def make_datasets(self, data, label_col, batch_size, num_epochs):
        # Extract features and labels from the data
        features = data.drop([label_col], axis=1)
        labels = data[label_col]

        # Normalize the features
        features_normalized = normalize(features.values, axis=1)

        # Create a TensorFlow dataset
        dataset = Dataset.from_tensor_slices((features_normalized, labels))

        # Shuffle and batch the dataset
        dataset = dataset.shuffle(len(data)).batch(batch_size)

        # Repeat the dataset for the specified number of epochs
        dataset = dataset.repeat(num_epochs)

        return dataset

    def train_model(self, model, train_dataset, val_dataset, optimizer, loss_fn, epochs, early_stopping=None):
        model.compile(optimizer=optimizer, loss=loss_fn, metrics=['accuracy'])

        callbacks = []
        if early_stopping:
            callbacks.append(early_stopping)

        history = model.fit(train_dataset, validation_data=val_dataset, epochs=epochs, callbacks=callbacks)

        return history

    def test_model(self, model, test_dataset):
        loss, accuracy = model.evaluate(test_dataset)
        return loss, accuracy

    def run_experiment(self, data, label, params):
        # Load the data
        features, labels = self.load_data(data, label)

        # Normalize the data
        features = self.normalize_data(features)

        # Select the best features
        if 'k' in params:
            features = features[:, self.select_features(features, labels, params['k'])]

        # Split the data into training and testing sets
        X_train, X_test, y_train, y_test = self.split_data(features, labels)

        # Build the model
        model = self.build_model(X_train.shape[1:], len(np.unique(labels)), params['hidden_layers'], params['dropout_rate'])

        # Train the model
        self.train_model(model, self.make_datasets(X_train, y_train, params['batch_size'], params['num_epochs']),
                         self.make_datasets(X_test, y_test, params['batch_size'], params['num_epochs']),
                         params['optimizer'], params['loss_fn'], params['epochs'], params.get('early_stopping'))

        # Test the model
        test_loss, test_acc = self.test_model(model, self.make_datasets(X_test, y_test, params['batch_size'], 1))

        return test_acc





class ExperimentRunner:
    def __init__(self, model_class):
        self.model_class = model_class
        self.model = None
        self.history = None
        self.train_scores = None
        self.test_scores = None

    def run_experiment(self, dataset, label_col, params):
        self.model = self.model_class(params)
        self.history, self.train_scores, self.test_scores = self.model.fit(dataset, label_col)
        return self.model, self.history, self.train_scores, self.test_scores

    def predict(self, dataset, label_col):
        return self.model.predict(dataset, label_col)

    def save_model(self, save_path):
        self.model.save(save_path)

    def run_multiple_experiments(self, num_experiments, data_paths, label_col, params, save_path=None):

        results = []

        for i in range(num_experiments):
            experiment_name = f"{label_col}_experiment_{i+1}"

            # Randomly choose a data path
            data_path = random.choice(data_paths)

            # Load dataset
            dataset = load_data(data_path)

            model, history, train_scores, test_scores = self.run_experiment(dataset, label_col, params)
            self.save_model(f"{experiment_name}_model.h5")

            # Make predictions on the test set
            preds = self.predict(dataset, label_col)

            # Evaluate performance
            accuracy = accuracy_score(self.model.y_test, preds)
            precision = precision_score(self.model.y_test, preds, average='weighted')
            recall = recall_score(self.model.y_test, preds, average='weighted')
            f1 = f1_score(self.model.y_test, preds, average='weighted')
            mse = mean_squared_error(self.model.y_test, preds)
            mae = mean_absolute_error(self.model.y_test, preds)
            r2 = r2_score(self.model.y_test, preds)

            # Save results to list
            result = {
                'experiment_name': experiment_name,
                'data_path': data_path,
                'params': params,
                'test_loss': self.test_scores[0],
                'test_acc': self.test_scores[1],
                'accuracy': accuracy,
                'precision': precision,
                'recall': recall,
                'f1': f1,
                'mse': mse,
                'mae': mae,
                'r2': r2,
                'preds': preds.tolist(),
                'y_test': self.model.y_test.tolist()
            }
            results.append(result)

            # Print results
            print(f"Experiment {i+1} - Accuracy: {accuracy:.4f}, Precision: {precision:.4f}, Recall: {recall:.4f}, F1 Score: {f1:.4f}")

        # Save results to file if specified
        if save_path:
            with open(save_path, 'w') as f:
                json.dump(results, f)

        return results






































class Helper():
    def __init__(self):
        pass

    def print_stats(self, itter, mean_out_con, stddev_out_con, mean_out_dec, stddev_out_dec, mean_out, stddev_out):
        date_ = time.strftime("%a, %d %b %Y")
        time_ = time.strftime("%H:%M")
        print('Current Model Statistics on {date_} @ {time_} \n'
              '             ITTERATION {itter} \n'
              '      CURRENT MODEL ACCURACY = {accuracy}% \n'
              '+ Mean Conifer Outage: {mean_out_con} \n'
              '+ Standard Deviation of Conifer Outage: {stddev_out_con} \n'
              '+ Mean Deciduous Outage: {mean_out_dec} \n'
              '+ Standard Deviation of Deciduous Outage: {stddev_out_dec} \n'
              '+ Mean Total Outage: {mean_out} \n'
              '+ Standard Deviation of Total Outage: {stddev_out}'.format(date_=date_, time_=time_, itter=itter, mean_out_con=mean_out_con, stddev_out_con=stddev_out_con, mean_out_dec=mean_out_dec, stddev_out_dec=stddev_out_dec, mean_out=mean_out, stddev_out=stddev_out, accuracy=round(accuracy*100)))

class ChatGptImplementation():
    def __init__(self):
        pass
    """
    One popular library is Hugging Face's Transformers, which allows for easy fine-tuning of pre-trained models, including ChatGPT, on custom datasets. The library also provides a simple API for using the fine-tuned models for various NLP tasks such as text classification, language generation, and more.
    
    Here is an example of how you can use the Transformers library to fine-tune a pre-trained ChatGPT model on your dataset
    """
    def chatgpt_implemenation_a(self, train_dataset):
        # Define the model and the training arguments
        model = "microsoft/DialoGPT-medium"
        training_args = TrainingArguments(
            output_dir='./results',
            evaluation_strategy="steps",
            eval_steps=100,
            save_steps=100,
            overwrite_output_dir=True,
            num_train_epochs=1,
            per_device_train_batch_size=1,
            per_device_eval_batch_size=1,
            logging_dir="./logs",
            logging_steps=100,
            logging_level="info",
        )

        # Fine-tune the model
        trainer = Trainer(model_type='gpt2', model_name=model, args=training_args)
        trainer.train(train_dataset=train_dataset)


        """You can also use this library to use the fine-tuned model for various NLP tasks such as text generation and answering questions."""

        from transformers import pipeline

        # Use the fine-tuned model for text generation
        text_generator = pipeline("text-generation", model="./results")
        generated_text = text_generator("The forest is a place of", max_length=2048)

        # Use the fine-tuned model for question answering
        question_answerer = pipeline("question-answering", model="./results")
        answer = question_answerer({
            'question': 'What is the main function of forests?',
            'context': 'Forests provide numerous ecosystem services such as carbon sequestration, habitat for wildlife, and water regulation'
        })


    def chatgpt_implemenation_b(self, train_dataset): #OR

        """Here is an example of a Python script that demonstrates how you can use the Transformers library to fine-tune a pre-trained ChatGPT model on your dataset in csv format and use the fine-tuned model for text generation:"""


        # Read the CSV file into a dataframe
        df = pd.read_csv("your_data.csv")

        # Convert the dataframe into a list of strings
        texts = df['text_column'].tolist()

        # Prepare the dataset for training
        train_dataset = [{'input_text': text} for text in texts]

        # Define the model and the training arguments
        model = "microsoft/DialoGPT-medium"
        training_args = TrainingArguments(
            output_dir='./results',
            evaluation_strategy="steps",
            eval_steps=100,
            save_steps=100,
            overwrite_output_dir=True,
            num_train_epochs=1,
            per_device_train_batch_size=1,
            per_device_eval_batch_size=1,
            logging_dir="./logs",
            logging_steps=100,
            logging_level="info",
        )

        # Fine-tune the model
        trainer = Trainer(model_type='gpt2', model_name=model, args=training_args)
        trainer.train(train_dataset=train_dataset)

        # Use the fine-tuned model for text generation
        text_generator = pipeline("text-generation", model="./results")
        generated_text = text_generator("The forest is a place of", max_length=2048)
        print(generated_text)

    def prepare_dataset_for_chatgpt(self, voxel_dict, tile_name):

        pattern_command_start = 'in forcorp vision forest the pattern describing species is '

        # loop all fields and create pattern text
        number_of_columns_as_index = len(voxel_dict.items())
        for column in range(number_of_columns_as_index):
            # for column_info in range(number_of_columns_as_index):
            if column == 4:
                continue
            for key, item in voxel_dict.items():
                text_to_add = pattern_command_start + str(1)

        table = 'ssim_pipeline_' + tile_name + '.' + tile_name + '_avi'

        command = f"SELECT " \
                  f"'the AVI polygon number (poly_num) is ' || poly_num, " \
                  f"'the AVI species 1 estimate is '|| sp1 as sp1, " \
                  f"'the AVI species 1 percent coverage is ' || sp1_per, " \
                  f"'the AVI species 2 estimate is '|| sp2 as sp2, " \
                  f"'the AVI species 2 percent coverage is ' || sp2_per, " \
                  f"'the AVI species 3 estimate is '|| sp3 as sp3, " \
                  f"'the AVI species 3 percent coverage is ' || sp3_per, " \
                  f"'the AVI species 4 estimate is '|| sp4 as sp4, " \
                  f"'the AVI species 4 percent coverage is ' || sp4_per, " \
                  f"'the AVI species 5 estimate is '|| sp5 as sp5, " \
                  f"'the AVI species 5 percent coverage is ' || sp5_per, " \
                  f"'the AVI understory species 1 estimate is '|| usp1 as usp1, " \
                  f"'the AVI understory species 1 percent coverage is ' || usp1_per, " \
                  f"'the AVI understory species 2 estimate is '|| usp2 as usp2, " \
                  f"'the AVI understory species 2 percent coverage is ' || usp2_per, " \
                  f"'the AVI understory species 3 estimate is '|| usp3 as usp3, " \
                  f"'the AVI understory species 3 percent coverage is ' || usp3_per, " \
                  f"'the AVI understory species 4 estimate is '|| usp4 as usp4, " \
                  f"'the AVI understory species 4 percent coverage is ' || usp4_per, " \
                  f"'the AVI understory species 5 estimate is '|| usp5 as usp5, " \
                  f"'the AVI understory species 5 percent coverage is ' || usp5_per, " \
                  f"'the stand originated in ' || text(origin) stand_origin, " \
                  f"CASE " \
                  f" WHEN lower(density) = 'a' THEN 'crown closure is class A' " \
                  f"WHEN lower(density) = 'b' THEN 'crown closure is class B' " \
                  f"WHEN lower(density) = 'c' THEN 'crown closure is class C'  " \
                  f"WHEN lower(density) = 'd' THEN 'crown closure is class D'  " \
                  f"ELSE 'no crown closure recorded' END crown_closure, " \
                  f"CASE " \
                  f" WHEN lower(udensity) = 'a' THEN 'understory crown closure is class A'  " \
                  f"WHEN lower(udensity) = 'b' THEN 'understory crown closure is class B'  " \
                  f" WHEN lower(udensity) = 'c' THEN 'understory crown closure is class C'  " \
                  f"WHEN lower(udensity) = 'd' THEN 'understory crown closure is class D'  " \
                  f"ELSE 'no understory crown closure recorded' END crown_closure, " \
                  f"CASE " \
                  f"WHEN lower(tpr) = \'g\' THEN 'the timber productivity rating is Good'  " \
                  f"WHEN lower(tpr) = \'m\' THEN 'the timber productivity rating is Moderate' " \
                  f"WHEN lower(tpr) = \'f\' THEN 'the timber productivity rating is Fair'  " \
                  f"WHEN lower(tpr) = \'u\' THEN 'the timber productivity rating is Un-Productive' " \
                  f"ELSE 'no timber productivity rating recorded' END timber_productivity_rating, " \
                  f"'the natural sub region the stand is in is' || lower(nsrname) nsr " \
                  f"FROM {table}"

        return self.db_connection.pgsql_fetch(command, True)

    #
    #
    #     """
    #     itter = 0
    #     mean_out_con, stddev_out_con, mean_out_dec, stddev_out_dec, mean_out, stddev_out = StatisticsTest().get_stats()
    #     Helper().print_stats(itter, mean_out_con, stddev_out_con, mean_out_dec, stddev_out_dec, mean_out, stddev_out)
    #
    #     # Now implement ML
    #     while mean_out > 0.02 and mean_out_con > 0.2 and mean_out_dec > 0.2:  ## and stddev_out_con > 1: ## what are the success indicators and levels?
    #         CSVCreator().perform(itter)
    #         accuracy = MLrun().perform(itter)  ## we may want to reduce outage to nearest 10, 100, or even 1000 to start with - I imagine that there is a loss in population size as you get closer to actual outage -
    #         ## This may as well give some freedom to the model to be over or under lims volumes which may be incorrect
    #         UpdateSSI().perform()
    #         mean_out_con, stddev_out_con, mean_out_dec, stddev_out_dec, mean_out, stddev_out = StatisticsTest().get_stats()
    #         Helper().print_stats(itter, mean_out_con, stddev_out_con, mean_out_dec, stddev_out_dec, mean_out, stddev_out)
    #
    #         itter += 1
    #     """
