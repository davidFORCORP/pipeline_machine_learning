from core.rabbitmq_service import RabbitMqService
from processor.vision.vision_learning import VisionMachineLearning


def begin_vision_process():
    rabbitmq_service = RabbitMqService()
    stem_finder_processor = VisionMachineLearning(rabbitmq_service)
    stem_finder_processor.begin()

if __name__ == "__main__":
    begin_vision_process()
