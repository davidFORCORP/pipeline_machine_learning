class DbConnectionReconnectError(Exception):
    """
    Exception raised if the db_connection class re-connected and "exception_on_reconnect" flag is set
    """

    def __init__(self, message):
        self.message = message
