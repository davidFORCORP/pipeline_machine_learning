"""Contains the ProgressService class."""
from datetime import datetime

from core.config import Config
from core.db_connection import DbConnection


class ProgressService:
    """
    Tracks the progress of individual images as they make their way
    through the pipeline.
    """

    def __init__(self, db_connection):
        self.db_connection = db_connection
        config = Config()

        self.schema = config.get("db", "schema")
        # Commenting out this query for now because it causes multiple attribute processors to lock up
        self.ensure_progress_table_exists()

    def ensure_progress_table_exists(self):
        """
        Creates the per-image progress tracking table if it doesn't exist.
        """
        query = f"""
                    CREATE TABLE IF NOT EXISTS {self.schema}.tile_progress(
                        id serial primary key,
                        tile_name text unique,
                        state text,
                        start_time timestamp without time zone,
                        last_state_change timestamp without time zone
                    );
                """
        self.db_connection.execute(query)

    def update_progress(self, tile_name, new_state, reset_start_time=False):
        current_timestamp = DbConnection.epoch_to_postgresql_timestamp(datetime.now())

        query = f"""
                    INSERT INTO {self.schema}.tile_progress (tile_name, state, start_time, last_state_change)
                    VALUES (%s, %s, %s, %s)
                    ON CONFLICT (tile_name) DO UPDATE 
                        SET state = EXCLUDED.state, 
                            last_state_change = EXCLUDED.last_state_change
                """

        if reset_start_time:
            query = query + ", start_time = EXCLUDED.start_time"

        self.db_connection.execute(query, (tile_name, new_state, current_timestamp, current_timestamp))
