import os
import platform
from configparser import ConfigParser


class Config(object):
    """
    Reads from the settings.ini file and provides the configuration
    to the rest of the application
    """

    def __init__(self):
        self.config = ConfigParser()
        self.config.read(self.get_settings_file_path("settings.ini"))

        # Operating system overrides
        operating_system = platform.system()
        if operating_system == "Windows":
            self.config.read(self.get_settings_file_path("settings.windows.ini"))
        elif operating_system == "Linux":
            self.config.read(self.get_settings_file_path("settings.linux.ini"))

        # Local overrides that are not stored in version control
        self.config.read(self.get_settings_file_path("settings.local.ini"))

    def get(self, section, option):
        return self.config.get(section, option)

    @staticmethod
    def get_settings_file_path(name):
        return os.path.abspath(os.path.join(os.path.dirname(__file__), '..', name))
