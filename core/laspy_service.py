"""
Contains the LaspyService class.
"""
from laspy.file import File
import numpy as np


class LaspyService:
    """
    Contains methods for reading, modifying, and writing LAS files.
    """

    def __init__(self) -> None:
        pass

    def read_file(self, las_path):
        """
        Simply returns a Laspy file given a file path to LAS.
        """
        return File(las_path, mode="r")

    def close_file(self, laspy_file):
        """
        Simple function to remind every processor that we always have to close the laspy file after opening them.
        """
        laspy_file.close()

    def get_header(self, laspy_file):
        return laspy_file.header

    def get_2d_extent(self, laspy_file):
        """
        Returns the minimum and maximum coordinates on the XY-plane in an array.
        Aligns with Arcpy's definition of Extent - minimum bounding rectangle (xmin, ymin, xmax, ymax)
        """
        header = self.get_header(laspy_file)
        header_min = header.min
        header_max = header.max

        min_x = header_min[0]
        min_y = header_min[1]
        max_x = header_max[0]
        max_y = header_max[1]

        return min_x, min_y, max_x, max_y

    def get_3d_extent(self, laspy_file):
        """
        Returns the minimum and maximum coordinates on the XY-plane in an array.
        Aligns with Arcpy's definition of Extent - minimum bounding rectangle (xmin, ymin, zmin, xmax, ymax, zmax)
        """
        min_x, min_y, max_x, max_y = self.get_2d_extent(laspy_file)

        header = self.get_header(laspy_file)
        header_min = header.min
        header_max = header.max
        min_z = header_min[2]
        max_z = header_max[2]

        return min_x, min_y, min_z, max_x, max_y, max_z

    def to_array(self, laspy_file):
        """
        Given a laspy file, returns a numpy array containing all 3d coordinates contained in the file.
        """
        return_stack = np.array((laspy_file.x, laspy_file.y, laspy_file.z))

        return np.vstack(return_stack).transpose()

    def to_extended_array(self, las):
        """
        Returns same result as to_array(), but also with an additional column including intensity.
        """

        laspy_file = self.read_file(las)
        laspy_header = self.get_header(laspy_file)

        if laspy_header.max[2] < 1:
            laspy_file.close()
            return None, None, None, None, None

        return_stack = np.array((laspy_file.x, laspy_file.y, laspy_file.z, laspy_file.intensity))

        return np.vstack(return_stack).transpose(), laspy_header

    def round_xy(self, las_array, precision=4):
        """
        Given the result of to_array (via las_array argument), round XY precision to a certain decimal place.
        By default, precision is 4 (taken from original SSIM code)
        """
        las_array[0] = las_array[0].round(precision)
        las_array[1] = las_array[1].round(precision)

    def sort_by_z(self, las_array):
        """
        Given the result of to_array (via las_array argument), sort by Z.
        """
        return las_array[las_array[:, 2].argsort()]

    def sort_by_intensity(self, las_array):
        """
        Given the result of to_array (via las_array argument), sort by Z.
        """
        return las_array[las_array[:, 3].argsort()]

    def to_las_file(self, las_array, output_path, las_header):
        """
        Formerly known as write_las, this function takes an unmodified/modified las_array from to_array()
        and turns it into a las file.
        """
        x = las_array[:, 0]
        y = las_array[:, 1]
        z = las_array[:, 2]

        # For write-only files, las_header is automatically copied inside laspy.File's open()
        # so don't worry about las_header being modified.
        out_file = File(output_path, mode="w", header=las_header)

        out_file.x = x
        out_file.y = y
        out_file.z = z

        out_file.close()
