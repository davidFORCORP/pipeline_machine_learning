"""
Contains re-usable functions related to tests.
"""
import glob
import re


def get_las_file_paths(directory_path, tile_name):
    return get_file_paths(directory_path, tile_name, "las")


def get_shp_file_paths(directory_path, tile_name):
    return get_file_paths(directory_path, tile_name, "shp")


def get_txt_file_paths(directory_path, tile_name):
    return get_file_paths(directory_path, tile_name, "txt")


def get_file_paths(directory_path, tile_name, file_extension):
    """
    Returns a list of file paths for the given tile.

    :param directory_path: Absolute path to directory containing files
    :param tile_name:      Tile name to look for in file name
    :param file_extension: Filter files by file extension (e.g. las, shp, txt)
    :return:
    """

    all_file_paths = glob.glob(f"{directory_path}/*.{file_extension}", recursive=False)
    file_paths = []

    for file_path in all_file_paths:
        if re.match(f".*[\\\/]{tile_name}.*", file_path) is not None:
            file_paths.append(file_path)

    return file_paths
