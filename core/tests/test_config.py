"""
Contains TestConfig class.
"""
from core.config import Config


class TestConfig(object):
    """
    Replicates methods from Config(), specifically used for the testing environment.

    Adds a set() function to give a value to a corresponding section - option pair.
    """

    def __init__(self, properties=None):
        if properties is None:
            properties = {}
        self.config = properties
        self.production_config = None  # Do not initialize unless necessary

    def copy_from_production_config(self, section, option):
        if self.production_config is None:
            self.production_config = Config()

        self.config[f"{section},{option}"] = self.production_config.get(section, option)

    def set(self, section, option, value):
        self.config[f"{section},{option}"] = value

    def get(self, section, option):
        return self.config[f"{section},{option}"]
