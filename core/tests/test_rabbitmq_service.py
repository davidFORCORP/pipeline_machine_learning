"""
Contains TestRabbitMqService class.
"""


class TestRabbitMqService:
    """
    A class that mocks the RabbitMqService implementation for our test environments.
    """

    def connection(self):
        return None

    def channel(self, *args):
        return None

    def message_properties(self):
        return None

    def ensure_queue_exists(self, *args):
        pass
