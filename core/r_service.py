"""
Contains the RService class.
"""
from core.config import Config
config = Config()

import os
os.environ['R_HOME'] = config.get('r', 'r_home')

from rpy2.robjects.vectors import StrVector
import rpy2.robjects.packages as rpackages
base = rpackages.importr('base')
# R Operators need to be fetched from R's base environment (since they don't exist in python)
# Taken from https://stackoverflow.com/questions/63901279/how-to-change-the-default-value-of-rs4-class-attribute-rpy2
r_not = base.__dict__['!']
get_attribute = base.__dict__['$']
set_attribute = base.__dict__['$<-']


class RService:
    """
    Implements functions that interact with R.
    The current implementation tries to be pythonic as much as possible, but feel free to ditch this method of
    implementation if there's something really difficult to implement via Python.
    """
    def __init__(self) -> None:
        super().__init__()
        self.config = Config()

    def ensure_lidr_installed(self):
        """
        Ensure required libraries are installed for segment_trees().
        :return:
        """
        utils = rpackages.importr('utils')
        package_names = ('lidR', 'rgdal')
        names_to_install = [x for x in package_names if not rpackages.isinstalled(x)]
        if len(names_to_install) > 0:
            utils.install_packages(StrVector(names_to_install), repos='http://cran.us.r-project.org')

    def segment_trees(self, las_path, output_las_path):
        """
        Segment trees based on the algorithm detailed in Li et al. article in 2012. We use R to accomplish this.
        Writes a LAS file where its intensity is its tree ID.
        """
        self.ensure_lidr_installed()
        lidr = rpackages.importr('lidR')

        las_file = lidr.readLAS(las_path)
        las_file = lidr.segment_trees(las_file, lidr.li2012(dt1=1, dt2=2, R=1.5))
        las_file = lidr.filter_poi(las_file, r_not(base.is_na('treeID')))  # Alternative to !is_na
        set_attribute(las_file, 'Intensity', get_attribute(las_file, 'treeID'))
        lidr.writeLAS(las_file, output_las_path)
