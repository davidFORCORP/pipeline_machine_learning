"""
Contains functions for calculating function processing times and storing it in a DB.
"""
import inspect
import platform
from datetime import datetime

from core.db_connection import DbConnection


def timed_method(description):
    """
    This builds a function decorator (i.e. a higher-order function) that
    will time the internals of wrapped function. It persists the results
    into the database.
    """

    def timing_decorator(method):
        """
        This is the actual function decorator that accepts a method as an input
        and returns a wrapped version of that same method.
        """
        def timed(*args, **kwargs):

            db_connection = DbConnection()
            start = datetime.now()

            # Invoke the original method
            result = method(*args, **kwargs)
            end = datetime.now()
            elapsed = (end - start).total_seconds()

            # Misc properties
            class_name = method.__qualname__.split('.')[0]

            # Extract image name from named argument if possible
            tile_name = None
            if 'tile_name' in kwargs:
                tile_name = kwargs.get('tile_name')
            else:
                # Otherwise extract from positional arguments
                argument_names = list(inspect.signature(method).parameters.keys())
                if 'tile_name' in argument_names:
                    argument_index = argument_names.index('tile_name')
                    tile_name = args[argument_index]

            if tile_name is None:
                # Tile name must not have been an argument. As a last resort
                # let's check if "self" was the first argument and that the "self"
                # object happens to have an "tile_name" property. This is helpful
                # for the attribute processor and it's queriers that don't pass around
                # tile_name as an argument.
                if len(args) >= 1 and hasattr(args[0], 'tile_name'):
                    tile_name = args[0].tile_name

            start_timestamp = DbConnection.epoch_to_postgresql_timestamp(start)

            # Update tables
            create_timing_table(db_connection)
            update_timing(db_connection, class_name, tile_name, description, start_timestamp, elapsed)

            db_connection.close()
            return result

        return timed

    return timing_decorator

def create_timing_table(db_connection):
    """
    Creates the timing table if it doesn't yet exist.
    """
    query = """
            CREATE TABLE IF NOT EXISTS ssim_pipeline.metric_timing(
                id serial,
                hostname text,
                class_name text,
                tile_name text,
                action text,
                start_time timestamp without time zone,
                timing_seconds int
            );
        """
    db_connection.execute(query)

def update_timing(db_connection, class_name, tile_name, action, start_time, timing_seconds):
    """
    Inserts timed results of action on specified tile.
    """
    hostname = platform.node()

    query = """
            INSERT INTO metric_timing (hostname, class_name, tile_name, action, start_time, timing_seconds)
            VALUES (%s, %s, %s, %s, %s, %s)
            """
    db_connection.execute(query, (hostname, class_name, tile_name, action, start_time, timing_seconds))
