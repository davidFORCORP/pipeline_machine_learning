'''
This file includes utility functions used to create the extent of all processed tiles.
This extent is used on eLands to verify what spatials are allowed to be intersected with SSIM data.

If you come across permission issues with the LAS, you can run this script in the command line easily by moving this to
the project root directory.
'''
import glob
import os
import subprocess

from core.config import Config
from core.db_connection import DbConnection
from core.laspy_service import LaspyService


def save_to_brl_extent(input_folder_path):
    laspy_service = LaspyService()
    db_connection = DbConnection()

    # Create table if not exists
    db_connection.execute('CREATE TABLE IF NOT EXISTS ssim_pipeline.brl_extent(the_geom geometry(Point, 26911))')

    # Clear table everytime
    db_connection.execute('TRUNCATE TABLE ssim_pipeline.brl_extent')

    for file_path in glob.glob(input_folder_path + '/*.las'):
        print('Looking at: ' + file_path)
        laspy_file = laspy_service.read_file(file_path)
        min_x, min_y, max_x, max_y = laspy_service.get_2d_extent(laspy_file)

        db_connection.execute('INSERT INTO ssim_pipeline.brl_extent (the_geom) VALUES '
                              '(ST_SetSRID(ST_MakePoint(%s, %s), 26911)), '
                              '(ST_SetSRID(ST_MakePoint(%s, %s), 26911)), '
                              '(ST_SetSRID(ST_MakePoint(%s, %s), 26911)), '
                              '(ST_SetSRID(ST_MakePoint(%s, %s), 26911))',
                              variables=(min_x, min_y, min_x, max_y, max_x, min_y, max_x, max_y))

        laspy_service.close_file(laspy_file)

def save_shapefile_to_brl_extent_polygon(shp_path):
    # Setup
    config = Config()

    database = config.get("db", "database")
    host = config.get("db", "host")
    user = config.get("db", "user")
    password = config.get("db", "password")

    # Table creation command
    # shp2pgsql_cmd = f"shp2pgsql -p -I -s {26911} {shp_path} ssim_pipeline.brl_extent_polygon"
    # Populating command
    shp2pgsql_cmd = f"shp2pgsql -a -s 26911 {shp_path} ssim_pipeline.brl_extent_polygon"
    psql_cmd = f"psql -q -d {database} -h {host} -U {user} -w"
    command = f"{shp2pgsql_cmd} | {psql_cmd}"

    # Create a custom environment with the psql password
    custom_environment = os.environ.copy()
    custom_environment["PGPASSWORD"] = password

    # Invoke shp2pgsql
    return_value = subprocess.call(command, shell=True, env=custom_environment)
    if return_value != 0:
        raise Exception(f"shp2pgsql failed to run command: {shp2pgsql_cmd}")

if __name__ == "__main__":
    print('start')
    # save_to_brl_extent('/mnt/autofs/raster_data/FORCORP/SSIM/data_received/Las_v12_ASPRS/Ground_and_Non_Ground')
    save_shapefile_to_brl_extent_polygon('/home/forcorp/henry/Documents/O52/brl_extent.shp')
    print('end')

