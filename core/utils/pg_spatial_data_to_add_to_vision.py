import os


def identified_spatial_data_in_pg():
    avi = ['brl_avi', ['sp1, sp2', 'sp3']]
    net_landbase = ['net_landbase', ['f_del']]
    return [avi, net_landbase]