import os


def retrieve_raster_locations(raster_storage_location):
    full_provided_images_20211024 = os.path.join(raster_storage_location, 'full_provided_images_20211024.tif')
    ndvi_30cm = os.path.join(raster_storage_location, 'ndvi_30cm.tif')
    ndvi_1m = os.path.join(raster_storage_location, 'ndvi_1m.tif')
    chm_5m = os.path.join(raster_storage_location, 'chm_5m.tif')
    chm_18m = os.path.join(raster_storage_location, 'chm_18m.tif')
    chm_std_18m = os.path.join(raster_storage_location, 'chm_std_18m.tif')
    tmi_1m = os.path.join(raster_storage_location, 'tmi_1m.tif')
    tmi_std_18m = os.path.join(raster_storage_location, 'tmi_std_18m.tif')
    tmi_avg_18m = os.path.join(raster_storage_location, 'tmi_avg_18m.tif')
    tmi_log10 = os.path.join(raster_storage_location, 'tmi_log10.tif')
    tpi_small_18m = os.path.join(raster_storage_location, 'tpi_small_18m.tif')
    tpi_medium_100m = os.path.join(raster_storage_location, 'tpi_medium_100m.tif')
    tpi_large_1000m = os.path.join(raster_storage_location, 'tpi_large_1000m.tif')
    tpi_landscape_5000m = os.path.join(raster_storage_location, 'tpi_landscape_5000m.tif')
    ruggedness_18m = os.path.join(raster_storage_location, 'ruggedness_18m.tif')
    flow_dir_1m = os.path.join(raster_storage_location, 'flow_dir_1m.tif')
    flow_acc_avg_18m = os.path.join(raster_storage_location, 'flow_acc_avg_18m.tif')
    flow_acc_1m = os.path.join(raster_storage_location, 'flow_acc_1m.tif')
    distance_to_stream = os.path.join(raster_storage_location, 'distance_to_stream.tif')
    dem_1m = os.path.join(raster_storage_location, 'dem_1m.tif')
    dem_18m = os.path.join(raster_storage_location, 'dem_18m.tif')
    dsm_18m = os.path.join(raster_storage_location, 'dsm_18m.tif')
    dem_curvature_1m = os.path.join(raster_storage_location, 'dem_curvature_1m.tif')
    dem_profile_1m = os.path.join(raster_storage_location, 'dem_profile_1m.tif')
    dem_plan_1m = os.path.join(raster_storage_location, 'dem_plan_1m.tif')
    dem_curvature_18m = os.path.join(raster_storage_location, 'dem_curvature_18m.tif')
    dem_profile_18m = os.path.join(raster_storage_location, 'dem_profile_18m.tif')
    dem_plan_18m = os.path.join(raster_storage_location, 'dem_plan_18m.tif')
    dem_aspect_1m = os.path.join(raster_storage_location, 'dem_aspect_1m.tif')
    dem_aspect_18m = os.path.join(raster_storage_location, 'dem_aspect_18m.tif')
    dsm_aspect_1m = os.path.join(raster_storage_location, 'dsm_aspect_1m.tif')
    dem_solar_radiation_1m = os.path.join(raster_storage_location, 'dem_solar_radiation_1m.tif')
    dsm_solar_radiation_1m = os.path.join(raster_storage_location, 'dsm_solar_radiation_1m.tif')
    iso_classes_30cm = os.path.join(raster_storage_location, 'iso_classes_30cm.tif')
    iso_classes_1m = os.path.join(raster_storage_location, 'iso_classes_1m.tif')
    iso_classes_1m_mean = os.path.join(raster_storage_location, 'iso_classes_1m_mean.tif')
    iso_classes_1m_majority = os.path.join(raster_storage_location, 'iso_classes_1m_majority.tif')
    full_provided_images_20211024_log = os.path.join(raster_storage_location, 'full_provided_images_20211024_log.tif')
    ndvi_30cm_log = os.path.join(raster_storage_location, 'ndvi_30cm_log.tif')
    ndvi_1m_log = os.path.join(raster_storage_location, 'ndvi_1m_log.tif')
    chm_5m_log = os.path.join(raster_storage_location, 'chm_5m_log.tif')
    chm_18m_log = os.path.join(raster_storage_location, 'chm_18m_log.tif')
    chm_std_18m_log = os.path.join(raster_storage_location, 'chm_std_18m_log.tif')
    tmi_1m_log = os.path.join(raster_storage_location, 'tmi_1m_log.tif')
    tmi_std_18m_log = os.path.join(raster_storage_location, 'tmi_std_18m_log.tif')
    tmi_avg_18m_log = os.path.join(raster_storage_location, 'tmi_avg_18m_log.tif')
    tmi_log10_log = os.path.join(raster_storage_location, 'tmi_log10_log.tif')
    tpi_small_18m_log = os.path.join(raster_storage_location, 'tpi_small_18m_log.tif')
    tpi_medium_100m_log = os.path.join(raster_storage_location, 'tpi_medium_100m_log.tif')
    tpi_large_1000m_log = os.path.join(raster_storage_location, 'tpi_large_1000m_log.tif')
    tpi_landscape_5000m_log = os.path.join(raster_storage_location, 'tpi_landscape_5000m_log.tif')
    ruggedness_18m_log = os.path.join(raster_storage_location, 'ruggedness_18m_log.tif')
    flow_dir_1m_log = os.path.join(raster_storage_location, 'flow_dir_1m_log.tif')
    flow_acc_avg_18m_log = os.path.join(raster_storage_location, 'flow_acc_avg_18m_log.tif')
    flow_acc_1m_log = os.path.join(raster_storage_location, 'flow_acc_1m_log.tif')
    distance_to_stream_log = os.path.join(raster_storage_location, 'distance_to_stream_log.tif')
    dem_1m_log = os.path.join(raster_storage_location, 'dem_1m_log.tif')
    dem_18m_log = os.path.join(raster_storage_location, 'dem_18m_log.tif')
    dsm_18m_log = os.path.join(raster_storage_location, 'dsm_18m_log.tif')
    dem_curvature_1m_log = os.path.join(raster_storage_location, 'dem_curvature_1m_log.tif')
    dem_profile_1m_log = os.path.join(raster_storage_location, 'dem_profile_1m_log.tif')
    dem_plan_1m_log = os.path.join(raster_storage_location, 'dem_plan_1m_log.tif')
    dem_curvature_18m_log = os.path.join(raster_storage_location, 'dem_curvature_18m_log.tif')
    dem_profile_18m_log = os.path.join(raster_storage_location, 'dem_profile_18m_log.tif')
    dem_plan_18m_log = os.path.join(raster_storage_location, 'dem_plan_18m_log.tif')
    dem_aspect_1m_log = os.path.join(raster_storage_location, 'dem_aspect_1m_log.tif')
    dem_aspect_18m_log = os.path.join(raster_storage_location, 'dem_aspect_18m_log.tif')
    dsm_aspect_1m_log = os.path.join(raster_storage_location, 'dsm_aspect_1m_log.tif')
    dem_solar_radiation_1m_log = os.path.join(raster_storage_location, 'dem_solar_radiation_1m_log.tif')
    dsm_solar_radiation_1m_log = os.path.join(raster_storage_location, 'dsm_solar_radiation_1m_log.tif')
    iso_classes_30cm_log = os.path.join(raster_storage_location, 'iso_classes_30cm_log.tif')
    iso_classes_1m_log = os.path.join(raster_storage_location, 'iso_classes_1m_log.tif')
    iso_classes_1m_mean_log = os.path.join(raster_storage_location, 'iso_classes_1m_mean_log.tif')
    iso_classes_1m_majority_log = os.path.join(raster_storage_location, 'iso_classes_1m_majority_log.tif')
    full_provided_images_20211024_norm = os.path.join(raster_storage_location, 'full_provided_images_20211024_norm.tif')
    ndvi_30cm_norm = os.path.join(raster_storage_location, 'ndvi_30cm_norm.tif')
    ndvi_1m_norm = os.path.join(raster_storage_location, 'ndvi_1m_norm.tif')
    chm_5m_norm = os.path.join(raster_storage_location, 'chm_5m_norm.tif')
    chm_18m_norm = os.path.join(raster_storage_location, 'chm_18m_norm.tif')
    chm_std_18m_norm = os.path.join(raster_storage_location, 'chm_std_18m_norm.tif')
    tmi_1m_norm = os.path.join(raster_storage_location, 'tmi_1m_norm.tif')
    tmi_std_18m_norm = os.path.join(raster_storage_location, 'tmi_std_18m_norm.tif')
    tmi_avg_18m_norm = os.path.join(raster_storage_location, 'tmi_avg_18m_norm.tif')
    tmi_log10_norm = os.path.join(raster_storage_location, 'tmi_log10_norm.tif')
    tpi_small_18m_norm = os.path.join(raster_storage_location, 'tpi_small_18m_norm.tif')
    tpi_medium_100m_norm = os.path.join(raster_storage_location, 'tpi_medium_100m_norm.tif')
    tpi_large_1000m_norm = os.path.join(raster_storage_location, 'tpi_large_1000m_norm.tif')
    tpi_landscape_5000m_norm = os.path.join(raster_storage_location, 'tpi_landscape_5000m_norm.tif')
    ruggedness_18m_norm = os.path.join(raster_storage_location, 'ruggedness_18m_norm.tif')
    flow_dir_1m_norm = os.path.join(raster_storage_location, 'flow_dir_1m_norm.tif')
    flow_acc_avg_18m_norm = os.path.join(raster_storage_location, 'flow_acc_avg_18m_norm.tif')
    flow_acc_1m_norm = os.path.join(raster_storage_location, 'flow_acc_1m_norm.tif')
    distance_to_stream_norm = os.path.join(raster_storage_location, 'distance_to_stream_norm.tif')
    dem_1m_norm = os.path.join(raster_storage_location, 'dem_1m_norm.tif')
    dem_18m_norm = os.path.join(raster_storage_location, 'dem_18m_norm.tif')
    dsm_18m_norm = os.path.join(raster_storage_location, 'dsm_18m_norm.tif')
    dem_curvature_1m_norm = os.path.join(raster_storage_location, 'dem_curvature_1m_norm.tif')
    dem_profile_1m_norm = os.path.join(raster_storage_location, 'dem_profile_1m_norm.tif')
    dem_plan_1m_norm = os.path.join(raster_storage_location, 'dem_plan_1m_norm.tif')
    dem_curvature_18m_norm = os.path.join(raster_storage_location, 'dem_curvature_18m_norm.tif')
    dem_profile_18m_norm = os.path.join(raster_storage_location, 'dem_profile_18m_norm.tif')
    dem_plan_18m_norm = os.path.join(raster_storage_location, 'dem_plan_18m_norm.tif')
    dem_aspect_1m_norm = os.path.join(raster_storage_location, 'dem_aspect_1m_norm.tif')
    dem_aspect_18m_norm = os.path.join(raster_storage_location, 'dem_aspect_18m_norm.tif')
    dsm_aspect_1m_norm = os.path.join(raster_storage_location, 'dsm_aspect_1m_norm.tif')
    dem_solar_radiation_1m_norm = os.path.join(raster_storage_location, 'dem_solar_radiation_1m_norm.tif')
    dsm_solar_radiation_1m_norm = os.path.join(raster_storage_location, 'dsm_solar_radiation_1m_norm.tif')
    iso_classes_30cm_norm = os.path.join(raster_storage_location, 'iso_classes_30cm_norm.tif')
    iso_classes_1m_norm = os.path.join(raster_storage_location, 'iso_classes_1m_norm.tif')
    iso_classes_1m_mean_norm = os.path.join(raster_storage_location, 'iso_classes_1m_mean_norm.tif')
    iso_classes_1m_majority_norm = os.path.join(raster_storage_location, 'iso_classes_1m_majority_norm.tif')

    return [full_provided_images_20211024, ndvi_30cm, ndvi_1m, chm_5m, chm_18m, chm_std_18m, tmi_1m, tmi_std_18m,
            tmi_avg_18m, tmi_log10, tpi_small_18m, tpi_medium_100m, tpi_large_1000m, tpi_landscape_5000m,
            ruggedness_18m, flow_dir_1m, flow_acc_avg_18m, flow_acc_1m, distance_to_stream, dem_1m, dem_18m,
            dsm_18m, dem_curvature_1m, dem_profile_1m, dem_plan_1m, dem_curvature_18m, dem_profile_18m,
            dem_plan_18m, dem_aspect_1m, dem_aspect_18m, dsm_aspect_1m, dem_solar_radiation_1m,
            dsm_solar_radiation_1m, iso_classes_30cm, iso_classes_1m, iso_classes_1m_mean, iso_classes_1m_majority,
            full_provided_images_20211024_log, ndvi_30cm_log, ndvi_1m_log, chm_5m_log, chm_18m_log, chm_std_18m_log,
            tmi_1m_log, tmi_std_18m_log, tmi_avg_18m_log, tmi_log10_log, tpi_small_18m_log, tpi_medium_100m_log,
            tpi_large_1000m_log, tpi_landscape_5000m_log, ruggedness_18m_log, flow_dir_1m_log, flow_acc_avg_18m_log,
            flow_acc_1m_log, distance_to_stream_log, dem_1m_log, dem_18m_log, dsm_18m_log, dem_curvature_1m_log,
            dem_profile_1m_log, dem_plan_1m_log, dem_curvature_18m_log, dem_profile_18m_log, dem_plan_18m_log,
            dem_aspect_1m_log, dem_aspect_18m_log, dsm_aspect_1m_log, dem_solar_radiation_1m_log,
            dsm_solar_radiation_1m_log, iso_classes_30cm_log, iso_classes_1m_log, iso_classes_1m_mean_log,
            iso_classes_1m_majority_log, full_provided_images_20211024_norm, ndvi_30cm_norm, ndvi_1m_norm,
            chm_5m_norm, chm_18m_norm, chm_std_18m_norm, tmi_1m_norm, tmi_std_18m_norm, tmi_avg_18m_norm,
            tmi_log10_norm, tpi_small_18m_norm, tpi_medium_100m_norm, tpi_large_1000m_norm,
            tpi_landscape_5000m_norm, ruggedness_18m_norm, flow_dir_1m_norm, flow_acc_avg_18m_norm,
            flow_acc_1m_norm, distance_to_stream_norm, dem_1m_norm, dem_18m_norm, dsm_18m_norm,
            dem_curvature_1m_norm, dem_profile_1m_norm, dem_plan_1m_norm, dem_curvature_18m_norm,
            dem_profile_18m_norm, dem_plan_18m_norm, dem_aspect_1m_norm, dem_aspect_18m_norm, dsm_aspect_1m_norm,
            dem_solar_radiation_1m_norm, dsm_solar_radiation_1m_norm, iso_classes_30cm_norm, iso_classes_1m_norm,
            iso_classes_1m_mean_norm, iso_classes_1m_majority_norm]
