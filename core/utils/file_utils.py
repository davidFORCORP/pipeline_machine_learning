"""
Contains functions that create, modify, and delete files.
"""
import os
import shutil

import core.gdal_service


def delete_all_files_from_directory(directory_path):
    """
    Deletes all files from a specified directory.
    """
    for filename in os.listdir(directory_path):
        file_path = os.path.join(directory_path, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print("Failed to delete {}. Reason: {}".format(file_path, e))


def ensure_file_exists_and_can_be_opened(file_path):
    """
    Checks that a file not only exists, but that we are able to open it too.
    This will catch issues where the file exists but the permissions might be
    wrong.
    """
    if not os.path.isfile(file_path):
        raise Exception("Required file doesn't exist [{}]".format(file_path))
    try:
        file = open(file_path)
        file.close()
    except IOError:
        raise Exception("File exists but can not be opened; check permissions [{}]".format(file_path))


def ensure_directory_exists(directory_path):
    """
    Checks that a directory exists.
    """
    if not os.path.isdir(directory_path):
        raise Exception("Required directory doesn't exist [{}]".format(directory_path))


def delete_file_if_exists(file_path):
    if os.path.isfile(file_path):
        os.unlink(file_path)


def get_filename_without_extension(filename):
    return os.path.splitext(filename)[0]


def copy_file(file_path, output_directory_path):
    """
    Copies a file, specifically checking for TIFs/shapefiles to handle them appropriately.
    """
    filename = os.path.basename(file_path)
    target_path = os.path.join(output_directory_path, filename)
    if file_path.endswith('.shp'):
        # Shapefiles consist of multiple files that all need to be copied.
        gdal_service = core.gdal_service.GdalService()
        gdal_service.copy_shapefile(file_path, target_path)
    elif file_path.endswith('.tif'):
        # TIFs may also have a few extraneous metadata files with them and we want to copy them all
        filename_without_extension = get_filename_without_extension(filename)
        source_directory_path = os.path.dirname(file_path)
        copy_all_files_with_prefix(source_directory_path, filename_without_extension, output_directory_path)
    else:
        shutil.copyfile(file_path, target_path)


def copy_all_files_with_prefix(source_directory_path, prefix, output_directory_path):
    for filename in os.listdir(source_directory_path):
        if filename.startswith(prefix):
            source_file_path = os.path.join(source_directory_path, filename)
            destination_file_path = os.path.join(output_directory_path, filename)
            shutil.copyfile(source_file_path, destination_file_path)
