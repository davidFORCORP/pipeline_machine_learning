"""Contains helper functions used inside Voxel related classes."""


def calculate_voxel_mathematical_index(x, y, z, x_max, y_max):
    """
    Calculates a mathematical index of a voxel in the current voxel grid.

    For each voxel that we go up on Z, we add x_max * y_max
    For each voxel that we move up on Y, we add x_max
    For each voxel that we move up on X, we just add 1 voxel
    With these sets of rules, only values with the same x, y, and z will collide.
    """
    indexed_z = z * ((x_max + 1) * (y_max + 1))
    indexed_y = y * (x_max + 1)
    return indexed_z + indexed_y + x

def get_xyz_from_mathematical_index(index, x_max, y_max):
    """
    Reverses calculate_voxel_mathematical_index(). Returns X, Y, and Z (in voxel) of a mathematical index.
    """

    indexed_x_max = x_max + 1
    indexed_y_max = y_max + 1

    multiple_of_z = indexed_x_max * indexed_y_max
    z = index // multiple_of_z
    z_remainder = index % multiple_of_z
    y = z_remainder // indexed_x_max
    x = z_remainder % indexed_x_max
    return x, y, z
