from num2words import num2words

def tree_locations_table_name(shortened_image_name):
    return shortened_image_name + "_tree_locations"

def translate_numbers_to_text(number):
    return num2words(number)
