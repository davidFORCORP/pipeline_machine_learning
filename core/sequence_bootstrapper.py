"""
Contains the Bootstrapper class.
"""
import signal
from itertools import cycle
import multiprocessing
import setproctitle
import time
from typing import Final

from core.abstract_bootstrapper import AbstractBootstrapper
from core.config import Config
from core.rabbitmq_service import RabbitMqService
from processor.attribute.attribute_processor import AttributeProcessor
from processor.bare_earth.bare_earth_processor import BareEarthProcessor
from processor.clip.clip_processor import ClipProcessor
from processor.elands_import.elands_export_processor import ElandsExportProcessor
from processor.finalize_clip.finalize_clip_processor import FinalizeClipProcessor
from processor.height_above_ground.height_above_ground_processor import HeightAboveGroundProcessor
from processor.height_filter.height_filter_processor import HeightFilterProcessor
from processor.pgloader.pg_loader_processor import PgLoaderProcessor
from processor.projection.projection_processor import ProjectionProcessor
from processor.stem_finder.stem_finder_processor import StemFinderProcessor
from processor.vision.vision_processor import VisionProcessor

# Static variables for processor types
projection_type: Final = 'projection'
clip_type: Final = 'clip'
bare_earth_type: Final = 'bare_earth'
height_above_ground_type: Final = 'height_above_ground'
height_above_ground_tls_type: Final = 'height_above_ground_tls'
height_filter_type: Final = 'height_filter'
stem_finder_type: Final = 'stem_finder'
finalize_clip_type: Final = 'finalize_clip'
pgloader_type: Final = 'pgloader'
attribute_type: Final = 'attribute'
vision_type: Final = 'vision'
elands_export_type: Final = 'elands_export'


class SequenceBootstrapper(AbstractBootstrapper):
    """
    A variant of the Bootstrapper class inspired by ContainerGroupSequencer from Spring's Kafka module.

    This bootstrapper only cycles between a group of processors, running only one group at a time. This was created to
    solve two issues:
    1) Our dev machines and VMs have limited memory and CPU power, so they cannot run all of our processors at once.
       We needed a way to limit a PC's workload and prevent it from crashing at all times.
    2) Given the issue above, a developer always needed to monitor the progress of images and manually change the
       processor configuration when messages moved from queue to queue. This makes sure all of our messages move from
       queue to queue without any developer intervention.
    """

    def __init__(self,
                 number_of_projection,
                 number_of_clip,
                 number_of_bare_earth,
                 number_of_height_above_ground,
                 number_of_height_filter,
                 number_of_stem_finder,
                 number_of_finalize_clip,
                 number_of_pg_loader,
                 number_of_attribute,
                 number_of_vision,
                 number_of_elands_export,):
        self.number_of_projection = number_of_projection
        self.number_of_clip = number_of_clip
        self.number_of_bare_earth = number_of_bare_earth
        self.number_of_height_above_ground = number_of_height_above_ground
        self.number_of_height_filter = number_of_height_filter
        self.number_of_stem_finder = number_of_stem_finder
        self.number_of_finalize_clip = number_of_finalize_clip
        self.number_of_pg_loader = number_of_pg_loader
        self.number_of_attribute = number_of_attribute
        self.number_of_vision = number_of_vision
        self.number_of_elands_export = number_of_elands_export

        self.config = Config()
        self.heartbeat_interval = int(self.config.get("bootstrapper", "heartbeat_interval"))

    def run(self):
        # ALS Pipeline sequence
        process_types = cycle([projection_type, clip_type, bare_earth_type, height_above_ground_type,
                               height_filter_type, stem_finder_type, finalize_clip_type, pgloader_type, attribute_type,
                               vision_type, elands_export_type])
        # TLS Pipeline sequence
        # process_types = cycle([clip_type, height_above_ground_tls_type, stem_finder_type, finalize_clip_type, pgloader_type, attribute_type])

        # Use a flag and an associated signal handler to try and catch when the user wants to stop the
        # infinite consumption loop. This allows us to do some cleanup during graceful exits.
        stop = False

        def handle_signal(signal, frame):
            print(None, "Signal has been caught and infinite consumption loop will soon end")
            nonlocal stop
            stop = True
        signal.signal(signal.SIGINT, handle_signal)

        # Cycle through each processor group
        # Keep running the processor group until all processors die. Processors will die if they are idle for too long.
        # Move onto the next processor group and keep cycling to make sure all messages will eventually be processed
        # (unless new messages keep coming in).
        while not stop:
            current_type = next(process_types)
            processes = self.assemble_processes(current_type)

            if len(processes) > 0:
                print(f"Bootstrapper: Running processor type [{current_type}]")
                for process in processes:
                    process.start()
                    time.sleep(1)

                while self.check_processes_still_alive(processes):
                    time.sleep(self.heartbeat_interval)

                for process in processes:
                    process.close()

    def assemble_processes(self, process_type):
        processes = []

        if self.number_of_projection and process_type == projection_type:
            for i in range(self.number_of_projection):
                name = 'projection-processor-{}'.format(i)
                processes.append(self.build_process(name, process_type))

        if self.number_of_clip and process_type == clip_type:
            for i in range(self.number_of_clip):
                name = 'clip-processor-{}'.format(i)
                processes.append(self.build_process(name, process_type))

        if self.number_of_bare_earth and process_type == bare_earth_type:
            for i in range(self.number_of_bare_earth):
                name = 'bare-earth-processor-{}'.format(i)
                processes.append(self.build_process(name, process_type))

        if self.number_of_height_above_ground and process_type == height_above_ground_type:
            for i in range(self.number_of_height_above_ground):
                name = 'height-above-ground-processor-{}'.format(i)
                processes.append(self.build_process(name, process_type))

        # if self.number_of_height_above_ground and process_type == height_above_ground_tls_type:
        #     for i in range(self.number_of_height_above_ground):
        #         name = 'height-above-ground-tls-processor-{}'.format(i)
        #         processes.append(self.build_process(name, process_type))

        if self.number_of_height_filter and process_type == height_filter_type:
            for i in range(self.number_of_height_filter):
                name = 'height-filter-processor-{}'.format(i)
                processes.append(self.build_process(name, process_type))

        if self.number_of_stem_finder and process_type == stem_finder_type:
            for i in range(self.number_of_stem_finder):
                name = 'stem-finder-processor-{}'.format(i)
                processes.append(self.build_process(name, process_type))

        if self.number_of_finalize_clip and process_type == finalize_clip_type:
            for i in range(self.number_of_finalize_clip):
                name = 'finalize-clip-processor-{}'.format(i)
                processes.append(self.build_process(name, process_type))

        if self.number_of_pg_loader and process_type == pgloader_type:
            for i in range(self.number_of_pg_loader):
                name = 'pgloader-processor-{}'.format(i)
                processes.append(self.build_process(name, process_type))

        if self.number_of_attribute and process_type == attribute_type:
            for i in range(self.number_of_attribute):
                name = 'attribute-processor-{}'.format(i)
                processes.append(self.build_process(name, process_type))

        if self.number_of_vision and process_type == vision_type:
            for i in range(self.number_of_attribute):
                name = 'vision-processor-{}'.format(i)
                processes.append(self.build_process(name, process_type))

        if self.number_of_elands_export and process_type == elands_export_type:
            for i in range(self.number_of_elands_export):
                name = 'elands-export-processor-{}'.format(i)
                processes.append(self.build_process(name, process_type))

        return processes

    def build_process(self, name, processor_type):
        return multiprocessing.Process(name=name, target=run_processor, args=(processor_type,))

    def check_processes_still_alive(self, processes):
        """
        Check if all processes are still alive. Returns true if at least one process is still alive.
        """
        alive = False

        for process in processes:
            if process.is_alive():
                alive = True
                break

        return alive

def run_processor(processor_type):
    """
    This is the actual function that will run in each individual process.
    """

    # Sets the OS name for this process so it's visible in "htop" as "ps"
    setproctitle.setproctitle(multiprocessing.current_process().name)

    # Build and run the actual processor
    processor = None
    rabbitmq_service = RabbitMqService()

    if processor_type == projection_type:
        processor = ProjectionProcessor(rabbitmq_service, terminate_on_idle=True)
    elif processor_type == clip_type:
        processor = ClipProcessor(rabbitmq_service, terminate_on_idle=True)
    elif processor_type == bare_earth_type:
        processor = BareEarthProcessor(rabbitmq_service, terminate_on_idle=True)
    elif processor_type == height_above_ground_type:
        processor = HeightAboveGroundProcessor(rabbitmq_service, terminate_on_idle=True)
    elif processor_type == height_above_ground_tls_type:
        processor = HeightAboveGroundProcessor(rabbitmq_service, terminate_on_idle=True)
    elif processor_type == height_filter_type:
        processor = HeightFilterProcessor(rabbitmq_service, terminate_on_idle=True)
    elif processor_type == stem_finder_type:
        processor = StemFinderProcessor(rabbitmq_service, terminate_on_idle=True)
    elif processor_type == finalize_clip_type:
        processor = FinalizeClipProcessor(rabbitmq_service, terminate_on_idle=True)
    elif processor_type == pgloader_type:
        processor = PgLoaderProcessor(rabbitmq_service, terminate_on_idle=True)
    elif processor_type == attribute_type:
        processor = AttributeProcessor(rabbitmq_service, terminate_on_idle=True)
    elif processor_type == vision_type:
        processor = VisionProcessor(rabbitmq_service, terminate_on_idle=True)
    elif processor_type == elands_export_type:
        processor = ElandsExportProcessor(rabbitmq_service, terminate_on_idle=True)

    # Begin!
    processor.begin()
