import numpy as np
from scipy import interpolate, stats


class ScipyService:
    def __init__(self) -> None:
        super().__init__()

    def interpolate_2d_in_voxel_space(self, x, y, z, xyz_max):
        """
        uses scipy to resample the raster to our desired precision
        assumes coordinates already turned to voxels
        :param x:
        :param y:
        :param z:
        :param xyz_max:
        :return:
        """
        x_max = xyz_max[0] + 1
        y_max = xyz_max[1] + 1

        xnew_edges, ynew_edges = np.mgrid[0:x_max:x_max, 0:y_max:y_max]
        xnew = xnew_edges[:-1, :-1] + np.diff(xnew_edges[:2, 0])[0] / 2.
        ynew = ynew_edges[:-1, :-1] + np.diff(ynew_edges[0, :2])[0] / 2.
        interp = interpolate.bisplrep(x, y, z, s=0)
        znew = interpolate.bisplev(xnew[:, 0], ynew[0, :], interp)

        return np.array((xnew_edges, ynew_edges, znew))

