from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import numpy as np
import pandas as pd
####https://wizardforcel.gitbooks.io/deep-learning-keras-tensorflow/content/Requirements.html
import sys

class MLmodel():
    def __init__(self):
        pass

    def perform(self, in_train_data, in_predict_data):
        dataset = pd.read_csv(in_train_data)
        max_col_n = len(dataset.columns)

        sys.exit()
        y = dataset.iloc[:, :1].values
        x = dataset.iloc[:, 1:max_col_n].values

        sc = StandardScaler()
        x = sc.fit_transform(x)

        ohe = OneHotEncoder()
        y = ohe.fit_transform(y).toarray()

        X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2)

        model = Sequential()
        model.add(Dense(16, input_dim=max_col_n-1, activation='relu'))
        model.add(Dense(12, activation='relu'))
        model.add(Dense(2, activation='softmax'))

        model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

        history = model.fit(X_train, y_train, epochs=100, batch_size=64)

        y_pred = model.predict(X_test)
        # Converting predictions to label
        pred = list()
        for i in range(len(y_pred)):
            pred.append(np.argmax(y_pred[i]))
        # Converting one hot encoded test label to label
        test = list()
        for i in range(len(y_test)):
            test.append(np.argmax(y_test[i]))

        a = accuracy_score(pred, test)
        print('Accuracy is:', a * 100)
