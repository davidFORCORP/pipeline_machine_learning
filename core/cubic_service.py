from core.db_connection import DbConnection
import numpy as np


class CubicService:
    def __init__(self):
        self.db_connection = DbConnection()

    def create_or_gather_cubic_space(self, tile_name, xyz_max, extra_fields='', return_the_dict=True, data_limits='', reset_table=False):
        """this will either create the cube and return the voxel index list or it will simply pull the voxel index list
        for the current assessments"""
        # TODO there must be a way to create the cube table data once in postgres and then just copy it into each
        #  schema it is needed instead of recreating the cube for every schema. The only problem I see is that the cube
        #  shape will be different for many tiles (i.e., long and skinny versus short and wide).
        voxel_space_name = f"{tile_name}_vision"
        table_exists = self.db_connection.test_table_exists(voxel_space_name)

        if table_exists and not reset_table:
            if return_the_dict:
                return self.db_connection.create_voxel_index_dictionary_object(voxel_space_name, extra_fields=extra_fields, data_limits=data_limits)
            return

        self.create_cube(tile_name, xyz_max)
        if not return_the_dict:
            return

        # will only return a dictionary with keys, no values
        return self.db_connection.create_voxel_index_dictionary_object(voxel_space_name, extra_fields=extra_fields, data_limits=data_limits)

    def strip_index_positions_from_items_value_array_in_dictionary(self, dictionary, indexes_to_strip):

        for item in dictionary.items():
            dictionary[item[0]] = [x for i, x in enumerate(item[1]) if i not in indexes_to_strip]

        return dictionary

    def create_cube(self, tile_name, xyz_max):

        voxel_space_z_cross_section_max = (xyz_max[0] + 1) * (xyz_max[1] + 1)
        command = f"CREATE TABLE ssim_pipeline_{tile_name}.{tile_name}_vision (" \
                  f"voxel_index BIGINT NOT NULL PRIMARY KEY, " \
                  f"x INTEGER NOT NULL, " \
                  f"y INTEGER NOT NULL, " \
                  f"z INTEGER NOT NULL); " \
                  f"WITH tab AS " \
                  f"(" \
                  f"    SELECT x, y, z " \
                  f"    FROM generate_series(0, {xyz_max[0]}) x " \
                  f"    CROSS JOIN generate_series(0, {xyz_max[1]}) y " \
                  f"    CROSS JOIN generate_series(0, 350) z" \
                  f")" \
                  f"INSERT INTO ssim_pipeline_{tile_name}.{tile_name}_vision (voxel_index, x, y, z) " \
                  f"SELECT (z * {voxel_space_z_cross_section_max}) + (y * ({xyz_max[0]} + 1)) + x, x, y, z " \
                  f"FROM tab; " \
                  f"CREATE INDEX ON ssim_pipeline_{tile_name}.{tile_name}_vision USING gist(geom);"

        self.db_connection.execute(command)
