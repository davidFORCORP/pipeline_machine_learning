import os
import shutil

from osgeo import ogr, gdal, osr

from core.command_runner import CommandRunner
from core.config import Config
from core.utils import file_utils


class GdalService:

    def __init__(self) -> None:
        super().__init__()
        config = Config()
        self.gdal_home = config.get('gdal', 'gdal_home')
        self.gdal_calc_path = config.get('gdal', 'gdal_calc_path')
        self.srid = int(config.get('db', 'srid'))

    @staticmethod
    def read_shapefile(shapefile_path):
        """
        Simply reads the shapefile and returns it.
        """
        ogr.UseExceptions()
        gdal.UseExceptions()
        file_utils.ensure_file_exists_and_can_be_opened(shapefile_path)

        return ogr.Open(shapefile_path, 1)

    @staticmethod
    def get_srs_of_tif(source_path):
        """
        Gets the SRS of a specified TIF
        """
        ogr.UseExceptions()
        gdal.UseExceptions()
        file_utils.ensure_file_exists_and_can_be_opened(source_path)

        source = gdal.Open(source_path, gdal.GA_ReadOnly)
        srs = osr.SpatialReference()
        srs.ImportFromWkt(source.GetProjectionRef())
        return srs

    @staticmethod
    def delete_features_using_filter(shapefile_path, delete_filter):
        """
        Deletes all features from a shapefile that match the given filter
        """
        ogr.UseExceptions()
        gdal.UseExceptions()
        file_utils.ensure_file_exists_and_can_be_opened(shapefile_path)

        shapefile = ogr.Open(shapefile_path, 1)
        layer = shapefile.GetLayer()
        layer.SetAttributeFilter(delete_filter)
        for feat in layer:
            layer.DeleteFeature(feat.GetFID())
        shapefile.ExecuteSQL('REPACK ' + layer.GetName())

        # GDAL requires us to delete references for it to flush to disk
        shapefile = layer = None

    @staticmethod
    def clip_shapefile_using_another(shapefile_to_clip_path, shapefile_as_clipper_path,
                                     output_path):
        ogr.UseExceptions()
        gdal.UseExceptions()
        file_utils.ensure_file_exists_and_can_be_opened(shapefile_to_clip_path)
        file_utils.ensure_file_exists_and_can_be_opened(shapefile_as_clipper_path)

        driver_name = "ESRI Shapefile"
        driver = ogr.GetDriverByName(driver_name)
        shapefile_to_clip = driver.Open(shapefile_to_clip_path, 1)
        shapefile_to_clip_layer = shapefile_to_clip.GetLayer()

        shapefile_as_clipper = driver.Open(shapefile_as_clipper_path, 0)
        shapefile_as_clipper_layer = shapefile_as_clipper.GetLayer()
        srs = shapefile_as_clipper_layer.GetSpatialRef()

        output_shapefile = driver.CreateDataSource(output_path)
        output_layer = output_shapefile.CreateLayer(output_path, srs, geom_type=ogr.wkbPolygon)
        ogr.Layer.Clip(shapefile_to_clip_layer, shapefile_as_clipper_layer, output_layer)

        # GDAL requires us to delete references for it to flush to disk
        shapefile_to_clip = shapefile_to_clip_layer = shapefile_as_clipper = output_shapefile = output_layer = None

    @staticmethod
    def erase_shapes(shapefile_to_erase_path, shapefile_eraser_path, output_path):
        """
        Erases shapes from the shapefile_to_erase_path that exist in the shapefile_eraser_path.
        """

        ogr.UseExceptions()
        gdal.UseExceptions()
        file_utils.ensure_file_exists_and_can_be_opened(shapefile_to_erase_path)
        file_utils.ensure_file_exists_and_can_be_opened(shapefile_eraser_path)

        driver = ogr.GetDriverByName('ESRI Shapefile')
        shapefile_to_erase = driver.Open(shapefile_to_erase_path, 0)
        shapefile_to_erase_layer = shapefile_to_erase.GetLayer()

        shapefile_eraser = driver.Open(shapefile_eraser_path, 1)
        shapefile_eraser_layer = shapefile_eraser.GetLayer()

        srs = shapefile_to_erase_layer.GetSpatialRef()

        output_shapefile = driver.CreateDataSource(output_path)
        output_layer = output_shapefile.CreateLayer(os.path.splitext(os.path.basename(output_path))[0], srs,
                                                    ogr.wkbPolygon)

        output_shapefile = shapefile_to_erase_layer.Erase(shapefile_eraser_layer, output_layer)

        # GDAL requires us to delete references for it to flush to disk
        output_shapefile = output_layer = None

    @staticmethod
    def copy_shapefile(input_path, output_path, feature_type=None):
        """
        Copies a shapefile to another location.
        """

        ogr.UseExceptions()
        gdal.UseExceptions()
        file_utils.ensure_file_exists_and_can_be_opened(input_path)

        input_datasource = ogr.Open(input_path)
        input_layer = input_datasource.GetLayerByIndex(0)

        if feature_type is None:
            # Try to dynamically figure it out
            layer_definition = input_layer.GetLayerDefn()
            data_type = layer_definition.GetGeomType()
        elif feature_type.upper() == 'POINT':
            data_type = ogr.wkbPoint
        elif feature_type.upper() == 'LINE':
            data_type = ogr.wkbLineString
        elif feature_type.upper() == 'POLYGON':
            data_type = ogr.wkbPolygon
        else:
            raise Exception("ERROR with feature type")


        driver_name = "ESRI Shapefile"
        driver = ogr.GetDriverByName(driver_name)
        output_datasource = driver.CreateDataSource(output_path)
        srs = input_layer.GetSpatialRef()
        output_layer = output_datasource.CreateLayer(output_path.split(".")[0], srs, data_type)
        layer_definition = input_layer.GetLayerDefn()

        # Copy all the fields
        for i in range(layer_definition.GetFieldCount()):
            output_layer.CreateField(layer_definition.GetFieldDefn(i))

        # Copy all the features
        for feat in input_layer:
            output_layer.CreateFeature(feat)

        # GDAL requires us to delete references for it to flush to disk
        output_datasource = output_layer = None

    @staticmethod
    def add_field_to_shapefile(shapefile_path, field_name, field_type):
        """
        Adds a new field to a shapefile.
        """

        ogr.UseExceptions()
        gdal.UseExceptions()
        file_utils.ensure_file_exists_and_can_be_opened(shapefile_path)

        if field_type.upper() == 'INTEGER':
            data_type = ogr.OFTInteger
        elif field_type.upper() == 'FLOAT':
            data_type = ogr.OFTReal
        elif field_type.upper() == 'STRING':
            data_type = ogr.OFTString
        else:
            raise Exception('ERROR with feature type')

        driver = ogr.GetDriverByName('ESRI Shapefile')
        data_source = driver.Open(shapefile_path, 1)

        field_definition = ogr.FieldDefn(field_name, data_type)
        layer = data_source.GetLayer()
        layer.CreateField(field_definition)

        # GDAL requires us to delete references for it to flush to disk
        data_source = layer = None

    @staticmethod
    def populate_field_value(shapefile_path, field_name, field_value):
        ogr.UseExceptions()
        gdal.UseExceptions()
        file_utils.ensure_file_exists_and_can_be_opened(shapefile_path)

        driver = ogr.GetDriverByName('ESRI Shapefile')
        data_source = driver.Open(shapefile_path, 1)
        layer = data_source.GetLayer()
        for feat in layer:
            feat.SetField(field_name, field_value)
            layer.SetFeature(feat)

        # GDAL requires us to delete references for it to flush to disk
        data_source = layer = None

    @staticmethod
    def append_shapefile_to_another(source_shapefile_path, shapefile_to_append_path):
        """
        Append one shapefile to another shapefile.
        """
        ogr.UseExceptions()
        gdal.UseExceptions()
        file_utils.ensure_file_exists_and_can_be_opened(source_shapefile_path)
        file_utils.ensure_file_exists_and_can_be_opened(shapefile_to_append_path)

        driver = ogr.GetDriverByName('ESRI Shapefile')
        data_source = driver.Open(source_shapefile_path, 1)
        source_layer = data_source.GetLayer()
        append_source = driver.Open(shapefile_to_append_path, 0)
        append_layer = append_source.GetLayer()

        new_feature = append_layer.GetNextFeature()
        if new_feature:
            new_feature.SetFID(source_layer.GetFeatureCount())
            source_layer.CreateFeature(new_feature)

        # GDAL requires us to delete references for it to flush to disk
        data_source = append_source = source_layer = new_feature = None

    @staticmethod
    def get_shapefile_extent(shapefile_path):
        """
        Assuming shapefile only has one layer, returns the layer's extent: (min_x, max_x, min_y, max_y)
        """
        driver = ogr.GetDriverByName('ESRI Shapefile')
        data_source = driver.Open(shapefile_path, 0)

        layer = data_source.GetLayer()
        extent = layer.GetExtent()
        return extent[0], extent[1], extent[2], extent[3]

    def clip_raster_using_shapefile_bounding_box(self, input_raster_path, clip_shapefile_path, output_raster_path):
        """
        Clips a given raster using the extents of a shapefile, and spits out an output raster.
        """
        ogr.UseExceptions()
        gdal.UseExceptions()
        # Input raster path is in ESRI grid format (i.e. is a directory)
        # Sometimes it isn't though (e.g. mean_ht_3m.tif)
        # file_utils.ensure_directory_exists(input_raster_path)
        file_utils.ensure_file_exists_and_can_be_opened(clip_shapefile_path)

        gdalwarp_path = os.path.join(self.gdal_home, "gdalwarp")
        x_min, x_max, y_min, y_max = self.get_shapefile_extent(clip_shapefile_path)

        gdal_warp_str = '{gdalwarp_path} ' \
                        '-dstnodata -9999 ' \
                        '-wo NUM_THREADS=ALL_CPUS ' \
                        '-co NUM_THREADS=ALL_CPUS ' \
                        '-q -te {georeferenced_extents} ' \
                        '{input_raster_path} {output_raster_path}'

        gdal_warp_command = gdal_warp_str.format(
            gdalwarp_path=gdalwarp_path,
            georeferenced_extents=' '.join(str(xy) for xy in [x_min - 10, y_min - 10, x_max + 10, y_max + 10]),
            input_raster_path=input_raster_path,
            output_raster_path=output_raster_path
        )

        CommandRunner().run(gdal_warp_command)

    def clip_raster_using_minmax_extents(self, input_raster_path, xy_maxmin, output_raster_path, buffer_raster=0):
        """
        Clips a given raster using min and max extents, and spits out an output raster.
        """
        ogr.UseExceptions()
        gdal.UseExceptions()
        # Input raster path is in ESRI grid format (i.e. is a directory)
        # Sometimes it isn't though (e.g. mean_ht_3m.tif)
        # file_utils.ensure_directory_exists(input_raster_path)

        gdalwarp_path = os.path.join(self.gdal_home, "gdalwarp")

        gdal_warp_str = '{gdalwarp_path}.exe ' \
                        '-dstnodata -9999 ' \
                        '-wo NUM_THREADS=ALL_CPUS ' \
                        '-co NUM_THREADS=ALL_CPUS ' \
                        '-q -te {georeferenced_extents} ' \
                        '{input_raster_path} {output_raster_path}'

        gdal_warp_command = gdal_warp_str.format(
            gdalwarp_path=gdalwarp_path,
            georeferenced_extents=' '.join(str(xy) for xy in [xy_maxmin[0] - buffer_raster, xy_maxmin[1] - buffer_raster, xy_maxmin[2] + buffer_raster, xy_maxmin[3] + buffer_raster]),
            input_raster_path=input_raster_path,
            output_raster_path=output_raster_path
        )

        CommandRunner().run(gdal_warp_command)

        return output_raster_path

    def clip_raster_using_shapefile(self, input_raster_path, clip_shapefile_path, output_raster_path):
        """
        Clips a given raster using the specified shapefile, and spits out an output raster.
        """
        ogr.UseExceptions()
        gdal.UseExceptions()
        file_utils.ensure_file_exists_and_can_be_opened(input_raster_path)
        file_utils.ensure_file_exists_and_can_be_opened(clip_shapefile_path)

        """
        Immediately return if there are no features because there is nothing to clip.
        Also, the following gdalwarp command will error out if there are no features in the shapefile.
        """
        if self.shapefile_has_no_features(clip_shapefile_path):
            return

        gdalwarp_path = os.path.join(self.gdal_home, "gdalwarp")

        gdal_warp_str = '{gdalwarp_path} ' \
                        '-dstnodata -9999 ' \
                        '-wo NUM_THREADS=ALL_CPUS ' \
                        '-co NUM_THREADS=ALL_CPUS ' \
                        '-q -cutline ' \
                        '{clip_shapefile} {input_raster_path} {output_raster_path}'

        gdal_warp_command = gdal_warp_str.format(
            gdalwarp_path=gdalwarp_path,
            clip_shapefile=clip_shapefile_path,
            input_raster_path=input_raster_path,
            output_raster_path=output_raster_path
        )

        try:
            CommandRunner().run(gdal_warp_command)
        except Exception as e:
            print("Exception occurred during clipping of raster with shapefile (perhaps a self-intersection issue?). "
                  "Will try again with a de-slivered shapefile.")
            file_utils.delete_file_if_exists(output_raster_path)
            deslivered_shapefile = self.remove_slivers_from_shapefile(clip_shapefile_path)
            gdal_warp_command = gdal_warp_str.format(
                gdalwarp_path=gdalwarp_path,
                clip_shapefile=deslivered_shapefile,
                input_raster_path=input_raster_path,
                output_raster_path=output_raster_path
            )
            CommandRunner().run(gdal_warp_command)

    @staticmethod
    def shapefile_has_no_features(shapefile_path):
        """
        Check if shapefile has no features.
        """
        ogr.UseExceptions()
        gdal.UseExceptions()

        driver = ogr.GetDriverByName('ESRI Shapefile')
        shapefile = driver.Open(shapefile_path, 0)
        shapefile_layer = shapefile.GetLayer()

        return shapefile_layer.GetFeatureCount() == 0

    def separate_band_from_raster(self, input_raster_path, band_number, resolution, output_raster_path):
        """
        Extracts a single band from the provided raster and outputs that into a new raster.
        """
        ogr.UseExceptions()
        gdal.UseExceptions()
        file_utils.ensure_file_exists_and_can_be_opened(input_raster_path)

        gdal_translate_path = os.path.join(self.gdal_home, "gdal_translate")

        gdal_translate_command_list = [gdal_translate_path,
                                       '-co', 'NUM_THREADS=ALL_CPUS', '-q',
                                       '-b', str(band_number),
                                       '-tr', str(resolution), str(resolution),
                                       input_raster_path, output_raster_path]

        CommandRunner().run(gdal_translate_command_list)

    def perform_gdal_calc(self, input_raster_path, calc_expression, output_raster_path):
        gdal_calc_str = 'python {0} -A {1} --A_band=1 --outfile={2} --calc="{3}" --type=Int16 --NoDataValue=0'
        gdal_calc_path_escaped = self.gdal_calc_path.replace(' ', '\\ ')
        gdal_calc_process = gdal_calc_str.format(gdal_calc_path_escaped, input_raster_path,
                                                 output_raster_path, calc_expression)
        CommandRunner().run(gdal_calc_process)

    def remove_slivers_from_shapefile(self, shapefile_path):
        """
        Removes slivers by negatively buffering the polygons and then positively buffering them by an equal amount.
        Small portions will disappear from the r_not buffer, but larger portions will be relatively unaffected.
        """
        negatively_buffered_shapefile = self.buffer_shapefile(shapefile_path, -1)
        positively_buffered_shapefile = self.buffer_shapefile(negatively_buffered_shapefile, 1)
        return positively_buffered_shapefile

    @staticmethod
    def buffer_shapefile(shapefile_path, buffer_amount):
        """
        Buffers a shapefile by a given amount (r_not or positive)
        """

        ogr.UseExceptions()
        gdal.UseExceptions()
        file_utils.ensure_file_exists_and_can_be_opened(shapefile_path)

        output_path = shapefile_path.replace('.shp', '_buffered_{}m.shp'.format(buffer_amount))

        input_datasource = ogr.Open(shapefile_path)
        input_layer = input_datasource.GetLayerByIndex(0)

        driver = ogr.GetDriverByName("ESRI Shapefile")

        if os.path.exists(output_path):
            driver.DeleteDataSource(output_path)

        output_datasource = driver.CreateDataSource(output_path)
        output_layer = output_datasource.CreateLayer(output_path, geom_type=ogr.wkbPolygon)
        layer_definition = output_layer.GetLayerDefn()

        # Copy all the fields
        field_names = []
        for i in range(input_layer.GetLayerDefn().GetFieldCount()):
            field_definition = input_layer.GetLayerDefn().GetFieldDefn(i)
            output_layer.CreateField(field_definition)
            field_names.append(field_definition.name)

        # Copy all the features with slivers removed
        for feature in input_layer:
            original_geom = feature.GetGeometryRef()
            field_values = []
            for field_name in field_names:
                field_values.append(feature.GetField(field_name))

            output_feature = ogr.Feature(layer_definition)
            buffered_geom = original_geom.Buffer(buffer_amount)

            output_feature.SetGeometry(buffered_geom)
            for key, value in enumerate(field_values):
                output_feature.SetField(field_names[key], value)

            output_layer.CreateFeature(output_feature)
            output_feature = None

        input_datasource = output_datasource = output_layer = None

        # Copy the .prj file
        shutil.copyfile(shapefile_path.replace('.shp', '.prj'), output_path.replace('.shp', '.prj'))

        return output_path

    def create_point_shapefile_from_array(self, tile_name, array, shapefile_path, extra_fields=None):
        """
        Creates a shapefile with a point layer based on values inside of a numpy array.

        :param tile_name:      Tile name, formerly known as image name.
        :param array:          An array where each index has 3 elements: (x, y, z)
        :param shapefile_path: Path for output shapefile.
        :param extra_fields:   Adds a field to each element in the array given a list of the following tuple:
                               (field name, array position), where array position is the array index of an array
                               element. (e.g. 2 = Z position of the element, 0 = X)
        :return:
        """
        ogr.UseExceptions()
        type = "POINT"

        driver = ogr.GetDriverByName("ESRI Shapefile")
        datasource = driver.CreateDataSource(shapefile_path)

        if os.path.exists(shapefile_path):
            driver.DeleteDataSource(shapefile_path)

        srs = osr.SpatialReference()
        srs.ImportFromEPSG(self.srid)  # 26911 = srid

        layer = datasource.CreateLayer(tile_name + "_layer", srs, ogr.wkbPoint)

        if extra_fields is not None:
            for field_name, array_position in extra_fields:
                layer_field_name = ogr.FieldDefn(field_name, ogr.OFTReal)
                layer.CreateField(layer_field_name)

        for point in array:
            feature = ogr.Feature(layer.GetLayerDefn())

            for field_name, array_position in extra_fields:
                feature.SetField(field_name, point[array_position])

            x = point[0]
            y = point[1]
            z = point[2]
            output_wkt = type + " ({x} {y} {z})".format(x=x, y=y, z=z)

            shp = ogr.CreateGeometryFromWkt(output_wkt)
            feature.SetGeometry(shp)
            layer.CreateFeature(feature)

        datasource = layer = feature = None

    def clip_shapefile_by_extent(self, input_shp_path, output_shp_path, extent, out_type='POINT'):
        """
        Creates a new shapefile based on points inside input shapefile and within an extent.

        We only handle POINT type shapefiles for now, but it shouldn't be too hard to modify this and handle
        other type of shapefiles.
        """
        ogr.UseExceptions()

        driver = ogr.GetDriverByName("ESRI Shapefile")
        srs = osr.SpatialReference()
        srs.ImportFromEPSG(self.srid)  # 26911 = srid

        input_data_source = driver.Open(input_shp_path, 0)
        output_datasource = driver.CreateDataSource(output_shp_path)

        if os.path.exists(output_shp_path):
            driver.DeleteDataSource(output_shp_path)

        input_layer = input_data_source.GetLayer()
        if out_type.upper() == 'POINT':
            output_layer = output_datasource.CreateLayer(input_layer.GetName(), srs, ogr.wkbPoint)
        else:
            output_layer = output_datasource.CreateLayer(input_layer.GetName(), srs, ogr.wkbPolygon)

        # Copy all the field definitions
        field_names = []
        for i in range(input_layer.GetLayerDefn().GetFieldCount()):
            field_definition = input_layer.GetLayerDefn().GetFieldDefn(i)
            output_layer.CreateField(field_definition)
            field_names.append(field_definition.name)

        min_x, max_x, min_y, max_y = extent
        input_feature = input_layer.GetNextFeature()

        while input_feature is not None:
            geom = input_feature.GetGeometryRef()
            geom_min_x, geom_max_x, geom_min_y, geom_max_y = geom.GetEnvelope()

            if min_x <= geom_min_x <= max_x and min_y <= geom_min_y <= max_y:
                # Create a copy of input features if it's within extent
                output_feature = ogr.Feature(output_layer.GetLayerDefn())
                output_feature.SetGeometry(geom)
                for field_name in field_names:
                    output_feature.SetField(field_name, input_feature.GetField(field_name))

                output_layer.CreateFeature(output_feature)

            input_feature = input_layer.GetNextFeature()
            output_feature = None

        output_layer = output_datasource = input_feature = None

    def get_coordinates_as_array(self, in_data, return_field_value_array=None, return_z=False):
        # in_data right now only tested for points
        out_array = []

        driver = ogr.GetDriverByName('ESRI Shapefile')
        dataSource = driver.Open(in_data, 0)
        layer = dataSource.GetLayer()

        for feat in layer:
            point = feat.GetGeometryRef()

            if return_field_value_array is not None:

                values = [feat.GetField(j) for j in return_field_value_array]

                if not return_z:
                    out_array.append([point.GetX(), point.GetY()] + values)
                else:
                    out_array.append([point.GetX(), point.GetY(), point.GetZ()] + values)

            elif return_z:
                out_array.append([point.GetX(), point.GetY(), point.GetZ()])

            else:
                out_array.append([point.GetX(), point.GetY()])


        return out_array

    def open_raster(self, in_raster, write=False):
        open_status = gdal.GA_ReadOnly
        if write:
            open_status = gdal.GA_Update
        return gdal.Open(in_raster, open_status)

    def get_raster_extent(self, gdal_raster_object):

        geoTransform = gdal_raster_object.GetGeoTransform()
        minx = geoTransform[0]
        maxy = geoTransform[3]
        maxx = minx + geoTransform[1] * gdal_raster_object.RasterXSize
        miny = maxy + geoTransform[5] * gdal_raster_object.RasterYSize

        return minx, miny, maxx, maxy

    def raster_to_array(self, open_raster_object, band=1):
        """
        needs an np.array() wrapper to get into numpy array
        array is returned with x, y @ (0,0) = top left - need to flip to get to voxel index position so that (0,0) is in the bottom left
        """
        return open_raster_object.GetRasterBand(band).ReadAsArray()

    def get_raster_row_col_count(self, open_raster_object):

        return int(open_raster_object.RasterXSize), int(open_raster_object.RasterYSize)

    def get_raster_cellsize(self, open_raster_object):
        """ assume square cell"""

        return open_raster_object.GetGeoTransform()[1]

    def close_raster(self, open_raster_object):

        open_raster_object = None

        return True

    def get_field_name_list(self, in_shp):

        driver = ogr.GetDriverByName("ESRI Shapefile")
        srcShp1 = driver.Open(in_shp)
        srcLay1 = srcShp1.GetLayer()
        ldefn = srcLay1.GetLayerDefn()

        field_names = []
        for n in range(ldefn.GetFieldCount()):
            fdefn = ldefn.GetFieldDefn(n)
            field_names.append(fdefn.name)

        return field_names

    def point_to_array_for_proximity_analysis(self, point_file, dataset_name, plot_shape, plot_name, dict_key, shape_dict):

        driver = ogr.GetDriverByName("ESRI Shapefile")
        point_source = driver.Open(point_file)
        point_source_layer = point_source.GetLayer()
        layer_definition = point_source_layer.GetLayerDefn()

        polygon_source = driver.Open(plot_shape)
        polygon_source_layer = polygon_source.GetLayer()

        point_field_names = []
        for field in range(layer_definition.GetFieldCount()):
            point_field_names.append(layer_definition.GetFieldDefn(field).name.upper())

        for point_feat in point_source_layer:

            species = None
            if 'SPECIES' in point_field_names:
                species = point_feat.GetField('SPECIES')

            dbh = None
            if 'DBH_HUANG' in point_field_names:
                dbh = point_feat.GetField('DBH_HUANG')
            elif 'DBH' in point_field_names:
                dbh = point_feat.GetField('DBH')

            point_geom = point_feat.GetGeometryRef()

            z = point_geom.GetZ()
            ht = None
            if 'height' in point_field_names:
                ht = 'height'
            if 'TREE_HT' in point_field_names:
                ht = 'TREE_HT'
            if 'HEIGHT' in point_field_names:
                ht = 'HEIGHT'
            if ht:
                z = point_feat.GetField(ht)

            if z < 10:
                continue

            if 'POINT_X' not in point_field_names:
                x = point_geom.GetX()
                y = point_geom.GetY()
            else:
                x = point_feat.GetField('POINT_X')
                y = point_feat.GetField('POINT_Y')

                wkt = f'POINT ({x} {y})'
                point_geom = ogr.CreateGeometryFromWkt(wkt)

            plot = None
            for polygon_feature in polygon_source_layer:
                polygon_geom = polygon_feature.GetGeometryRef()

                if polygon_geom.Distance(point_geom) > 5:
                    # this would cover the center of the plot and then some in case values increase and decrease from
                    # the polygon edge line - plot = None
                    continue
                else:
                    plot = polygon_feature.GetField(plot_name)

                    if polygon_geom.Intersects(point_geom):
                        plot = plot + '_inplot'
                    break
            if plot is None:
                continue
            dict_key += 1
            shape_dict[dict_key] = [x]
            shape_dict[dict_key].append(y)
            shape_dict[dict_key].append(z)
            shape_dict[dict_key].append(species)
            shape_dict[dict_key].append(dbh)
            shape_dict[dict_key].append(dataset_name)
            shape_dict[dict_key].append(plot)
            # if dataset_name == 'field':
            #     break
        return dict_key

    def point_to_array(self, point_file, fields=None):

        master = []
        driver = ogr.GetDriverByName("ESRI Shapefile")
        point_source = driver.Open(point_file)
        point_source_layer = point_source.GetLayer()
        layer_definition = point_source_layer.GetLayerDefn()

        for point_feat in point_source_layer:
            subordinate = []
            point_geom = point_feat.GetGeometryRef()
            master = [point_geom.GetX(), point_geom.GetY(), point_geom.GetZ()]

            if isinstance(fields, list):
                for field in fields:
                    subordinate.append(point_feat.GetField(field))
            else:
                for n in range(layer_definition.GetFieldCount()):
                    field = layer_definition.GetFieldDefn(n).name
                    subordinate.append(point_feat.GetField(field))
            master.append(subordinate)

        return master

    def point_in_poly(self, point_file, polygon_file, polygon_field_return=None, point_field_return=None, default_z=None):
        """needs a np.array wrapper when returned"""
        complete_array = []
        driver = ogr.GetDriverByName("ESRI Shapefile")
        point_source = driver.Open(point_file)
        point_source_layer = point_source.GetLayer()

        polygon_source = driver.Open(polygon_file)
        polygon_source_layer = polygon_source.GetLayer()
        for point_feat in point_source_layer:
            point_geom = point_feat.GetGeometryRef()
            if default_z:
                subordinate = [point_geom.GetX(), point_geom.GetY(), default_z]
            else:
                if point_geom.GetZ() == 0:
                    try:
                        z = point_feat.GetField('tree_ht')
                    except:
                        z = point_geom.GetZ()
                else:
                    z = point_geom.GetZ()
                subordinate = [point_geom.GetX(), point_geom.GetY(), z]

            if isinstance(point_field_return, list):
                for field in point_field_return:
                    subordinate.append(point_feat.GetField(field))
            elif point_field_return is None:
                pass
            else:
                subordinate.append(point_field_return)

            for polygon_feature in polygon_source_layer:
                polygon_geom = polygon_feature.GetGeometryRef()
                if polygon_geom.Intersects(point_geom):
                    if isinstance(polygon_field_return, list):
                        for field in polygon_field_return:
                            subordinate.append(polygon_feature.GetField(field))
                    elif polygon_field_return is None:
                        pass
                    else:
                        subordinate.append(polygon_feature.GetField(polygon_field_return))
                    complete_array.append(subordinate)
                    break
        return complete_array

    def create_point_shapefile_from_list(self, shp_list, x_y_z_position, output_shp, field=None, plot_z=False):
        # only point for now
        # tuple format = [[x, y, {z}, [field_value1, field_value2,...]]]
        # field format = [['field_name', 'field_type', tuple_position]]

        ogr.UseExceptions()

        driver = ogr.GetDriverByName("ESRI Shapefile")
        datasoutput = driver.CreateDataSource(output_shp)

        if os.path.exists(output_shp):
            driver.DeleteDataSource(output_shp)

        srs = osr.SpatialReference()
        srs.ImportFromEPSG(26911)

        layer = datasoutput.CreateLayer("alayer", srs, ogr.wkbPoint)

        field_names = []
        f_type = None

        if field is not None:

            for f in field:
                if f[1].upper() in ('TEXT', 'STRING', 'CHARACTER'):
                    f_type = ogr.OFTString
                elif f[1].upper() in ('INTEGER'):
                    f_type = ogr.OFTInteger
                elif f[1].upper() in ('DOUBLE', 'FLOAT', 'REAL'):
                    f_type = ogr.OFTReal
                field_name = ogr.FieldDefn(f[0], f_type)
                layer.CreateField(field_name)

                field_names.append([f[0], f[2]])

        #####TODO      CURRENTLY CAN"T GET THIS TO WORK FOR ANYTHING BUT POINTS
        index_a = 0
        # load array of shape type in wkb/t and populate field values of feature
        while index_a < len(shp_list):  # e.g., count of records - i.e., 880,000

            feature = ogr.Feature(layer.GetLayerDefn())
            geometry_building = []
            element_a = shp_list[index_a]

            x = element_a[x_y_z_position[0]]
            y = element_a[x_y_z_position[1]]

            for _name, _pos in field_names:

                set_value = shp_list[index_a][_pos]
                feature.SetField(_name, set_value)

            if plot_z:
                z = element_a[x_y_z_position[2]]
                geometry_building.append(['{x} {y} {z}'.format(x=x, y=y, z=z)])
            geometry_building.append(['{x} {y}'.format(x=x, y=y)])

            wkt_building = ', '.join(x[0] for x in geometry_building)
            wkt = f'POINT ({wkt_building})'
            if plot_z:
                wkt = 'POINT Z(' + wkt_building + ')'

            shp = ogr.CreateGeometryFromWkt(wkt)

            feature.SetGeometry(shp)
            layer.CreateFeature(feature)

            index_a += 1
        feature = None

        return

    def get_raster_band_count(self, open_raster_object):
        return open_raster_object.RasterCount

    def get_raster_band_data_type(self, open_raster_object, band=1):
        return open_raster_object.GetRasterBand(band).DataType
