"""
Contains the Logger class.
"""
import json
import platform
import sys
from datetime import datetime

from core.rabbitmq_service import RabbitMqService
from core.queue import Queue


class Logger:
    """
    Prints logs to stdout and persists logs to the message queue.
    """

    def __init__(self, classname, connection=None):
        self.classname = classname
        self.rabbitmq_service = RabbitMqService()
        self.channel = None
        self.rabbitmq_warning_shown = False

        if connection:
            self.set_channel(connection)

    def set_channel(self, connection):
        self.channel = self.rabbitmq_service.channel(connection)

    def log(self, tile_name, message):
        log_message = "{0} [{1}]: {2}".format(self.classname, tile_name, message)

        # Print the message to stdout
        print(log_message)

        # Log the message to RabbitMQ
        if self.channel is not None:
            message = {
                "timestamp": datetime.now().isoformat(),
                "host": platform.node(),
                "message": log_message
            }
            self.channel.basic_publish("",
                                       Queue().LOGGING,
                                       json.dumps(message).encode("utf-8"),
                                       self.rabbitmq_service.message_properties())
        elif not self.rabbitmq_warning_shown:
            print("WARNING: Previous log message was not sent to RabbitMQ. This warning will only appear once to avoid "
                  "clogging our test environment with duplicate log messages.")
            self.rabbitmq_warning_shown = True

        sys.stdout.flush()
