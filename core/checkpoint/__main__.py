"""
Run this module if you want to disable all checkpoints.
"""
from core.checkpoint.checkpoint_service import CheckpointService
from core.db_connection import DbConnection


def delete_all_checkpoints():
    db_connection = DbConnection()
    checkpoint_service = CheckpointService(db_connection)
    checkpoint_service.clear_all_checkpoints()


if __name__ == "__main__":
    delete_all_checkpoints()
