"""
Contains the CheckpointService class.
"""

class CheckpointService:
    """
    Service responsible for creating/deleting tile checkpoints.
    Currently, this service is used to add checkpoints to AttributeProcessor's queries.
    """

    def __init__(self, db_connection):
        self.db_connection = db_connection

    def checkpoint_exists(self, tile_name, class_name, action):
        """
        Given tile name and its action, check if the checkpoint exists.
        """
        query = f"SELECT count(*) " \
                f"FROM tile_checkpoint " \
                f"WHERE tile_name = '{tile_name}' " \
                f"  AND class_name = '{class_name}' " \
                f"  AND action = '{action}'"
        return self.db_connection.execute(query)[0][0][0] == 1

    def checkpoint_exists_for_tile(self, tile_name):
        """
        Returns true if there is at least one checkpoint for the specified tile. Otherwise, return false.
        """
        query = f"SELECT count(*) " \
                f"FROM tile_checkpoint " \
                f"WHERE tile_name = '{tile_name}'"
        return self.db_connection.execute(query)[0][0][0] == 1

    def update_checkpoint(self, tile_name, class_name, action):
        """
        Creates or replaces an existing checkpoint.
        """
        query = f"DELETE FROM tile_checkpoint " \
                f"WHERE tile_name = '{tile_name}' " \
                f"  AND class_name = '{class_name}' " \
                f"  AND action = '{action}'; " \
                f"" \
                f"INSERT INTO tile_checkpoint (tile_name, class_name, action, timestamp) " \
                f"VALUES ('{tile_name}', '{class_name}', '{action}', now());"
        self.db_connection.execute(query)

    def clear_checkpoint(self, tile_name):
        """Deletes all checkpoints under a specific tile name."""
        query = f"DELETE FROM tile_checkpoint WHERE tile_name = '{tile_name}'"
        self.db_connection.execute(query)

    def clear_all_checkpoints(self):
        """Deletes all checkpoints."""
        query = "TRUNCATE tile_checkpoint"
        self.db_connection.execute(query)
