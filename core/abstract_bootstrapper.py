"""
Contains the class AbstractBootstrapper.
"""
from abc import ABCMeta, abstractmethod


class AbstractBootstrapper(metaclass=ABCMeta):
    """
    Interface that bootstrapper classes must implement.
    """

    @abstractmethod
    def __init__(self,
                 number_of_projection,
                 number_of_clip,
                 number_of_bare_earth,
                 number_of_height_above_ground,
                 number_of_height_filter,
                 number_of_stem_finder,
                 number_of_finalize_clip,
                 number_of_pg_loader,
                 number_of_attribute,):
        """
        Make sure to add new processors to the constructor - every sequencer should be able to create every processor
        in the current pipeline.
        """
        pass

    @abstractmethod
    def run(self):
        """
        Creates all the processors/runs all the processors.
        """
        raise NotImplementedError("Subclass of AbstractBootstrapper does not implement run()")
