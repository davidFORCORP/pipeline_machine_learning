from core.config import Config


class Queue:
    """
    Structured RabbitMQ Queues
    """

    def __init__(self):
        config = Config()
        version = config.get("global", "version")

        # Global (ssim.[version].[purpose])
        self.LOGGING = "ssim.{0}.log".format(version)

        # Queues (ssim.[version].queue.[order].[name])
        self.QUEUE_PROJECTION = "ssim.{0}.queue.00.projection".format(version)
        self.QUEUE_CLIP = "ssim.{0}.queue.10.clip".format(version)
        self.QUEUE_BARE_EARTH = "ssim.{0}.queue.20.bare-earth".format(version)
        self.QUEUE_HEIGHT_ABOVE_GROUND = "ssim.{0}.queue.30.height-above-ground".format(version)
        self.QUEUE_HEIGHT_FILTER = "ssim.{0}.queue.40.height-filter".format(version)
        self.QUEUE_STEM_FINDER = "ssim.{0}.queue.50.stem-finder".format(version)
        self.QUEUE_FINALIZE_CLIP = "ssim.{0}.queue.60.finalize-clip".format(version)
        self.QUEUE_PGLOADER = "ssim.{0}.queue.70.pgloader".format(version)
        self.QUEUE_ATTRIBUTE = "ssim.{0}.queue.80.attribute".format(version)
        self.QUEUE_ELANDS_EXPORT = "ssim.{0}.queue.90.elands-export".format(version)
        self.QUEUE_MERCHANDIZING = "ssim.{0}.queue.100.merchandizing".format(version)
        self.QUEUE_CLEANUP = "ssim.{0}.queue.110.cleanup".format(version)
        # Stages from stage 2 that need to be updated
        self.STAGE_ELANDS_EXPORT_COMPLETED = "ssim.{0}.queue.60.elands-export-completed".format(version)
        self.STAGE_POSTGRESQL_CLEANED = "ssim.{0}.queue.70.postgresql-cleaned".format(version)

        # Potentially deprecated stages
        self.STAGE_MERCHANDIZING_CALCULATED = "ssim.{0}.queue.50.merchandizing-calculated".format(version)
        self.STAGE_ELANDS_EXPORT_COMPLETED = "ssim.{0}.queue.60.elands-export-completed".format(version)
        self.STAGE_POSTGRESQL_CLEANED = "ssim.{0}.queue.70.postgresql-cleaned".format(version)
