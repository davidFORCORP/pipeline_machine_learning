import pdal
import json
import os
from osgeo import ogr

class PdalService:
    def __init__(self) -> None:
        super().__init__()

    @staticmethod
    def clip_las_by_polygons(in_las, clipping_polygons, out_las):
        driver = ogr.GetDriverByName("ESRI Shapefile")
        dataSource = driver.Open(clipping_polygons, 0)
        layer = dataSource.GetLayer()

        geom = None
        for feature in layer:
            geom = feature.GetGeometryRef().ExportToWkt()
        del layer, dataSource

        clip_by_polygons = {"pipeline": [
            {"type": "readers.las", "filename": in_las},
            {
                "type": "filters.crop",
                "polygon": geom
            },
            {
                "type": "filters.sample",
                "radius": 0.01
            },
            {"type": "writers.las", "filename": out_las, "forward": "all"}]}

        pipeline = pdal.Pipeline(json.dumps(clip_by_polygons))
        pipeline.validate()
        pipeline.execute()

    @staticmethod
    def las_slicer(in_las, out_las, expression='(Z >= 0 && Z <= 100)'):

        las_slice = {"pipeline": [
            {"type": "readers.las", "filename": in_las},
            {
                "type": "filters.expression",
                "expression": expression
            },
            {
                "type": "writers.las",
                "filename": out_las,
                "forward": "all"
            }
        ]}
        pipeline = pdal.Pipeline(json.dumps(las_slice))
        pipeline.validate()
        pipeline.execute()

    @staticmethod
    def li2012_segtree(in_hag_las, out_las_data_file_location, min_height_of_tree=3, min_points_in_tree=25, search_radius=100, max_z=None):

        out_las_data_list = []

        if isinstance(in_hag_las, list):

            for i, las_file in enumerate(in_hag_las):

                las_name = os.path.splitext(os.path.basename(las_file))[0]  # file name no extension or folder location
                out_las_data_name = 'segtree_' + las_name + '.las'

                seg_tree_location = out_las_data_file_location + '/seg_tree/'
                out_hag_data = seg_tree_location + out_las_data_name

                if not os.path.isdir(seg_tree_location):
                    os.makedirs(seg_tree_location)

                if max_z is not None:
                    min_height_of_tree = int(max_z[i] - min_height_of_tree)

                self.li2012_segtree_worker(las_file, out_hag_data, min_height_of_tree=min_height_of_tree, min_points_in_tree=min_points_in_tree, search_radius=search_radius)
                out_las_data_list.append(out_hag_data)
            me = 1
        else:

            seg_tree_location = os.path.basename(in_hag_las) + '/seg_tree/'
            out_hag_data = seg_tree_location + 'segtree_full_tile.las'
            self.li2012_segtree_worker(in_hag_las, out_hag_data, min_height_of_tree=min_height_of_tree, min_points_in_tree=min_points_in_tree, search_radius=search_radius)

            return out_hag_data

        return out_las_data_list

    @staticmethod
    def li2012_segtree_worker(in_las, out_las, min_height_of_tree=3, min_points_in_tree=25, search_radius=20):

        segTree = {"pipeline": [
            {"type": "readers.las",
             "filename": in_las,
             "extra_dims": "HeightAboveGround=float32",
             # "use_eb_vlr": True
             },
            {
                "type": "filters.voxeldownsize",
                "cell": 0.1,
                "mode": "first"
            },
            {
                "type": "filters.litree",
                "min_points": min_points_in_tree,
                "min_height": min_height_of_tree,
                "radius": search_radius
            },
            {
                "type": "writers.las",
                "filename": out_las,
                "extra_dims": "all"
            }]}
        pipeline = pdal.Pipeline(json.dumps(segTree))
        pipeline.validate()
        pipeline.execute()

    @staticmethod
    def create_mesh(in_las, out_glb):

        pdal_command = {"pipeline": [
            {"type": "readers.las",
             "filename": in_las,
             },
            {
                "type": "filters.delaunay"
            },
            {
                "type": "writers.ply",
                "filename": out_glb,
                "faces": True
            }
            #
            # {
            #     "type":"writers.gltf",
            #     "filename": out_glb,
            #     "red": 0.8,
            #     "metallic": 0.5
            # }
        ]}
        print('.........processing')
        pipeline = pdal.Pipeline(json.dumps(pdal_command))
        pipeline.validate()
        pipeline.execute()

    @staticmethod
    def execute_eigenvalues_for_vision(in_las, out_las, expression, knn):

        vision = {"pipeline": [
            {"type": "readers.las", "filename": in_las},
            {
                "type": "filters.expression",
                "expression": expression
            },
            {
                "type": "filters.eigenvalues",
                "knn": knn
            },
            {
                "type": "writers.las",
                "filename": out_las,
                "forward": "all"
            }
        ]}

        pipeline = pdal.Pipeline(json.dumps(vision))
        pipeline.validate()
        pipeline.execute()

    @staticmethod
    def execute_eigenvalues(in_las, out_las, knn):

        # closer to 1 the less affected by the eigen transformation the eigen vector was

        pdal_command = {"pipeline": [
            {"type": "readers.las",
             "filename": in_las
             },
            {
                "type": "filters.eigenvalues",
                "knn": knn
            },
            {
                "type": "writers.las",
                "extra_dims": "all",
                # "vlrs":
                #     [{
                #     "description": "Eigen0",
                #     "record_id": 99,
                #     "user_id": "dmhc",
                #     "data": "Eigenvalue0"
                #     },
                #     {
                #         "description": "Eigen1",
                #         "record_id": 98,
                #         "user_id": "dmhc",
                #         "data": "Eigenvalue1"
                #     },
                #     {
                #         "description": "Eigen2",
                #         "record_id": 97,
                #         "user_id": "dmhc",
                #         "data": "Eigenvalue2"
                #     }],
                "filename": out_las

            }
        ]}

        pipeline = pdal.Pipeline(json.dumps(pdal_command))
        pipeline.validate()
        pipeline.execute()

    @staticmethod
    def decimate(in_las, out_las):
        pdal_command = {"pipeline": [
            {
                "type": "readers.las",
                "filename": in_las
            },
            {
                "type": "filters.decimation",
                "step": 10
            },
            {
                "type": "writers.las",
                "filename": out_las
            }
        ]}

        print('...processing PDAL')
        pipeline = pdal.Pipeline(json.dumps(pdal_command))
        pipeline.validate()
        pipeline.execute()

    def merge(self, las_list, out_las):

        pdal_command = {"pipeline": [
            {"type": "readers.las",
             "filename": las_list,
             },
            {
                "type": "filters.merge"
            },
            {
                "type": "writers.las",
                "extra_dims": "all",
                "filename": out_las
            }
            ]}
        pipeline = pdal.Pipeline(json.dumps(pdal_command))
        pipeline.validate()
        pipeline.execute()

    @staticmethod
    def execute_pipeline(pipe):
        pipeline = pdal.Pipeline(json.dumps(pipe))
        pipeline.validate()
        pipeline.execute()
