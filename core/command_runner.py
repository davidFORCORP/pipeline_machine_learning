import os
import subprocess
import sys


class CommandRunner:

    @staticmethod
    def run(command):
        """
        Runs a command, and waits for it to return. Throws
        an exception if the return value is not 0.
        """

        environment = os.environ.copy()

        process = subprocess.Popen(command,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE,
                                   env=environment,
                                   shell=True)

        # Infinitely loop, printing stdout until the command completes
        while True:
            output = process.stdout.readline()
            if output == b'' and process.poll() is not None:
                break
            if output:
                print(str(output, 'utf-8'))

        # Print stderr if there is any
        errors = process.stderr.readlines()
        if errors is not None:
            for line in errors:
                print(str(line, 'utf-8'), file=sys.stderr)

        return_value = process.poll()

        process.stdout.close()
        process.stderr.close()

        if return_value != 0:
            raise Exception("Command resulted in non-zero return value [{}]".format(command))
