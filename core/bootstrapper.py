"""
Contains the Bootstrapper class.
"""
import multiprocessing
import setproctitle
import time

from core.abstract_bootstrapper import AbstractBootstrapper
from core.rabbitmq_service import RabbitMqService
from processor.attribute.attribute_processor import AttributeProcessor
from processor.bare_earth.bare_earth_processor import BareEarthProcessor
from processor.clip.clip_processor import ClipProcessor
from processor.elands_import.elands_export_processor import ElandsExportProcessor
from processor.finalize_clip.finalize_clip_processor import FinalizeClipProcessor
from processor.height_above_ground.height_above_ground_processor import HeightAboveGroundProcessor
from processor.height_filter.height_filter_processor import HeightFilterProcessor
from processor.pgloader.pg_loader_processor import PgLoaderProcessor
from processor.projection.projection_processor import ProjectionProcessor
from processor.stem_finder.stem_finder_processor import StemFinderProcessor


class Bootstrapper(AbstractBootstrapper):
    """
    A convenience class of starting up many processors at once.

    Most useful when you want to specify the number of processors running on one VM at the same time. This can be more
    beneficial than other bootstrappers if we ever create more than 1 SSIM VM.
    """

    def __init__(self,
                 number_of_projection,
                 number_of_clip,
                 number_of_bare_earth,
                 number_of_height_above_ground,
                 number_of_height_filter,
                 number_of_stem_finder,
                 number_of_finalize_clip,
                 number_of_pg_loader,
                 number_of_attribute,
                 number_of_elands_export,):
        self.number_of_projection = number_of_projection
        self.number_of_clip = number_of_clip
        self.number_of_bare_earth = number_of_bare_earth
        self.number_of_height_above_ground = number_of_height_above_ground
        self.number_of_height_filter = number_of_height_filter
        self.number_of_stem_finder = number_of_stem_finder
        self.number_of_finalize_clip = number_of_finalize_clip
        self.number_of_pg_loader = number_of_pg_loader
        self.number_of_attribute = number_of_attribute
        self.number_of_elands_export = number_of_elands_export

    def run(self):
        processes = self.assemble_processes()

        for process in processes:
            process.start()
            time.sleep(1)

    def assemble_processes(self):
        processes = []

        if self.number_of_projection:
            for i in range(self.number_of_projection):
                name = 'projection-processor-{}'.format(i)
                processes.append(self.build_process(name, 'projection'))

        if self.number_of_clip:
            for i in range(self.number_of_clip):
                name = 'clip-processor-{}'.format(i)
                processes.append(self.build_process(name, 'clip'))

        if self.number_of_bare_earth:
            for i in range(self.number_of_bare_earth):
                name = 'bare-earth-processor-{}'.format(i)
                processes.append(self.build_process(name, 'bare_earth'))

        if self.number_of_height_above_ground:
            for i in range(self.number_of_height_above_ground):
                name = 'height-above-ground-processor-{}'.format(i)
                processes.append(self.build_process(name, 'height_above_ground'))

        if self.number_of_height_filter:
            for i in range(self.number_of_height_filter):
                name = 'height-filter-processor-{}'.format(i)
                processes.append(self.build_process(name, 'height_filter'))

        if self.number_of_stem_finder:
            for i in range(self.number_of_stem_finder):
                name = 'stem-finder-processor-{}'.format(i)
                processes.append(self.build_process(name, 'stem_finder'))

        if self.number_of_finalize_clip:
            for i in range(self.number_of_finalize_clip):
                name = 'finalize-clip-processor-{}'.format(i)
                processes.append(self.build_process(name, 'finalize_clip'))

        if self.number_of_pg_loader:
            for i in range(self.number_of_pg_loader):
                name = 'pgloader-processor-{}'.format(i)
                processes.append(self.build_process(name, 'pgloader'))

        if self.number_of_attribute:
            for i in range(self.number_of_attribute):
                name = 'attribute-processor-{}'.format(i)
                processes.append(self.build_process(name, 'attribute'))

        if self.number_of_elands_export:
            for i in range(self.number_of_elands_export):
                name = 'elands-export-processor-{}'.format(i)
                processes.append(self.build_process(name, 'elands_export'))

        return processes

    @staticmethod
    def build_process(name, processor_type):
        return multiprocessing.Process(name=name, target=run_processor, args=(processor_type,))


def run_processor(processor_type):
    """
    This is the actual function that will run in each individual process.
    """

    # Sets the OS name for this process so it's visible in "htop" as "ps"
    setproctitle.setproctitle(multiprocessing.current_process().name)

    # Build and run the actual processor
    processor = None
    rabbitmq_service = RabbitMqService()

    if processor_type == 'projection':
        processor = ProjectionProcessor(rabbitmq_service)
    elif processor_type == 'clip':
        processor = ClipProcessor(rabbitmq_service)
    elif processor_type == 'bare_earth':
        processor = BareEarthProcessor(rabbitmq_service)
    elif processor_type == 'height_above_ground':
        processor = HeightAboveGroundProcessor(rabbitmq_service)
    elif processor_type == 'height_filter':
        processor = HeightFilterProcessor(rabbitmq_service)
    elif processor_type == 'stem_finder':
        processor = StemFinderProcessor(rabbitmq_service)
    elif processor_type == 'finalize_clip':
        processor = FinalizeClipProcessor(rabbitmq_service)
    elif processor_type == 'pgloader':
        processor = PgLoaderProcessor(rabbitmq_service)
    elif processor_type == 'attribute':
        processor = AttributeProcessor(rabbitmq_service)
    elif processor_type == 'elands_export':
        processor = ElandsExportProcessor(rabbitmq_service)

    # Begin!
    processor.begin()
