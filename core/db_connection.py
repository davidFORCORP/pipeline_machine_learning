"""Contains the DbConnection class."""
import psycopg2
import time

from core.db_connection_reconnect_error import DbConnectionReconnectError
from core.config import Config


class DbConnection:
    """
    Responsible for querying the database/schema related to ssim-pipeline.
    """

    def __init__(self):
        config = Config()
        self.host = config.get("db", "host")
        self.database = config.get("db", "database")
        self._schema = config.get("db", "schema")
        self.user = config.get("db", "user")
        self.password = config.get("db", "password")
        self._last_schema_used = None

        self.connection = self.get_connection()

    @property
    def schema(self):
        return self._schema

    @property
    def last_schema_used(self):
        return self._last_schema_used

    @last_schema_used.setter
    def last_schema_used(self, var):
        self._last_schema_used = var

    def get_connection(self, override_schema=None):
        # Establish a single connection that we can re-use for the lifecycle
        # of this class.
        self.connection = psycopg2.connect(host=self.host, database=self.database, user=self.user,
                                           password=self.password)

        # Automatically commit every query (no transaction)
        # This is necessary to use VACUUM
        self.connection.autocommit = True

        # Automatically set the search path so we don't have to provide the schema
        # name in every query we write.
        self.set_search_path(schema=self.last_schema_used)
        if override_schema is not None:
            self.set_search_path(schema=override_schema)

        return self.connection

    def set_search_path(self, schema=None):
        """
        Sets the search path.
        """
        if schema is None:
            self.execute(f"SET search_path TO {self.schema}, public")
        else:
            self.last_schema_used = schema
            self.execute(f"SET search_path TO {schema}, {self.schema}, public")

    def execute(self, query, variables=None, exception_on_reconnect=False):
        """
        Executes a single query.
        """
        cursor = None
        query_executed = False

        # Try catch block to send query execution to reconnect() in case something goes wrong
        while not query_executed:
            try:
                # Execute query
                cursor = self.connection.cursor()
                if isinstance(query, list):
                    query = ';'.join(query)

                cursor.execute(query, variables)
                query_executed = True
            except (psycopg2.OperationalError, psycopg2.InterfaceError) as err:
                print("Query failed with error:" + str(err))
                needed_to_reconnect = self.reconnect()
                if not needed_to_reconnect:
                    raise RuntimeError("Query failed with non-reconnection error: " + str(err)) from err
                if exception_on_reconnect:
                    raise DbConnectionReconnectError("Force throwing reconnection error.") from err

        if cursor.rowcount == 0:
            # This is a SELECT that return no rows
            return None
        elif cursor.rowcount == 1 and cursor.lastrowid:
            # This is an INSERT that affected 1 row.
            return cursor.lastrowid
        elif cursor.description is None:
            # This is not a SELECT - it must be an update
            return None
        else:
            return cursor.fetchall(), cursor.description

    def close(self):
        self.connection.close()

    def reconnect(self):
        """
        First checks out if connection is still valid.
        If it isn't, try to reconnect to the database every 5 minutes.

        :returns False if did not need to reconnect to database (likely query error), True otherwise
        """
        retry_attempts = 0
        max_retries = 5
        query_executed = False

        while not query_executed:
            try:
                if self.connection is None:
                    self.connection = self.get_connection()  # Re-connect

                # Execute query
                cursor = self.connection.cursor()
                cursor.execute("SELECT 1", None)
                query_executed = True
            except (psycopg2.OperationalError, psycopg2.InterfaceError) as err:
                # These errors are likely due to a terminated database connection
                # Try 5 minute attempts at re-connecting (for 30 minutes max)
                retry_attempts += 1
                print("Connection likely failed, reconnect attempt #" + str(retry_attempts))
                time.sleep(300)
                self.connection = None

                if retry_attempts > max_retries:
                    raise RuntimeError("Failed to reconnect to database after 30 minutes: " + str(err)) from err
        return retry_attempts != 0

    @staticmethod
    def epoch_to_postgresql_timestamp(epoch_datetime):
        return epoch_datetime.strftime("%a, %d %b %Y %H:%M:%S +0000")

    def pgsql_fetch(self, command, return_array=False, override_schema=None):
        """

        :param command: the command to execute
        :param return_array: flag to return entire queried array (true) or to return a 1 record request
        :return:
        """

        # retruns [(field1, field2...)]
        pg_connection = self.get_connection(override_schema=override_schema)
        pg_cursor = pg_connection.cursor()
        pg_cursor.execute(command)

        if return_array:
            return pg_cursor.fetchall()

        return pg_cursor.fetchall()[0][0]

    def insert_data_to_postgre_table(self, input_data, table_name, field_parameters, reset_table=True, logging=True):
        """
        loads tabular data into postgre
        """
        field_parameters = ['voxel_index int'] + field_parameters
        table_field_list = ', '.join(column for column in field_parameters)

        self.execute(f"SET search_path = {self.schema}, public")
        if reset_table:
            self.execute(f"DROP TABLE IF EXISTS {table_name}")
            self.execute(f"CREATE UNLOGGED TABLE {table_name} ({table_field_list})")

        column_names = ', '.join(x.split(' ')[0] for x in field_parameters)
        insert_statement = []
        if isinstance(input_data, dict):
            for i, item in enumerate(input_data.items()):
                key = item[0]
                insert_list = f"{key}, {', '.join(str(x) for x in input_data[key])}"
                insert_statement.append(f"INSERT INTO {table_name} ({column_names}) VALUES ({insert_list})")
                if i % 10000 == 0:
                    self.execute(insert_statement)
                    insert_statement = []
        else:
            for i, item in enumerate(input_data):
                key = item[0]
                insert_list = f"{key}, {', '.join(str(x) for x in item[1:])}"
                insert_statement.append(f"INSERT INTO {table_name} ({column_names}) VALUES ({insert_list})")
                if i % 10000 == 0:
                    self.execute(insert_statement)
                    insert_statement = []

        # just to grab any bits past the last batch insert
        self.execute(insert_statement)

        add_primary_key = f'ALTER TABLE {table_name} ADD PRIMARY KEY (voxel_index)'
        self.execute(add_primary_key)
        if logging and not self.is_table_logged(table_name):
            self.execute(f"ALTER TABLE {table_name} SET LOGGED")

    def join_data_to_postgre_table(self, input_data, append_table_name, field_parameters, join_key='voxel_index'):
        """
        appends dictionary data to existing tabular data in postgre
        VOXEL_INDEX implicitly applied in definitions here at index position 0
        """
        temp_table_name = f'temp_{append_table_name}'
        self.insert_data_to_postgre_table(input_data, temp_table_name, field_parameters, logging=False)

        temp2_table_name = f'temp2_{append_table_name}'

        list_of_fields = f"" \
                         f"SELECT column_name " \
                         f"FROM information_schema.columns " \
                         f"WHERE table_schema = '{self.schema}' " \
                         f" AND upper(table_name) = upper('{temp_table_name}') " \
                         f" AND upper(column_name) <> upper('{join_key}') " \
                         f" AND upper(data_type) NOT IN ('GEOMETRY')"
        list_of_fields = self.pgsql_fetch(list_of_fields, return_array=True)
        list_of_fields = ', '.join(x[0] for x in list_of_fields)

        command = f'' \
                  f'CREATE UNLOGGED TABLE {temp2_table_name} AS ' \
                  f'SELECT a.*, {list_of_fields} ' \
                  f' FROM {append_table_name} a ' \
                  f'    LEFT JOIN {temp_table_name} b ON a.{join_key} = b.{join_key}'
        self.execute(command)
        command = f"DROP TABLE {append_table_name}"
        self.execute(command)
        command = f"ALTER TABLE {temp2_table_name} RENAME TO {append_table_name}"
        self.execute(command)
        command = f"ALTER TABLE {append_table_name} SET LOGGED"
        self.execute(command)
        add_primary_key = f'ALTER TABLE {append_table_name} ADD PRIMARY KEY ({join_key})'
        self.execute(add_primary_key)
        command = f"DROP TABLE {temp_table_name}"
        self.execute(command)

        return True

    def test_table_exists(self, in_table):

        command = f"" \
                  f"SELECT EXISTS(" \
                  f"SELECT * FROM information_schema.tables " \
                  f"WHERE table_schema = lower('{self.schema}') AND table_name = lower('{in_table}'))"
        return self.pgsql_fetch(command)

    def create_voxel_index_dictionary_object(self, voxel_space, extra_fields='', data_limits=''):
        # extra_fields like ['field1', 'field2', '...']
        # data limits like "field1 > 0 AND field2 = 100"
        extra_fields_present = len(extra_fields) > 0
        if extra_fields_present:
            extra_fields = ', '.join(x for x in extra_fields)
            extra_fields = ', ' + extra_fields
        if len(data_limits) > 0:
            data_limits = "WHERE " + data_limits

        command = f"SELECT voxel_index {extra_fields} " \
                  f"FROM {voxel_space} {data_limits} " \
                  f"ORDER BY 1)"
        indicies = self.pgsql_fetch(command, True)
        voxel_dict = dict()

        for index in indicies:
            if extra_fields_present:
                voxel_dict[index[0]] = [index[1:]]
            else:
                voxel_dict[index[0]] = []

        return voxel_dict

    def is_table_logged(self, table):
        command = f"SELECT relname, relpersistence FROM pg_class WHERE relname = lower('{table}')"
        if self.pgsql_fetch(command) == 'u':
            return False
        return True

    def create_pgdb_vision_table(self, tile_name, xyz_max_voxelz):

        voxel_space_z_cross_section_max = (xyz_max_voxelz[0] + 1) * (xyz_max_voxelz[1] + 1)
        command = f"CREATE TABLE ssim_pipeline_{tile_name}.{tile_name}_vision (" \
                  f"voxel_index PRIMARY KEY, " \
                  f"x INTEGER NOT NULL, " \
                  f"y INTEGER NOT NULL, " \
                  f"z INTEGER NOT NULL); " \
                  f"INSERT INTO ssim_pipeline_{tile_name}.{tile_name}_vision (voxel_index, x, y, z) " \
                  f"SELECT (z * {voxel_space_z_cross_section_max}) + (y * ({xyz_max_voxelz[0]} + 1)) + x, x, y, z " \
                  f"FROM generate_series(0, 100) x " \
                  f"CROSS JOIN generate_series(0, 100) y " \
                  f"CROSS JOIN generate_series(0, 100) z; " \
                  f"CREATE INDEX ON ssim_pipeline_{tile_name}.{tile_name}_vision USING gist(geom);"

        self.execute(command)