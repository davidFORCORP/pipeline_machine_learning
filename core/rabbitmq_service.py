"""
Module that contains the RabbitMqService class.
"""

import pika
import threading
import time

from core.config import Config


class RabbitMqService:
    """
    Responsible for interacting and returning RabbitMQ related classes.

    Currently uses Pika to interact with our local RabbitMq server.
    """

    def __init__(self, config=None):
        self.config = config
        if self.config is None:
            self.config = Config()

        username = self.config.get("rabbitmq", "username")
        password = self.config.get("rabbitmq", "password")
        credentials = pika.PlainCredentials(username, password)

        host = self.config.get("rabbitmq", "host")
        port = self.config.get("rabbitmq", "port")
        virtual_host = self.config.get("rabbitmq", "virtual_host")
        heartbeat = int(self.config.get("rabbitmq", "heartbeat"))
        self.connection_parameters = pika.ConnectionParameters(host=host,
                                                               port=port,
                                                               virtual_host=virtual_host,
                                                               heartbeat=heartbeat,
                                                               credentials=credentials)

    def connection(self):
        """
        Creates a new connection based on server/project configuration.
        """
        return pika.BlockingConnection(self.connection_parameters)

    def channel(self, connection):
        """
        Creates a new channel based on current connection.
        """
        channel = connection.channel()
        channel.basic_qos(prefetch_count=1)
        return channel

    def heartbeat_thread(self, connection):
        """
        Creates a new thread whose sole purpose is to keep sending heartbeats to pika.

        Used for RabbitMq connections that are alive for more than 60 seconds (default heartbeat timeout), Pika's
        BlockingConnection pretty much only sends heartbeats during ack calls (e.g. basic_ack(), ack()). The workaround
        is to have your own dedicated thread for sending heartbeats.
        Inspired by this thread: https://github.com/pika/pika/issues/1104
        """
        t = threading.Thread(target=heartbeat_callback, args=(connection,))
        t.daemon = True
        t.start()

        return t

    def message_properties(self):
        return pika.BasicProperties(content_type="application/json", delivery_mode=pika.spec.PERSISTENT_DELIVERY_MODE)

    def ensure_queue_exists(self, channel, queue):
        """
        Ensures a queue exists before attempting to consume from it.
        """
        channel.queue_declare(queue, durable=True)


def heartbeat_callback(connection):
    """
    Callback helper function for heartbeat_thread(). Sends heartbeat signals back to RabbitMq broker.
    """
    while connection.is_open:
        connection.process_data_events()
        time.sleep(10)
