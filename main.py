"""
Module that contains classes related to running the bootstrapper. Should be the primary python class to start in PRD.
"""
import argparse

from core.sequence_bootstrapper import SequenceBootstrapper


def main():
    """
    Runs the bootstrapper with any given command line arguments.
    """
    parser = argparse.ArgumentParser(description='Bootstrap ML processors')

    parser.add_argument('--projection', type=int, help='Number of projection processors')
    parser.add_argument('--clip', type=int, help='Number of clip processors')
    parser.add_argument('--bare-earth', type=int, help='Number of bare earth processors')
    parser.add_argument('--height-above-ground', type=int, help='Number of height above ground processors')
    parser.add_argument('--height-filter', type=int, help='Number of height filter processors')
    parser.add_argument('--stem-finder', type=int, help='Number of stem finder processors')
    parser.add_argument('--finalize-clip', type=int, help='Number of finalize clip processors')
    parser.add_argument('--pgloader', type=int, help='Number of pgloader processors')
    parser.add_argument('--attribute', type=int, help='Number of attribute processors')
    parser.add_argument('--vision', type=int, help='Number of vision processors')
    parser.add_argument('--elands-export', type=int, help="Number of elands export processors")
    args = parser.parse_args()

    bootstrapper = SequenceBootstrapper(number_of_projection=args.projection,
                                        number_of_clip=args.clip,
                                        number_of_bare_earth=args.bare_earth,
                                        number_of_height_above_ground=args.height_above_ground,
                                        number_of_height_filter=args.height_filter,
                                        number_of_stem_finder=args.stem_finder,
                                        number_of_finalize_clip=args.finalize_clip,
                                        number_of_pg_loader=args.pgloader,
                                        number_of_attribute=args.attribute,
                                        number_of_vision=args.vision,
                                        number_of_elands_export=args.elands_export)
    bootstrapper.run()


if __name__ == '__main__':
    main()
