"""
Contains constants for the project.
"""
import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))  # This is your Project Root
POSTGIS_DIR = os.path.join(ROOT_DIR, 'postgis')
