
pipeline {
    agent any

    // TODO: yum install proj80-devel

    stages {
        stage('Install Conda') {
            steps {
                sh '''
                    wget -N https://repo.continuum.io/archive/Anaconda3-2019.10-Linux-x86_64.sh
                    chmod +x ./Anaconda3-2019.10-Linux-x86_64.sh
                    ./Anaconda3-2019.10-Linux-x86_64.sh -b -f
                    export PATH=/var/lib/jenkins/anaconda3/bin:$PATH
                    conda init bash
                '''
            }
        }

        stage('Setup Conda') {
            // Since this step is pretty slow, we only want to run this if any of the below conditions match:
            // 1) First time setting up Jenkins environment (i.e. blank "conda_packages_installed" file exists)
            // 2) A package has been added/updated/removed (i.e. "setup_conda_packages.sh" file has changed)
            when {
                anyOf {
                    not {
                        expression { return fileExists("conda_packages_installed") }
                    };
                    changeset "**/setup_conda_packages.sh";
                }
            }
            steps {
                sh '''
                    source ~/.bashrc
                    conda clean -ayq
                    ./setup_conda.sh
                    conda activate ssim-pipeline
                    ./setup_conda_packages.sh
                    echo > ./conda_packages_installed
                '''
            }
        }

        stage('Setup Config') {
            steps {
                sh '''
                    rm -f ./settings.local.ini
                    cp ./settings.testing.ini ./settings.local.ini
                '''
            }
        }

        stage('Test') {
            steps {
                sh '''
                    source ~/.bashrc
                    conda activate ssim-pipeline
                    python3 -m unittest discover -p \"test_*.py\"
                '''
            }
        }
    }

    post {
        failure {
            // Send email to developers when a build fails
            emailext (
                subject: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
                mimeType: 'text/html',
                body: """
                    <p>FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
                    <p>Check console output at <a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a></p>
                    """,
                recipientProviders: [[$class: 'DevelopersRecipientProvider']]
            )
        }
    }
}
