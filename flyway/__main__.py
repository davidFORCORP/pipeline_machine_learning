import os

from core.command_runner import CommandRunner


def migrate():
    flyway_executable = os.path.join(os.getcwd(), 'flyway', 'flyway')
    CommandRunner().run(flyway_executable + ' migrate')


if __name__ == "__main__":
    migrate()
