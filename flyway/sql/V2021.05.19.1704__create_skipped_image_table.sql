-- Purpose of this table is to keep track of what images were skipped during processing
-- The reason we keep track of this data is because we may need to come back to it.
-- e.g. Image was skipped due to avi_extent filter, but we eventually get avi info and update the avi_extent shapefile
CREATE TABLE ssim_pipeline.skipped_image (
    id         SERIAL PRIMARY KEY,
    image_name TEXT,
    timestamp  TIMESTAMP,
    reason     TEXT
);