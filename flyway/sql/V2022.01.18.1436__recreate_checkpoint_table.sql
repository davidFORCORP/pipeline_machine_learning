-- New checkpointing system will be based on event-sourcing
-- This way, we can implement querier checkpointing in a more reusable fashion
DROP TABLE IF EXISTS ssim_pipeline.attribute_checkpoint;
DROP TABLE IF EXISTS ssim_pipeline.tile_checkpoint;

CREATE TABLE ssim_pipeline.tile_checkpoint (
    tile_name text not null,
    class_name text not null,
    action text not null,
    timestamp timestamp not null,
    primary key (tile_name, class_name, action)
);