/*
To be consistent with changing all the other tables to be called "tile" instead of "tile", we're changing this one as well.
This SQL was already ran manually, this script is just for documenting my changes on this schema.
*/

-- ALTER TABLE ssim_pipeline.exported_image RENAME TO exported_tile;
-- ALTER TABLE ssim_pipeline.exported_tile RENAME COLUMN image_name To tile_name;