-- After more thought, it makes more sense to use the term 'export' when referring to the elands import.
-- ssim_pipeline is in charge of exporting imagery data to eLands, so we should be using the term 'export'.
ALTER TABLE ssim_pipeline.imported_image RENAME TO exported_image;