-- Based on the table matrix of scenarios that David gave me in Slack, fill a table to be used inside a specialized merchandizer.
-- This table will hold me accountable if we want to check a specific scenario's parameters.
-- It'll also make the kafka messages much easier to read
-- i.e. instead of multiple variables inside the kafka message like height_parameter and dbh_parameter, we only need one variable - scenario_number.
CREATE TABLE IF NOT EXISTS ssim_pipeline.merchandizing_scenario (
    scenario_number integer,
    merchandize_ssim_function text,
    log_metric_generation_function text,
    height_parameter text,
    dbh_parameter text
);

INSERT INTO ssim_pipeline.merchandizing_scenario VALUES (1, 'merchandize_ssim_v1p007', 'log_metric_generation_v1p004', 'chm_v1', 'dbh_huang');
INSERT INTO ssim_pipeline.merchandizing_scenario VALUES (2, 'merchandize_ssim_v2p001', 'log_metric_generation_v2p001', 'chm_v1', 'dbh_huang');
INSERT INTO ssim_pipeline.merchandizing_scenario VALUES (3, 'merchandize_ssim_v2p002', 'log_metric_generation_v2p002', 'chm_v1', 'dbh_huang');
INSERT INTO ssim_pipeline.merchandizing_scenario VALUES (4, 'merchandize_ssim_v2p003', 'log_metric_generation_v2p002', 'chm_v1', 'dbh_huang');
INSERT INTO ssim_pipeline.merchandizing_scenario VALUES (5, 'merchandize_ssim_v1p007', 'log_metric_generation_v1p004', 'chm_v1', 'dbh_huang_adj');
INSERT INTO ssim_pipeline.merchandizing_scenario VALUES (6, 'merchandize_ssim_v2p001', 'log_metric_generation_v2p001', 'chm_v1', 'dbh_huang_adj');
INSERT INTO ssim_pipeline.merchandizing_scenario VALUES (7, 'merchandize_ssim_v2p002', 'log_metric_generation_v2p002', 'chm_v1', 'dbh_huang_adj');
INSERT INTO ssim_pipeline.merchandizing_scenario VALUES (8, 'merchandize_ssim_v2p003', 'log_metric_generation_v2p002', 'chm_v1', 'dbh_huang_adj');
INSERT INTO ssim_pipeline.merchandizing_scenario VALUES (9, 'merchandize_ssim_v1p007', 'log_metric_generation_v1p004', 'adj_height', 'dbh_huang');
INSERT INTO ssim_pipeline.merchandizing_scenario VALUES (10, 'merchandize_ssim_v2p001', 'log_metric_generation_v2p001', 'adj_height', 'dbh_huang');
INSERT INTO ssim_pipeline.merchandizing_scenario VALUES (11, 'merchandize_ssim_v2p002', 'log_metric_generation_v2p002', 'adj_height', 'dbh_huang');
INSERT INTO ssim_pipeline.merchandizing_scenario VALUES (12, 'merchandize_ssim_v2p003', 'log_metric_generation_v2p002', 'adj_height', 'dbh_huang');
INSERT INTO ssim_pipeline.merchandizing_scenario VALUES (13, 'merchandize_ssim_v1p007', 'log_metric_generation_v1p004', 'adj_height', 'dbh_huang_adj');
INSERT INTO ssim_pipeline.merchandizing_scenario VALUES (14, 'merchandize_ssim_v2p001', 'log_metric_generation_v2p001', 'adj_height', 'dbh_huang_adj');
INSERT INTO ssim_pipeline.merchandizing_scenario VALUES (15, 'merchandize_ssim_v2p002', 'log_metric_generation_v2p002', 'adj_height', 'dbh_huang_adj');
INSERT INTO ssim_pipeline.merchandizing_scenario VALUES (16, 'merchandize_ssim_v2p003', 'log_metric_generation_v2p002', 'adj_height', 'dbh_huang_adj');