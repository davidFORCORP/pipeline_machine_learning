-- In order to manually run some queries (due to their slowness/crashes), we implement a checkpoint system
-- As a another benefit, we can avoid re-running old queries if the database crashes
CREATE TABLE ssim_pipeline.attribute_checkpoint (
    image_name text primary key not null,
    class_name text not null,
    name text not null,
    timestamp timestamp not null
);

-- Example data based on what's stuck in the pipeline currently + test data
INSERT INTO ssim_pipeline.attribute_checkpoint (image_name, class_name, name, timestamp) VALUES
('t050r11w5_NE.tif', 'CrownIndexTreeQuerier', 'Calculating crown index', now()),
('t050r12w5_NW.tif', 'CrownIndexTreeQuerier', 'Calculating crown index', now()),
('t056r16w5_SW.tif', 'CrownIndexTreeQuerier', 'Calculating crown index', now()),
('test_fixture_t043r06w5_NE.tif', 'CrownIndexTreeQuerier', 'Calculating crown index', now());