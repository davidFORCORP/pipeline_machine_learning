-- We're recreating these tables, whether they exist within ssim-pipeline
DROP TABLE IF EXISTS ssim_pipeline.crown_index_min;
DROP TABLE IF EXISTS ssim_pipeline.crown_index_max;
DROP TABLE IF EXISTS ssim_pipeline.huang_coef;

-- Crown index tables
CREATE TABLE ssim_pipeline.crown_index_min AS (
    SELECT * FROM ssim_v3p01.crown_index_min
);
CREATE TABLE ssim_pipeline.crown_index_max AS (
    SELECT * FROM ssim_v3p01.crown_index_max
);