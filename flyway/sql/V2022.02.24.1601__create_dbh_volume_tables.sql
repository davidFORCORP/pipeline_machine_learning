-- We're recreating these tables, whether they exist within ssim-pipeline
DROP TABLE IF EXISTS ssim_pipeline.huang_coef;
DROP TABLE IF EXISTs ssim_pipeline.nregcoef;

-- Huang table for dbh calculation
CREATE TABLE ssim_pipeline.huang_coef AS (
    SELECT * FROM ssim_base.huang_coef
);
-- nreg coefficients for volume calculation
CREATE TABLE ssim_pipeline.nregcoef AS (
    SELECT * FROM p839.nregcoef
);