ALTER TABLE ssim_pipeline.metric_timing RENAME COLUMN image_name TO tile_name;
ALTER TABLE ssim_pipeline.image_progress RENAME TO tile_progress;
ALTER TABLE ssim_pipeline.tile_progress RENAME COLUMN image_name TO tile_name;