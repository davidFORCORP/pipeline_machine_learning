-- Function copied from david.imageanalysis_assigntreevolume
drop function if exists ssim_pipeline.calculate_tree_volume(text, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision);

create function ssim_pipeline.calculate_tree_volume(text, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision) returns double precision
    language plpgsql
as
$$
DECLARE
    species text;
    a0 float;
    a1 float;
    a2 float;
    b1 float;
    b2 float;
    b3 float;
    b4 float;
    b5 float;
    d1 float;
    d2 float;
    c float;
    g0 float;
    g1 float;
    dbh float;
    ht float;
    stumpH float;
    dibs float; --diameter inside bark
    dobs float; --diameter outside bark
    mh float; --stem height from bottom to top of tree at user_topdib
    mlen float; --usable stem
    volume float;
    sectvol float;
    secno integer;
    hi float;
    sectlen float;
    hib float;
    him float;
    hiu float;
    zb float;
    xb float;
    dibb float;
    bab float;
    bam float;
    zm float;
    xm float;
    dibm float;
    zu float;
    xu float;
    dibu float;
    bau float;
    vol float;
    user_topdib float;
    user_dob float;
    user_length float;
    user_topdib_c float;
    user_dob_c float;
    user_length_c float;
    user_topdib_d float;
    user_dob_d float;
    user_length_d float;
BEGIN
    species:= $1;
    ht:= $2;
    dbh:= $3;
    a0:= $4;
    a1:= $5;
    a2:= $6;
    b1:= $7;
    b2:= $8;
    b3:= $9;
    b4:= $10;
    b5:= $11;
    d1:= $12;
    d2:= $13;
    user_topdib_c:= $14;
    user_dob_c:= $15;
    user_length_c:= $16;
    user_topdib_d:= $17;
    user_dob_d:= $18;
    user_length_d:= $19;

    IF UPPER(species) IN ('AW','BW','PB') THEN
        user_topdib:= user_topdib_d;
        user_dob:= user_dob_d;
        user_length:= user_length_d;
    ELSE
        user_topdib:= user_topdib_c;
        user_dob:= user_dob_c;
        user_length:= user_length_c;
    END IF;

    stumpH:= 0.15;
    dibs:= (a0*dbh^a1)*(a2^dbh)*((1 - sqrt(stumpH/ht)) / (1.000 - sqrt(0.225)))^(b1*(stumpH/ht)^2.00+b2*ln(stumpH/ht+0.001)+b3*sqrt(stumpH/ht)+b4*exp(stumpH/ht)+b5*dbh/ht);
    dobs:= (d1 + d2 * dibs);

    IF user_topdib < dbh AND dobs > user_dob THEN
        g0:= 0.9;

        c:= b1*(g0)^2.00+b2*log(g0+0.001)+b3*sqrt(g0)+b4*exp(g0)+b5*(dbh/ht);
        g1:=(1.00-((user_topdib/(a0*dbh^a1*a2^dbh))^(1.00/c))*(1.00-sqrt(0.225)))^2.00;
        WHILE abs(g0-g1) >= 0.1
            LOOP
                c:=b1*(g0)^2.00+b2*ln(g0+0.001)+b3*sqrt(g0)+b4*exp(g0)+b5*(dbh/ht);
                g1:=(1.00-((user_topdib/(a0*dbh^a1*a2^dbh))^(1.00/c))*(1.00-sqrt(0.225)))^2.00;
                g0:=(g0+g1)/2.00;
            END LOOP;

        mh:= (g0 * ht);
        mlen:= (mh - stumpH);
        volume:= 0.00;
        secno:= 1.00;
        hi:= stumpH;
        sectlen:= (mlen/30.00);

        WHILE secno <= 30.00
            LOOP
                hib:= (hi + (sectlen - sectlen));
                him:= (hi + (sectlen-sectlen/2.00));
                hiu:= (hi + (sectlen));
                zb:= ((hib) / ht);
                xb:= ((1.00 - sqrt(zb)) / (1.00 - sqrt(0.225)));
                dibb:= ((a0*dbh^a1)*(a2^dbh)*xb^(b1*zb^2.00+b2*log(zb+0.001)+b3*sqrt(zb)+b4*exp(zb)+b5*dbh/ht));
                bab:= ((dibb/200.00)^2.00*3.14159);

                IF him <= ht THEN
                    zm:= (him/ht);
                    xm:= ((1.00 - sqrt(zm)) / (1.00 - sqrt(0.225)));
                    dibm:= ((a0*dbh^a1)*(a2^dbh)*xm^(b1*zm^2.00+b2*log(zm+0.001)+b3*sqrt(zm)+b4*exp(zm)+b5*dbh/ht));
                    bam = ((dibb/200.00)^2.00*3.14159);
                END IF;

                IF hiu <= ht THEN
                    zu:= (hiu / ht);
                    xu:= ((1.00 - sqrt(zu)) / (1 - sqrt(0.225)));
                    dibu:= ((a0*dbh^a1)*(a2^dbh)*xu^(b1*zu^2.00+b2*log(zu+0.001)+b3*sqrt(zu)+b4*exp(zu)+b5*dbh/ht));
                    bau:= ((dibu/200)^2*3.14159);
                END IF;

                hi:= hi + sectlen;
                sectvol:= sectlen / 6.00 * (bab + 4.00 * bam + bau);
                volume:= volume + sectvol;
                secno:= secno + 1;
            END LOOP;
    END IF;

    IF user_topdib > dbh THEN
        vol:= 0;
    ELSIF user_topdib = 0.1 THEN
        vol:= volume + 0.00007854 * user_topdib^2.00 * (ht-mh)/ 3.00 + 0.00007854 * dibs^2.00 * stumpH;
    ELSIF user_topdib > 0.1 THEN
        vol:= volume;
    END IF;

    IF (dobs >= user_dob AND mlen >= user_length) THEN RETURN vol;
    ELSE RETURN 0;
    END IF;
    --raise notice '%', volume::text;
EXCEPTION
    WHEN OTHERS THEN RETURN 0;
END
$$;

alter function ssim_pipeline.calculate_tree_volume(text, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision) owner to ssim;

