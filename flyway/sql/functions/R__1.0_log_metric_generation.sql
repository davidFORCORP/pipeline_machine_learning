create or replace function ssim_pipeline.log_metric_generation(tile text, tsa_ukey integer, open_id text, tree_id bigint, species text, ht double precision, dbh double precision, a0 double precision, a1 double precision, a2 double precision, b1 double precision, b2 double precision, b3 double precision, b4 double precision, b5 double precision, mh double precision, mlen double precision, group_ integer, utilization double precision, stump_convert double precision, stump double precision, dib_b_max_treshold double precision) returns void
    language plpgsql
as
$$
DECLARE
    logtype text;
    dib_u double precision;  -- diameter inside bark (upper)
    hib_b double precision;
    dib_b double precision;  -- diameter inside bark (bottom)
    seclent double precision;  -- length of log
    piece_number integer;  -- current piece of stem being bucked
    logvol double precision;
    log_class integer;
    dec_log double precision;
    _trim double precision;
    _trim_wiggle double precision;
    _random double precision;
    feet2meters double precision;
    actual_remaining_stem double precision;
    working_remaining_stem double precision;
    test_log_class integer;
    height_class integer;

BEGIN

    feet2meters:= 12.0 * 0.0254;
    dec_log:= 8.3;
    _trim:= 4 * 0.0254; -- inches to meters
    _trim_wiggle:= 1 * 0.0254; -- plus minus on trim
    hib_b:= stump_convert;
    dib_b:= stump;
    actual_remaining_stem:= mlen;
    piece_number:=1;
    <<full_stem>>
    WHILE TRUE
        LOOP
            <<stem_or_pulp>>
            WHILE TRUE  -- still stem (mlen)
                LOOP
                    IF group_ = 2 THEN
                        logtype:='DEC_Log';
                        seclent:= dec_log * feet2meters + _trim;  -- actual dec log length un modified
                        IF seclent > actual_remaining_stem AND seclent - _trim_wiggle > actual_remaining_stem THEN  --remaining stem to small to make normal or wiggled log
                            seclent:=actual_remaining_stem;
                            logtype:='PUL';
                            EXIT stem_or_pulp;
                        ELSIF actual_remaining_stem >= seclent - _trim_wiggle AND actual_remaining_stem < seclent THEN  --remaining stem to small to make normal log but can make wiggled log
                            seclent:= seclent - _trim_wiggle; -- we can make a log just 1 inch shorter
                            EXIT stem_or_pulp;
                        ELSIF actual_remaining_stem <= seclent + _trim_wiggle AND actual_remaining_stem < seclent * 2 THEN  --remaining stem to big to make normal log and cannot fit another but can make wiggled log
                            seclent:= seclent + _trim_wiggle; -- we can make a log just 1 inch shorter
                            EXIT stem_or_pulp;
                        END IF;
                        EXIT stem_or_pulp;


                    ELSE
                        IF actual_remaining_stem >= ((32 + _trim * 2) * feet2meters) THEN

                            -- probability kick to create 10, 12, 14 at 5% chance each
                            SELECT random() INTO _random;
                            IF _random < 0.05 THEN log_class:= 10 ;
                            logtype := 'LOG'||log_class::text;
                            seclent:= log_class * feet2meters + _trim;
                            EXIT stem_or_pulp;
                            ELSIF _random < 0.1 THEN log_class:= 12 ;
                            logtype := 'LOG'||log_class::text;
                            seclent:= log_class * feet2meters + _trim;
                            EXIT stem_or_pulp;
                            ELSIF _random < 0.15 THEN log_class:= 14 ;
                            logtype := 'LOG'||log_class::text;
                            seclent:= log_class * feet2meters + _trim;
                            EXIT stem_or_pulp;
                            ELSIF _random < 0.30 THEN log_class:= 20 ;
                            logtype := 'LOG'||log_class::text;
                            seclent:= log_class * feet2meters + _trim;

                            SELECT a.dib_b, a.dib_u from ssim_pipeline.merchandize_get_dibs(dbh::int, ht, hib_b, hib_b + seclent, a0, a1, a2, b1, b2, b3, b4, b5) as a INTO dib_b, dib_u;

                            IF dib_b >= dib_b_max_treshold THEN
                                logtype := 'LOG16';
                                seclent:= 16.0 * feet2meters + _trim;
                            END IF;

                            ELSE log_class:= 16 ;
                            logtype := 'LOG'||log_class::text;
                            seclent:= log_class * feet2meters + _trim;
                            END IF;


                            EXIT stem_or_pulp;

                        ELSIF actual_remaining_stem < (32 + _trim * 2) * feet2meters AND actual_remaining_stem >= (26 + _trim * 2) * feet2meters THEN
                            logtype := 'LOG16';
                            seclent:= 16.0 * feet2meters + _trim;
                            EXIT stem_or_pulp;

                        ELSIF actual_remaining_stem < (26 + _trim * 2) * feet2meters AND actual_remaining_stem >= (22 + _trim * 2) * feet2meters THEN
                            logtype := 'LOG12';
                            seclent:= 12.0 * feet2meters + _trim;
                            EXIT stem_or_pulp;

                        ELSE
                            working_remaining_stem:= actual_remaining_stem;
                            height_class:= ((working_remaining_stem - _trim)* 3.28084 - 1.0)/2.0;
                            -- make sure we are accounting for the _trim that is required of the next log to make the actual class
                            test_log_class:= (round(height_class::numeric, 0) * 2.0)::int;

                            <<next_log>>
                            WHILE TRUE
                                LOOP
                                    -- retrieve the table value for log class
                                    EXECUTE format('SELECT log_class_gt_threshold
									FROM ssim_pipeline.log_class_table
									WHERE stem_remaining = %1$s', test_log_class) INTO log_class ;
                                    logtype := 'LOG'||log_class::text;

                                    IF log_class IS NULL AND test_log_class >= 8 THEN
                                        test_log_class:= test_log_class - 2;
                                        CONTINUE next_log;
                                    ELSIF log_class IS NULL AND (test_log_class < 8 OR test_log_class IS NULL)  THEN
                                        seclent:= working_remaining_stem;
                                        logtype:= 'PUL';
                                        EXIT stem_or_pulp;
                                    END IF;

                                    seclent:=log_class::numeric * feet2meters  + _trim;

                                    -- test to see if log with wiggle room is possible
                                    -- unlikely to be hit (0<X<_trim_wiggle)
                                    IF working_remaining_stem - seclent <= _trim_wiggle AND working_remaining_stem - seclent >= 0 THEN
                                        seclent:= working_remaining_stem;
                                        EXIT stem_or_pulp;
                                    END IF;

                                    -- next, test to see if a little wiggle room making the log smaller or larger gets us the last LOG8 product
                                    IF (working_remaining_stem >= seclent - _trim_wiggle OR working_remaining_stem <= seclent + _trim_wiggle)
                                        AND test_log_class = 8 THEN
                                        IF working_remaining_stem >= seclent - _trim_wiggle THEN
                                            seclent:= seclent - _trim_wiggle;
                                        ELSE
                                            seclent:= seclent + _trim_wiggle;
                                        END IF;
                                        EXIT stem_or_pulp;
                                    END IF;

                                    -- test for a log with normal trim
                                    IF seclent > working_remaining_stem THEN
                                        test_log_class:= test_log_class - 2;
                                        CONTINUE next_log;
                                    END IF;

                                    IF working_remaining_stem < seclent - _trim_wiggle OR seclent - _trim_wiggle <= 0 THEN
                                        logtype:= 'PUL' ;
                                        seclent := working_remaining_stem;
                                        EXIT stem_or_pulp;
                                    END IF;

                                    EXIT stem_or_pulp;

                                END LOOP next_log;

                        END IF;

                    END IF;

                END LOOP stem_or_pulp;

            IF logtype = 'PUL' THEN
                dib_b:= dib_u;
                dib_u:= utilization;
            ELSE
                SELECT a.dib_b, a.dib_u from ssim_pipeline.merchandize_get_dibs(dbh::int, ht, hib_b, hib_b + seclent, a0, a1, a2, b1, b2, b3, b4, b5) as a INTO dib_b, dib_u;
            END IF;

            logvol:= (3.14159265359 * seclent * 100.0) / 12.0 * (power(dib_b,2) + dib_b * dib_u + power(dib_u,2)) / 1000000;

            INSERT INTO ssim_pipeline.merchandized_tree (tsa_ukey, tile, open_id, tree_id, species, dbh, height, group_,
                                                      mh, mlen, dib_u, dib_b, seclent, hib_u, piece_num, logtype, logvol)
            VALUES (tsa_ukey, tile, open_id, tree_id, species, dbh, ht, group_, mh, mlen, dib_u, dib_b, seclent, hib_b + seclent, piece_number, logtype, logvol);

            IF logtype = 'PUL' OR actual_remaining_stem <= 0 THEN
                EXIT full_stem;
            END IF;

            hib_b:= hib_b + seclent;
            piece_number:= piece_number + 1;
            actual_remaining_stem:= actual_remaining_stem - seclent;

            IF logtype = 'PUL' OR actual_remaining_stem <= 0 THEN
                EXIT full_stem;
            END IF;

        END LOOP full_stem;

END;
$$;

alter function ssim_pipeline.log_metric_generation(text, text, integer, text, bigint, text, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, integer, double precision, double precision, double precision, double precision) owner to ssim;

