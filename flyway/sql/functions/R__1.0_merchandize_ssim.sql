create or replace function ssim_pipeline.merchandize_ssim(_tile text, _tsa_ukey integer, _open_id text, _tree_id integer, _species text, _ht double precision, _dbh double precision, _stump_ht double precision, _topdib_dec double precision, _topdib_con double precision, _nsr text) returns void
    language plpgsql
as
$$
DECLARE
    sp_comp2 character varying;
    group_ integer;
    topdib double precision;
    stump_convert double precision;
    dib double precision;
    a0 double precision;
    a1 double precision;
    a2 double precision;
    b1 double precision;
    b2 double precision;
    b3 double precision;
    b4 double precision;
    b5 double precision;
    d1 double precision;
    d2 double precision;
    stump double precision;
    g0 double precision;
    g1 double precision;
    c double precision;
    mh double precision;
    mlen double precision;
BEGIN

    --FIRST ASSIGN SPECIES GROUP
    IF _species IN ('FA', 'FB', 'FD', 'SW', 'SE') THEN
        sp_comp2:= 'SW';
    ELSIF _species IN ('SB') THEN
        sp_comp2:= 'SB';
    ELSIF _species IN ('PL', 'PJ', 'PF', 'PW', 'LT', 'P') THEN
        sp_comp2:= 'PL';
    ELSIF _species IN ('AW','PB','BW', 'DD') THEN
        sp_comp2:= 'AW';
    ELSE
        RETURN;
    END IF;

    IF sp_comp2 = 'AW' THEN
        group_ := 2;
    ELSE
        group_ := 1;
    END IF;

    SELECT INTO a0 (SELECT n.a0
                    FROM ssim_pipeline.nregcoef n
                    WHERE n.nat_region = _nsr
                      AND n.sp = sp_comp2
                   );
    SELECT INTO a1 (SELECT n.a1
                    FROM ssim_pipeline.nregcoef n
                    WHERE n.nat_region = _nsr
                      AND n.sp = sp_comp2
                   );
    SELECT INTO a2 (SELECT n.a2
                    FROM ssim_pipeline.nregcoef n
                    WHERE n.nat_region = _nsr
                      AND n.sp = sp_comp2
                   );
    SELECT INTO b1 (SELECT n.b1
                    FROM ssim_pipeline.nregcoef n
                    WHERE n.nat_region = _nsr
                      AND n.sp = sp_comp2
                   );
    SELECT INTO b2 (SELECT n.b2
                    FROM ssim_pipeline.nregcoef n
                    WHERE n.nat_region = _nsr
                      AND n.sp = sp_comp2
                   );
    SELECT INTO b3 (SELECT n.b3
                    FROM ssim_pipeline.nregcoef n
                    WHERE n.nat_region = _nsr
                      AND n.sp = sp_comp2
                   );
    SELECT INTO b4 (SELECT n.b4
                    FROM ssim_pipeline.nregcoef n
                    WHERE n.nat_region = _nsr
                      AND n.sp = sp_comp2
                   );
    SELECT INTO b5 (SELECT n.b5
                    FROM ssim_pipeline.nregcoef n
                    WHERE n.nat_region = _nsr
                      AND n.sp = sp_comp2
                   );
    SELECT INTO d1 (SELECT n.d1
                    FROM ssim_pipeline.nregcoef n
                    WHERE n.nat_region = _nsr
                      AND n.sp = sp_comp2
                   );
    SELECT INTO d2 (SELECT n.d2
                    FROM ssim_pipeline.nregcoef n
                    WHERE n.nat_region = _nsr
                      AND n.sp = sp_comp2
                   );

    stump_convert:= _stump_ht / 100;
    dib:=a0*power(_dbh,a1)*power(a2,_dbh)*
         power(((1 - sqrt(stump_convert/_ht)) / (1 - sqrt(.225))),(b1*power((stump_convert/_ht),2)+b2*
                                                                                                   ln(stump_convert/_ht+0.001)+b3*sqrt(stump_convert/_ht)+b4*exp(stump_convert/_ht)+b5*_dbh/_ht));
    stump:=d1+d2*dib;

    mlen := 0;
    mh:=0;

    IF group_ = 2 THEN
        topdib := _topdib_dec; --SET TOP DIAMETER (4 inches = 10)

    ELSE
        topdib := _topdib_con; --SET TOP DIAMETER (4 inches = 10)
    END IF;

    IF stump >= topdib THEN --minimum stump diameter
        IF topdib < _dbh THEN

            g0 := 0.900;
            c := b1 * power(g0,2) + b2 * ln(g0 + 0.001) + b3 * sqrt(g0) + b4 * exp(g0) + b5 * (_dbh / _ht);
            g1 := power((1 - (power((topdib / (a0 * power(_dbh,a1) * power(a2,_dbh))),(1 / c))) * (1 - sqrt(0.225))),2);

            WHILE abs(g0-g1) >= 0.00000001
                LOOP

                    c := b1 * power(g0,2) + b2 * ln(g0 + 0.001) + b3 * sqrt(g0) + b4 * exp(g0) + b5 * (_dbh / _ht);
                    g1 := power((1 - (power((topdib / (a0 * power(_dbh,a1) * power(a2,_dbh))),(1 / c))) * (1 - sqrt(0.225))),2);
                    g0 := (g0 + g1) / 2;
                END LOOP;

            mh := round((g0 * _ht)::numeric, 2);
            mlen := mh - stump_convert;
        ELSE
            RETURN;-- ADDED - Not sure if this is required to reset code as if topdib < dbh does this mean that no logs can be processed?
        END IF;
    ELSE
        RETURN; -- ADDED - Not sure if we are supposed to just move forward if stump < 15
    END IF;

    IF mlen = 0 THEN
        RETURN;
    END IF;
    -- RAISE NOTICE '%, %, %, %, %, %, %, %, %, %, %, %, %, %, %, %, %, %, %, %, %, %, %', _tile, _tsa_ukey, _open_id, _tree_id::bigint, _species, _ht, _dbh, a0, a1, a2, b1, b2, b3, b4, b5, mh, mlen, group_, topdib, stump_convert, stump, 35.56;
    PERFORM ssim_pipeline.log_metric_generation(_tile, _tsa_ukey, _open_id, _tree_id::bigint, _species, _ht, _dbh, a0, a1, a2, b1, b2, b3, b4, b5, mh, mlen, group_, topdib, stump_convert, stump, 35.56::double precision);

END;
$$;

alter function ssim_pipeline.merchandize_ssim(text, text, integer, text, integer, text, double precision, double precision, double precision, double precision, double precision, text) owner to ssim;

