create function ssim_pipeline.merchandize_get_dibs(dbh integer, ht double precision, hib_b double precision, hib_u double precision, a0 double precision, a1 double precision, a2 double precision, b1 double precision, b2 double precision, b3 double precision, b4 double precision, b5 double precision)
    returns TABLE(dib_b double precision, dib_u double precision)
    language plpgsql
as
$$
DECLARE
    zb double precision;
    xb double precision;
    xb_b double precision;
    zb_b double precision;

BEGIN
    zb := hib_u / ht;
    xb := (1.0 - sqrt(zb)) / (1.0 - sqrt(.225));
    dib_u := (a0 * power(dbh,a1)) * (power(a2,dbh)) * power(xb,(b1 * power(zb,2) + b2 * ln(zb+0.001) + b3 * SQRT(zb) + b4 * EXP(zb) + b5 * dbh / ht));

    zb_b := (hib_b)/ht;
    xb_b := (1.0 - sqrt(zb_b)) / (1.0 - sqrt(.225));
    dib_b := (a0* power(dbh,a1)) * (power(a2,dbh)) * power(xb_b, (b1 * power(zb_b,2) + b2 * ln(zb_b+0.001) + b3 * sqrt(zb_b) + b4 * exp(zb_b)+ b5 * dbh / ht));
    RETURN QUERY SELECT dib_b, dib_u;

END
$$;

alter function ssim_pipeline.merchandize_get_dibs(integer, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision) owner to ssim;