-- Simplifying version number on rows in imported_image table
-- Because we currently do not want to re-import any images into eLands yet the development versioning of older images
-- and newer images are technically different, we simplify the versioning for now in order to avoid re-importing in
-- import processor.
-- To document the versioning, the images imported on 2021-05-12 were 2.1.2. The current version is 2.1.4 but may
-- change in the near future.
UPDATE ssim_pipeline.imported_image SET version = '2.1';