CREATE TABLE IF NOT EXISTS ssim_pipeline.merchandized_tree_table
(
    master_key bigserial primary key,
    tile       varchar,
    tsa_ukey   integer,
    open_id    varchar,
    tree_id    integer,
    species    varchar,
    dbh        double precision,
    height     double precision,
    group_     integer,
    mh         double precision,
    mlen       double precision,
    dib_u      double precision,
    dib_b      double precision,
    seclent    double precision,
    hib_u      double precision,
    piece_num  integer,
    logtype    varchar,
    logvol     double precision
);

alter table ssim_pipeline.merchandized_tree_table
    owner to ssim;