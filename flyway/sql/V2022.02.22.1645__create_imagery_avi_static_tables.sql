DROP TABLE IF EXISTS ssim_pipeline.avi_lidar_ht;
DROP TABLE IF EXISTS ssim_pipeline.brl_sample_imagery;
DROP TABLE IF EXISTS ssim_pipeline.brl_avi;

CREATE TABLE ssim_pipeline.avi_lidar_ht AS (
    SELECT * FROM ssim_base.avi_lidar_ht
);

CREATE TABLE ssim_pipeline.brl_sample_imagery AS (
    SELECT * FROM ssim_base.brl_sample_imagery
);

CREATE TABLE ssim_pipeline.brl_avi AS (
    SELECT * FROM ssim_v3p01.brl_avi
);