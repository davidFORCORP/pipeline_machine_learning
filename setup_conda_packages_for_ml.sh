#!/bin/bash
# REQUIREMENT: You need to run setup_conda.sh first.
# Make sure to enter the conda environment via "conda activate ssim-pipeline" before running this script.
# Unfortunately running "conda activate" inside this shell script gives us the following error:
# |    CommandNotFoundError: Your shell has not been properly configured to use 'conda activate'.

# If you need the conda packages to run ssim-pipeline, refer to setup_conda_packages.sh instead.
# This is a script specifically for installing postgis shell scripts (e.g. shp2pgsql) that can't be installed
# without specific package versioning (and lack of other package requirements).
# I wouldn't normally save this shell script since it's not for ssim-pipeline specific purposes, but I've had to
# generate shp2pgsql several times now for the SSIM project so I thought might as well save it in version control for
# easy access.
conda install -qy tensorflow
conda install -qy scikit-learn
conda install -qy pandas
conda install -qy num2words